package me.perryplaysmc.eggwars.listeners;

import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.map.team.EggWarsTeamManager;
import me.perryplaysmc.eggwars.map.timer.Generator;
import me.perryplaysmc.eggwars.map.timer.GeneratorType;
import me.perryplaysmc.eggwars.utils.Integers;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.Version;
import me.perryplaysmc.eggwars.utils.inventory.Inventory;
import me.perryplaysmc.eggwars.utils.items.Items;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.MaterialData;
import org.bukkit.scheduler.BukkitRunnable;

import java.text.DecimalFormat;
import java.util.*;

@SuppressWarnings("all")
public class GeneratorInteraction implements Listener {
    HashMap<Player, Generator> invens = new HashMap<>();
    List<Player> upgrade = new ArrayList<>();

    @EventHandler
    void onBlockPlace(BlockPlaceEvent e) {
        EggWarsMap m = EggWarsCommand.getSelected().get(e.getPlayer());
        if ((e.getItemInHand().isSimilar(Items.getDiamondGenerator())) ||
                (e.getItemInHand().isSimilar(Items.getEmeraldGenerator())) || (e.getItemInHand().isSimilar(Items.getIronGenerator())) || (e.getItemInHand().isSimilar(Items.getGoldGenerator()))) {
            if (m == null) {
                e.getPlayer().sendMessage("§cSelect a map then try again.");
                return;
            }
            int start = 0;int max = 10;
            for (int i = 0; i < EggWarsCommand.getGens().size(); i++) {
                if (EggWarsCommand.getGens().get(i).getS().getName() == e.getPlayer().getName() &&
                        EggWarsCommand.getGens().get(i).getType() == GeneratorType.valueOf(e.getItemInHand().getType().name().replace("_BLOCK", ""))) {
                    Integers s = EggWarsCommand.getGens().get(i);
                    start = s.getInt1();
                    max = s.getInt2();
                    break;
                }
            }

            BlockFace blockFace = Utils.getBlockFaceOpposite(e.getPlayer());
            String blockFace1 = blockFace.name().contains("_") ? blockFace.name().split("-")[0] : blockFace.name();
            Block b = e.getBlockPlaced();
            Block a = b.getRelative(BlockFace.UP);
            if (!a.getType().name().contains("WALL_SIGN")) {
                a.setType(Material.OAK_WALL_SIGN, true);
                org.bukkit.block.Sign s = (org.bukkit.block.Sign)a.getState();
                setFacing(s, blockFace);
            }

            org.bukkit.block.Sign s = (org.bukkit.block.Sign)a.getState();
            m.addGenerator(new Generator(m, s, start, max, GeneratorType.valueOf(e.getItemInHand().getType().name().replace("_BLOCK", ""))));
            e.getPlayer().sendMessage("§cGenerator created");
        }
    }

    void setFacing(Sign b, BlockFace face) {
        if(Version.getVersion() == Version.Versions.v1_14) {
            org.bukkit.block.data.type.WallSign s = (org.bukkit.block.data.type.WallSign) b.getBlockData();
            s.setFacing(face);
            b.setBlockData(s);
            b.update();
        }else {
            org.bukkit.material.Sign sign = new org.bukkit.material.Sign(Material.LEGACY_WALL_SIGN);
            sign.setFacingDirection(face);
            b.setData(sign);
            b.update();
        }
    }
    List<Player> upgrades = new ArrayList<>();
    @EventHandler
    void onDamage(EntityDamageByEntityEvent e) {
        if(e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if(e.getDamager() instanceof Firework || upgrades.contains(p)) {
                e.setCancelled(true);
                e.setDamage(0);
                (new BukkitRunnable() {
                    @Override
                    public void run() {
                        upgrades.remove(p);
                    }
                }).runTaskLater(EggWars.getInstance(), 1L);
            }
        }
    }

    @EventHandler
    void onClick(PlayerInteractEvent e) {
        if ((!e.isCancelled()) && (!upgrade.contains(e.getPlayer())) &&
                (e.getAction() == Action.RIGHT_CLICK_BLOCK) && (
                ((e.getClickedBlock().getState() instanceof Sign)) || ((e.getClickedBlock().getRelative(BlockFace.UP).getState() instanceof Sign)))) {
            Sign s;
            if ((e.getClickedBlock().getState() instanceof Sign)) {
                s = (Sign)e.getClickedBlock().getState();
            } else {
                s = (Sign)e.getClickedBlock().getRelative(BlockFace.UP).getState();
            }
            Generator gen = null;
            EggWarsMap m = null;
            for (EggWarsMap ma : me.perryplaysmc.eggwars.map.MapManager.getMaps()) {
                if (e.isCancelled()) break;
                if (ma.getGenerator(s) != null) {
                    gen = ma.getGenerator(s);
                    m = ma;
                    break;
                }
            }
            if ((m == null) || (gen == null)) return;
            upgrade.add(e.getPlayer());
            if(!upgrades.contains(e.getPlayer()))
                upgrades.add(e.getPlayer());
            Bukkit.getScheduler().scheduleSyncDelayedTask(EggWars.getInstance(), () -> upgrade.remove(e.getPlayer()), 1L);


            if ((e.getPlayer().isSneaking()) && (!e.isCancelled()) && ((m.getState() == GameState.RUNNING) || (m.getState() == GameState.END))) {
                Player p = e.getPlayer();
                Material mm = gen.getUpgrade().getType();
                if (gen.getLevel() == gen.getMax()) {
                    Utils.sendMessage(p, "Map.GeneratorMaxed");
                    return;
                }
                if (getItems(p, gen.getUpgrade()) < gen.getUpgrade().getAmount()) {
                    Utils.sendMessage(p, "Map.NotEnoughMaterialForUpgrade", gen.getUpgrade().getAmount() + -getItems(p, gen.getUpgrade()), gen
                            .getUpgrade().getItemMeta().getDisplayName(), getItems(p, gen.getUpgrade()), gen.getUpgrade().getAmount());
                    return;
                }
                if (EggWarsTeamManager.getTeam(m, p) != null) {
                    EggWarsTeam t = EggWarsTeamManager.getTeam(m, p);
                    if (t != null) {
                        t.broadcast(Utils.translate("Messages.Team.upgradeGen", t
                                .getPlayers().size() > 1 ? p.getName() : "You", gen.getType().name().charAt(0) + gen.getType().name().substring(1).toLowerCase(), gen.getColor()));
                    }
                }
                e.setCancelled(true);
                removeItem(p, gen.getUpgrade().getAmount(), gen.getUpgrade().getType());
                if(!upgrades.contains(e.getPlayer()))
                    upgrades.add(e.getPlayer());
                gen.upgrade();
                return;
            }
            e.getPlayer().openInventory(gen.getInventory().getInventory());
            invens.put(e.getPlayer(), gen);
        }
    }


    @EventHandler
    void onInvenClick(InventoryClickEvent e) {
        Player p = (Player)e.getWhoClicked();
        if ((invens.containsKey(p)) && (e.getClickedInventory() != null) && (e.getClickedInventory().getType() != org.bukkit.event.inventory.InventoryType.CREATIVE)) {
            if ((new Inventory(p.getOpenInventory()).getName().equalsIgnoreCase(invens.get(p).getInventory().getName())) && (
                    (e.getClick() == ClickType.SHIFT_LEFT) || (e.getClick() == ClickType.SHIFT_RIGHT) || (e.getClick() == ClickType.DOUBLE_CLICK))) e.setCancelled(true);
            if (new Inventory(e.getView()).getName().equalsIgnoreCase(invens.get(p).getInventory().getName())) {
                Generator gen = invens.get(p);
                ItemStack i = new ItemStack(Material.EXPERIENCE_BOTTLE, gen.getLevel());

                ItemMeta im = i.getItemMeta();
                if (gen.getLevel() == gen.getMax()) {
                    i.setAmount(gen.getMax());
                    im.setDisplayName("§4§lGenerator maxed!");
                } else {
                    ItemStack upgrade = gen.getUpgrade();
                    gen.setLevel(gen.getLevel() + 1);
                    gen.updateLevel();
                    DecimalFormat a = new DecimalFormat("##0.0#");
                    String run = a.format(gen.getSpeed()).replace("-", "");
                    if (gen.getSpeed() == 1.0E7D) {
                        run = "0";
                    }
                    im.setDisplayName("§c§lUpgrade generator");
                    String gen1 = "§cGenerates: " + gen.getColor() + gen.getDropSize() + "§c every: " + gen.getColor() + run;
                    im.setLore(Arrays.asList("§cLevel: §4" + gen.getLevel(), "§cRequired Item: " + upgrade.getItemMeta().getDisplayName() + "§c x" + upgrade.getAmount(), gen1));
                    gen.setLevel(gen.getLevel() + -1);
                    gen.updateLevel();
                }
                gen.updateLevel();
                i.setItemMeta(im);
                if (e.getSlot() == 15) {
                    i = e.getClickedInventory().getItem(15);
                }
                e.setCancelled(true);
                if(!upgrades.contains(p))
                    upgrades.add(p);
                if ((i != null) && (e.getCurrentItem() != null) && (e.getCurrentItem().isSimilar(i)) && (!im.getDisplayName().equalsIgnoreCase("§4§lGenerator maxed!")) && (
                        (gen.getMap().getState() == GameState.RUNNING) || (gen.getMap().getState() == GameState.END))) {
                    Material mm = gen.getUpgrade().getType();
                    if (gen.getLevel() == gen.getMax()) {
                        Utils.sendMessage(p, "Map.GeneratorMaxed");
                        return;
                    }
                    if (getItems(p, gen.getUpgrade()) < gen.getUpgrade().getAmount()) {
                        Utils.sendMessage(p, "Map.NotEnoughMaterialForUpgrade", gen.getUpgrade().getAmount() + -getItems(p, gen.getUpgrade()), gen
                                .getUpgrade().getItemMeta().getDisplayName(), getItems(p, gen.getUpgrade()), gen.getUpgrade().getAmount());
                        return;
                    }
                    removeItem(p, gen.getUpgrade().getAmount(), gen.getUpgrade().getType());

                    if(!upgrades.contains(p))
                        upgrades.add(p);
                    gen.upgrade();
                    EggWarsMap m = gen.getMap();
                    if (EggWarsTeamManager.getTeam(m, p) != null) {
                        EggWarsTeam t = EggWarsTeamManager.getTeam(m, p);
                        t.broadcast(Utils.translate("Messages.Team.upgradeGen", t
                                .getPlayers().size() > 1 ? p.getName() : "You", gen.getType().name().charAt(0) + gen.getType().name().substring(1).toLowerCase(), gen.getColor()));
                    }
                    e.getClickedInventory().setContents(gen.getInventory().getContents());
                    p.updateInventory();
                    p.updateCommands();
                }
            }
        }
    }

    public void print(ItemStack i) {
        String a = "";
        if (i.hasItemMeta()) {
            ItemMeta im = i.getItemMeta();
            a = im.getDisplayName() + "\n" + im.getLore().toString().substring(1, im.getLore().toString().length() + -1);
        }
        System.out.println(a);
    }

    static boolean removeItem(Player user, int count, Material mat) {
        Map<Integer, ? extends ItemStack> ammo = user.getInventory().all(mat);

        int found = 0;
        for (ItemStack stack : ammo.values())
            found += stack.getAmount();
        if (count > found) {
            return false;
        }
        for (int index : ammo.keySet()) {
            ItemStack stack = ammo.get(index);

            int removed = Math.min(count, stack.getAmount());
            count -= removed;

            if (stack.getAmount() == removed) {
                user.getInventory().setItem(index, null);
            } else {
                stack.setAmount(stack.getAmount() - removed);
            }
            if (count <= 0)
                break;
        }
        user.updateInventory();
        return true;
    }

    private static int getItems(Player user, ItemStack mat) {
        ItemStack[] g = user.getInventory().getContents();

        int cuantity = 0;
        for (int i = 0; i < g.length; i++) {
            if ((g[i] != null) &&
                    (g[i].isSimilar(mat))) {
                int cant = g[i].getAmount();
                cuantity += cant;
            }
        }

        return cuantity;
    }
}
