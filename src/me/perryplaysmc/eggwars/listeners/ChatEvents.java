package me.perryplaysmc.eggwars.listeners;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatEvents implements Listener {



    @EventHandler
    void onChat(AsyncPlayerChatEvent e) {
        EggWarsMap m = MapManager.byPlayer(e.getPlayer());
        try {
            for(Player p : e.getRecipients()) {
                if(MapManager.byPlayer(p) != null) {
                    e.getRecipients().remove(p);
                }
            }
        }catch(Exception ee) {

        }
        if(m == null) return;
        e.setCancelled(true);
        if(m.getState() == GameState.LOBBY) {
            for(Player p : m.getPlayers()) {
                p.sendMessage("§cLOBBY §e" + e.getPlayer().getName() + "§7: " + e.getMessage());
            }
            return;
        }
        if(m.getMaxPlayersPerTeam() == 1) {
            EggWarsTeam t = m.getTeam(e.getPlayer());
            if(m.getDeadTeams().contains(t)) {
                for (Player p : m.getPlayers()) {
                    p.sendMessage("§7§lDEAD: " + t.getName() + "§e " + e.getPlayer().getName() + "§7: " + e.getMessage());
                }
                return;
            }
            for (Player p : m.getPlayers()) {
                p.sendMessage(t.getColor() + "§l" + t.getName() + "§e " + e.getPlayer().getName() + "§7: " + e.getMessage());
            }
            return;
        }
        EggWarsTeam t = m.getTeam(e.getPlayer());
        if(e.getMessage().startsWith("!")) {
            e.setMessage(e.getMessage().substring(1));
            if(t.getDead().contains(e.getPlayer())) {
                for (Player p : m.getPlayers()) {
                    p.sendMessage("§4§lGLOBAL: §7§l(DEAD)" + t.getColor() + "§l" + t.getName() + "§e " + e.getPlayer().getName() + "§7: " + e.getMessage());
                }
                return;
            }
            for (Player p : m.getPlayers()) {
                p.sendMessage("§4§lGLOBAL: " + t.getColor() + "§l" + t.getName() + "§e " + e.getPlayer().getName() + "§7: " + e.getMessage());
            }
            return;
        }
        for(Player p : t.getPlayers()) {
            p.sendMessage(t.getColor() + "§lTEAM §e" + e.getPlayer().getName() + "§7: " + e.getMessage());
        }
        for(Player p : t.getDead()) {
            if(t.getPlayers().contains(p))continue;
            p.sendMessage(t.getColor() + "§lTEAM §e" + e.getPlayer().getName() + "§7: " + e.getMessage());
        }
    }


}
