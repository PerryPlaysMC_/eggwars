package me.perryplaysmc.eggwars.listeners;

import java.util.HashMap;
import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.timer.Generator;
import me.perryplaysmc.eggwars.map.timer.GeneratorType;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.config.MapConfig;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignEvents implements Listener {
  @EventHandler
  void onWrite(SignChangeEvent e) {
    String[] s = Utils.getConfig().getString("Sign.lines.set").split("\\{0}");
    EggWarsMap m = MapManager.byName(e.getLine(0).replace(s[0], "").replace(s[1], ""));
    Player p = e.getPlayer();
    boolean found = false;
    String name = e.getLine(0).replace(s[0], "").replace(s[1], "");
    for (EggWarsMap map : MapManager.getMaps()) {
      if ((e.getLine(0).equalsIgnoreCase(s[0] + map.getName() + s[1])) && (map.getName().equalsIgnoreCase(name)))
        if (!map.getConfig().isSet("Locations.Sign")) {
          e.setLine(0, trans(map, "Sign.lines.line-1"));
          e.setLine(1, trans(map, "Sign.lines.line-2"));
          e.setLine(2, trans(map, "Sign.lines.line-3"));
          e.setLine(3, trans(map, "Sign.lines.line-4"));
          e.getBlock().getState().update();
          map.setSign(e.getBlock().getLocation());
          found = true;
          break;
        }
    }
    if ((m != null) && (e.getLine(0).equalsIgnoreCase(s[0] + m.getName() + s[1]))) {
      if ((found) || (m.getConfig().isSet("Locations.Sign")) || (m.getSign() != null)) {
        e.setLine(1, "All maps with");
        e.setLine(2, "that name have");
        e.setLine(3, "Signs.");
        return;
      }
      if (!Utils.hasPermission(p, "sign-place")) {
        Utils.sendMessage(p, "Error.invalidPermission", "eggwars.command.sign-place");
        return;
      }
      if (m.getSign() != null) return;
      e.setLine(0, trans(m, "Sign.lines.line-1"));
      e.setLine(1, trans(m, "Sign.lines.line-2"));
      e.setLine(2, trans(m, "Sign.lines.line-3"));
      e.setLine(3, trans(m, "Sign.lines.line-4"));
      e.getBlock().getState().update();
      m.setSign(e.getBlock().getLocation());
    } }
  
  @EventHandler
  void onBreak(BlockBreakEvent e) {
    Sign s;
    Player p;
    if (((e.getBlock().getState() instanceof Sign)) || ((e.getBlock().getRelative(BlockFace.UP).getState() instanceof Sign))) {
      if ((e.getBlock().getState() instanceof Sign)) {
        s = (Sign)e.getBlock().getState();
      } else {
        s = (Sign)e.getBlock().getRelative(BlockFace.UP).getState();
      }
      p = e.getPlayer();
      for (EggWarsMap m : MapManager.getMaps()) {
        if ((m != null) && 
        
          (m.getState() == GameState.LOBBY))
        {


          boolean a = (!(e.getBlock().getRelative(BlockFace.UP).getState() instanceof Sign)) && (m.getConfig().isSet("Locations.Sign")) && (m.getSign() != null) && (s.getX() == m.getSign().getX()) && (s.getY() == m.getSign().getY()) && (s.getZ() == m.getSign().getZ());
          if ((!Utils.hasPermission(p, "sign-break")) && (a)) {
            Utils.sendMessage(p, "Error.invalidPermission", "eggwars.command.sign-break");
            e.setCancelled(true);
            return;
          }
          if ((e.getPlayer().isSneaking()) && (m.getGenerator(s) != null)) {
            Generator gen = m.getGenerator(s);
            String name = new StringBuilder().append(gen.getType().name().charAt(0)).toString().toUpperCase() + gen.getType().name().substring(1).toLowerCase();
            Utils.sendMessage(e.getPlayer(), "Map.brokenSign", name);
            m.removeGenerator(gen);
            if ((e.getBlock().getRelative(BlockFace.UP).getState() instanceof Sign)) {
              e.getBlock().getRelative(BlockFace.UP).setType(org.bukkit.Material.AIR);
              e.getBlock().getRelative(BlockFace.UP).getState().update();
            }
            return;
          }
          if ((e.getPlayer().isSneaking()) && (a)) {
            m.setSign(null);
            Utils.sendMessage(e.getPlayer(), "Map.brokenSign", m.getName());
            break;
          }
          if ((!e.getPlayer().isSneaking()) && (m.getGenerator(s) == null) && (a)) {
            Utils.sendMessage(e.getPlayer(), "Map.mustSneakToBreakSign", m.getName());
            e.setCancelled(true);
            return; }
          if ((!e.getPlayer().isSneaking()) && (m.getGenerator(s) != null)) {
            Generator gen = m.getGenerator(s);
            String name = new StringBuilder().append(gen.getType().name().charAt(0)).toString().toUpperCase() + gen.getType().name().substring(1).toLowerCase();
            Utils.sendMessage(e.getPlayer(), "Map.mustSneakToBreakSign", name);
            e.setCancelled(true);
            return;
          }
        }
      }
    }
  }
  
  @EventHandler
  void onWriteGenerator(SignChangeEvent e) {
    String[] args = e.getLines();
    if (args[0].equalsIgnoreCase("[Generator]")) {
      if (!EggWarsCommand.getSelected().containsKey(e.getPlayer())) {
        e.setLine(1, "Invalid map selected");
        e.setLine(2, "Select one with");
        e.setLine(3, "/ew select map <EggWarsMap>");
        return;
      }
      EggWarsMap m = EggWarsCommand.getSelected().get(e.getPlayer());
      Generator gen = null;
      if (!Utils.isInt(args[2])) {
        e.setLine(2, "Invalid number");
        return;
      }
      if (!Utils.isInt(args[3])) {
        e.setLine(3, "Invalid number");
        return;
      }
      if (args[1].equalsIgnoreCase("emerald")) {
        gen = m.addGenerator(new Generator(m, (Sign)e.getBlock().getState(), Integer.parseInt(args[2]), Integer.parseInt(args[3]), GeneratorType.EMERALD));
      }
      else if (args[1].equalsIgnoreCase("diamond")) {
        gen = m.addGenerator(new Generator(m, (Sign)e.getBlock().getState(), Integer.parseInt(args[2]), Integer.parseInt(args[3]), GeneratorType.DIAMOND));
      }
      else if (args[1].equalsIgnoreCase("gold")) {
        gen = m.addGenerator(new Generator(m, (Sign)e.getBlock().getState(), Integer.parseInt(args[2]), Integer.parseInt(args[3]), GeneratorType.GOLD));
      }
      else if (args[1].equalsIgnoreCase("iron")) {
        gen = m.addGenerator(new Generator(m, (Sign)e.getBlock().getState(), Integer.parseInt(args[2]), Integer.parseInt(args[3]), GeneratorType.IRON));
      } else {
        e.setLine(2, "Invalid gen type");
        return;
      }
      String name = (""+gen.getType().name().charAt(0)).toUpperCase() + gen.getType().name().substring(1).toLowerCase();
      e.setLine(0, Utils.translate("Map-Settings.Defaults.Generator.Line-1", name, gen.getLevel(), gen.getMax(), gen.getSpeed(), gen.getColor()));
      e.setLine(1, Utils.translate("Map-Settings.Defaults.Generator.Line-2", name, gen.getLevel(), gen.getMax(), gen.getSpeed(), gen.getColor()));
      e.setLine(2, Utils.translate("Map-Settings.Defaults.Generator.Line-3", name, gen.getLevel(), gen.getMax(), gen.getSpeed(), gen.getColor()));
      e.setLine(3, Utils.translate("Map-Settings.Defaults.Generator.Line-4", name, gen.getLevel(), gen.getMax(), gen.getSpeed(), gen.getColor()));
      gen.updateSign(true);
    }
  }
  
  @EventHandler
  void onClick(PlayerInteractEvent e) {
    Sign s;
    if ((e.getAction() == org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK) && 
      ((e.getClickedBlock().getState() instanceof Sign))) {
      s = (Sign)e.getClickedBlock().getState();
      Player p = e.getPlayer();
      for (EggWarsMap m : MapManager.getMaps()) {
        if ((m != null) && (m.getState() == GameState.LOBBY) && (m.getConfig().isSet("Locations.Sign")) && (m.getSign() != null)) {
          if ((s.getX() == m.getSign().getX()) && 
            (s.getY() == m.getSign().getY()) && 
            (s.getZ() == m.getSign().getZ())) {
            m.addPlayer(e.getPlayer());
            m.updateSign();
            break;
          }
        }
      }
    }
  }
  
  String trans(EggWarsMap m, String str) {
    return Utils.translate(str, m.getName(), (m.getMinPlayers()), (m.getMaxPlayers()),
            (m.getPlayers().size()),
            (m.getMaxPlayersPerTeam()), m.getState().getLocalizedName());
  }
}
