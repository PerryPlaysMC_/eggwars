package me.perryplaysmc.eggwars.listeners;

import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.map.team.EggWarsTeamManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class PlayerDeath implements Listener {

    @EventHandler
    void onDamage(EntityDamageEvent e) {
        if ((e.getEntity() instanceof Player)) {
            Player p = (Player)e.getEntity();
            EggWarsMap m = MapManager.byPlayer(p);
            if ((m != null) && ((m.getState() == GameState.END) || (m.getState() == GameState.LOBBY)))
                e.setCancelled(true);
        }
        if (e.getEntity() instanceof Villager) {
            Villager v = (Villager)e.getEntity();
            for (EggWarsMap m : MapManager.getMaps()) {
                if (m.getVillagers().contains(v)) {
                    e.setCancelled(true);
                    return;
                }
            }
        }
    }

    HashMap<Player, Player> damage = new HashMap<>();

    @EventHandler
    void onDmg(EntityDamageByEntityEvent e) {
        if(e.getDamager() instanceof Player){
            if(e.getEntity()instanceof Player) {
                damage.put((Player)e.getEntity(), (Player)e.getDamager());
                (new BukkitRunnable() {
                    @Override
                    public void run() {
                        damage.remove((Player)e.getEntity());
                    }
                }).runTaskLater(EggWars.getInstance(), 20*10);
            }
        }
    }

    @EventHandler
    void move(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if ((m != null) && (e.getFrom().getBlockX() != e.getTo().getBlockX()) && (e.getFrom().getBlockZ() != e.getTo().getBlockZ()) &&
                (p.getLocation().distance(m.getRespawnLoc()) > 200.0D)) {
            if (m.getState() == GameState.LOBBY) {
                p.teleport(m.getLobbyLoc());
                return;
            }
            p.teleport(m.getRespawnLoc());
        }

        if ((e.getPlayer().getLocation().getBlockY() <= -5) &&
                (m != null)) {
            if (m.getState() == GameState.LOBBY) {
                p.teleport(m.getLobbyLoc());
                return;
            }
            EggWarsTeam t = EggWarsTeamManager.getTeam(m, e.getPlayer());
            if ((t == null) || (t.getDead().contains(p))) {
                p.teleport(m.getRespawnLoc());
                return;
            }
            Player killer = damage.getOrDefault(e.getPlayer(), null);
            t.respawnPlayer(e.getPlayer(), killer, true);
        }
    }

    @EventHandler
    void onJoin(PlayerJoinEvent e) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (MapManager.byPlayer(p) != null) {
                e.getPlayer().hidePlayer(p);
                p.hidePlayer(e.getPlayer());
            }
        }
    }

    @EventHandler
    void onLeave(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if (m != null) {
            m.removePlayer(p);
        }
    }

    @EventHandler
    void onDeath(PlayerDeathEvent e) {
        EggWarsMap m = MapManager.byPlayer(e.getEntity());
        if (m != null) {
            EggWarsTeam t = EggWarsTeamManager.getTeam(m, e.getEntity());
            if (t == null) {
                return;
            }
            t.respawnPlayer(e.getEntity());
        }
    }
}
