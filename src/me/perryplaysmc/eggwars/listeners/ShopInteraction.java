package me.perryplaysmc.eggwars.listeners;

import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.config.Config;
import me.perryplaysmc.eggwars.utils.inventory.Gui;
import me.perryplaysmc.eggwars.utils.inventory.Inventory;
import me.perryplaysmc.eggwars.utils.inventory.InventoryManager;
import me.perryplaysmc.eggwars.utils.items.Items;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.material.Wool;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

@SuppressWarnings("all")
public class ShopInteraction implements Listener {
    private static Config cfg = Utils.getInventoryMain();

    private HashMap<Player, BukkitTask> r = new HashMap<>();
    private Inventory main = new Inventory(cfg.getInt("InventorySize"), Utils.translate(cfg.getString("Title")));

    public void loadItems(EggWarsTeam t) {
        main = new Inventory(cfg.getInt("InventorySize"), Utils.translate(cfg.getString("Title")));
        loadItems1(main, t);
    }

    public static void loadItems1(Inventory main, EggWarsTeam t) {
        for (String a : cfg.getSection("Items").getKeys(false)) {
            String m = cfg.getString("Items." + a + ".Type.Material");
            ItemStack item;
            if(m.equalsIgnoreCase("Bridge")) {
                item = new ItemStack(Material.BRICKS);
                ItemMeta im = item.getItemMeta();
                im.setDisplayName(Utils.translate("[c]Bridge"));
                im.setLore(Collections.singletonList(Utils.translate("[c]Use this to make an instant bridge!")));
                item.setItemMeta(im);
            }else
                item = new ItemStack(Material.getMaterial(m), cfg.getInt("Items." + a + ".Type.Amount"));
            if (m.contains("HARD_CLAY") || m.contains("TERRACOTTA")) {
                DyeColor color = null;
                String c = cfg.getString("Items." + a + ".Type.Color");
                if(cfg.isSet("Items." + a + ".Type.Color"))
                    if(c.contains("{TEAM_COLOR}")) color = t.getDyeColor(); else DyeColor.valueOf(c);
                else color = DyeColor.WHITE;
                item = new ItemStack(Utils.terracottaFromDye(color), cfg.getInt("Items." + a + ".Type.Amount"));
            }
            if (m.contains("WOOL")) {
                DyeColor color = null;
                if(cfg.isSet("Items." + a + ".Type.Color"))
                    if (cfg.getString("Items." + a + ".Type.Color").contains("{TEAM_COLOR}")) color = t.getDyeColor(); else DyeColor.valueOf(cfg.getString("Items." + a + ".Type.Color"));
                else color = DyeColor.WHITE;
                item = new Wool(color).toItemStack(cfg.getInt("Items." + a + ".Type.Amount"));
            }
            ItemMeta im = item.getItemMeta();
            im.setDisplayName(Utils.translate(cfg.getString("Items." + a + ".Name")));
            List<String> lore = new ArrayList<>();
            String b; for (Iterator localIterator2 = cfg.getStringList("Items." + a + ".Lore").iterator(); localIterator2.hasNext(); lore.add(Utils.translate(b))) b = (String)localIterator2.next();
            im.setLore(lore);
            item.setItemMeta(im);
            main.setItem(cfg.getInt("Items." + a + ".Slot"), item);
        }
    }

    void load(Gui gui, EggWarsMap m, Player p){
        gui.setTeam(m.getTeam(p) == null ? m.getTeams().get(0) : m.getTeam(p));
        gui.loadItems();
        Inventory inv = gui.getInventory();
        Inventory x = new Inventory(inv.getSize(), inv.getName());
        x.setContents(inv.getContents());
        p.openInventory(x.getInventory());
        r.put(p, new BukkitRunnable() {
            public void run() {
                for (int ii = 0; ii < inv.getSize(); ii++) {
                    Config cfg = gui.getConfig();
                    for (String a : cfg.getSection("Items").getKeys(false)) {
                        if (cfg.getInt("Items." + a + ".Slot") == ii) {
                            ItemStack i = inv.getItem(ii);
                            if(i == null || i.getType() == Material.AIR || Material.getMaterial(cfg.getString("Items." + a + ".Buy.Type")) == null) continue;
                            ItemMeta im = i.getItemMeta();
                            if (ShopInteraction.getItems(p,
                                    new ItemStack(Material.getMaterial(cfg.getString("Items." + a + ".Buy.Type")))) >= cfg.getInt("Items." + a + ".Buy.Cost")) {
                                im.addEnchant(EggWars.getInstance().ench, 1, true);
                            } else {
                                im.removeEnchant(EggWars.getInstance().ench);
                            }
                            i.setItemMeta(im);
                            inv.setItem(ii, i);
                        }
                    }
                }
            }
        }.runTaskTimer(EggWars.getInstance(), 0L, 2L));
    }

    @EventHandler
    void onClick(InventoryClickEvent e) {
        EggWarsMap m = MapManager.byPlayer((Player)e.getWhoClicked());
        Player p = (Player)e.getWhoClicked();
        if (m != null) {
            if ((e.getWhoClicked().getOpenInventory().getTopInventory().toString().equalsIgnoreCase(main.getName())) &&
                    (e.getAction() == org.bukkit.event.inventory.InventoryAction.MOVE_TO_OTHER_INVENTORY)) e.setCancelled(true);
            if ((e.getClickedInventory() != null) && (new Inventory(e.getView()).getName().equalsIgnoreCase(main.getName()))) {
                e.setCancelled(true);
                for (String a : cfg.getSection("Items").getKeys(false)) {
                    if ((e.getSlot() == cfg.getInt("Items." + a + ".Slot")) || (e.getRawSlot() == cfg.getInt("Items." + a + ".Slot"))) {
                        if(!cfg.isSet("Items." + a + ".gui")) continue;
                        Gui gui = InventoryManager.getGui(cfg.getString("Items." + a + ".gui"));
                        if (gui == null) break;
                        load(gui, m, p);
                        break;
                    }
                }
            }
        }
    }

    @EventHandler
    void close(InventoryCloseEvent e) {
        Player p = (Player)e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if ((m != null) &&
                (r.containsKey(p))) {
            r.get(p).cancel();
        }
    }

    @EventHandler
    void itemShop(InventoryClickEvent e) {
        EggWarsMap m = MapManager.byPlayer((Player)e.getWhoClicked());
        Player p = (Player)e.getWhoClicked();
        if (m != null) {
            if ((e.getWhoClicked().getOpenInventory().getTopInventory() != null) &&
                    (new Inventory(e.getView()).getName().equalsIgnoreCase(main.getName())) &&
                    (e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)) e.setCancelled(true);
            if(e.getCurrentItem() != null && e.getSlot() == (e.getClickedInventory().getSize() +- 5) && e.getCurrentItem().isSimilar(Items.getBook()) && r.containsKey(p)) {
                loadItems(m.getTeam(p));
                p.openInventory(main.getInventory());
                r.remove(p);
                return;
            }
            for (Gui gui : InventoryManager.getGuis()) {
                if ((e.getWhoClicked().getOpenInventory().getTopInventory() != null) &&
                        (new Inventory(e.getView()).getName().equalsIgnoreCase(gui.getInventory().getName())) &&
                        (e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY)) e.setCancelled(true);
                if ((e.getClickedInventory() != null) && (new Inventory(e.getView()).getName().equalsIgnoreCase(gui.getDisplayName()))) {
                    Config cfg = gui.getConfig();
                    e.setCancelled(true);
                    for (String a : cfg.getSection("Items").getKeys(false)) {
                        if ((e.getSlot() == cfg.getInt("Items." + a + ".Slot")) || (e.getRawSlot() == cfg.getInt("Items." + a + ".Slot"))) {
                            if (e.getClick().isShiftClick()) {
                                while (getItems(p, new ItemStack(Material.getMaterial(cfg.getString("Items." + a + ".Buy.Type")))) >= cfg.getInt("Items." + a + ".Buy.Cost")) {
                                    remove(cfg, e.getCurrentItem(), a, gui, p);
                                    e.setCancelled(true);
                                    if (p.getOpenInventory().getTopInventory() != null) if (p.getOpenInventory().getTopInventory() != e.getClickedInventory()) break;
                                }
                                return;
                            }
                            if (getItems(p, new ItemStack(Material.getMaterial(cfg.getString("Items." + a + ".Buy.Type")))) < cfg.getInt("Items." + a + ".Buy.Cost")) break;
                            remove(cfg, e.getCurrentItem(), a, gui, p);
                            e.setCancelled(true);
                        }
                    }


                    break;
                }
            }
        }
    }

    void remove(Config cfg, ItemStack imeta, String a, Gui gui, Player p) {
        String mm = cfg.getString("Items." + a + ".Type.Material");
        ItemStack item;
        if(mm.equalsIgnoreCase("Bridge")) {
            item = new ItemStack(Material.BRICKS);
            ItemMeta im = item.getItemMeta();
            im.setDisplayName(Utils.translate("[c]Bridge"));
            im.setLore(Collections.singletonList(Utils.translate("[c]Use this to make an instant bridge!")));
            item.setItemMeta(im);
        }else
            item = new ItemStack(Material.getMaterial(mm), cfg.getInt("Items." + a + ".Type.Amount"));
        if (mm.contains("HARD_CLAY")) {
            DyeColor color = null;
            if(cfg.isSet("Items." + a + ".Type.Color"))
                if (cfg.getString("Items." + a + ".Type.Color").contains("{TEAM_COLOR}"))
                    color = gui.getTeam().getDyeColor(); else DyeColor.valueOf(cfg.getString("Items." + a + ".Type.Color"));
            else color = DyeColor.WHITE;
            item = new ItemStack(Utils.terracottaFromDye(color), cfg.getInt("Items." + a + ".Type.Amount"));
        }
        if (item.getType().name().contains("WOOL")) {
            DyeColor color = null;
            if(cfg.isSet("Items." + a + ".Type.Color"))
                if (cfg.getString("Items." + a + ".Type.Color").contains("{TEAM_COLOR}"))
                    color = gui.getTeam().getDyeColor(); else DyeColor.valueOf(cfg.getString("Items." + a + ".Type.Color"));
            else color = DyeColor.WHITE;
            item = new Wool(color).toItemStack(cfg.getInt("Items." + a + ".Type.Amount"));
        }
        if (item.getType().name().contains("LEATHER")) {
            item = new ItemStack(Material.getMaterial(mm), cfg.getInt("Items." + a + ".Type.Amount"));
            LeatherArmorMeta im = (LeatherArmorMeta) item.getItemMeta();
            im.setColor(((LeatherArmorMeta)imeta.getItemMeta()).getColor());

        }
        if (cfg.isSet("Items." + a + ".Enchantments")) {
            ItemMeta im = item.getItemMeta();
            for (String x : cfg.getStringList("Items." + a + ".Enchantments"))
                if (x.contains(",")) {
                    String[] sp = x.split(",");

                    Enchantment ench =
                            Enchantment.getByKey(org.bukkit.NamespacedKey.minecraft(sp[0].toLowerCase())) != null ?
                                    Enchantment.getByKey(org.bukkit.NamespacedKey.minecraft(sp[0].toLowerCase())) : Enchantment.getByName(sp[0].toUpperCase());
                    if (ench == null) {
                        System.out.println("Invalid enchantment '" + sp[0] + "'");
                    }
                    else {
                        try {
                            Integer.parseInt(sp[1]);
                        } catch (Exception e) {
                            System.out.println("Invalid enchantment level '" + sp[1] + "'");
                            break;
                        }
                        if (Integer.parseInt(sp[1]) < 1) {
                            System.out.println("Invalid enchantment level '" + sp[1] + "' must be higher than 0");
                        }
                        else
                            im.addEnchant(ench, Integer.parseInt(sp[1]), true);
                    }
                }
            item.setItemMeta(im);
        }
        removeItem(p, cfg.getInt("Items." + a + ".Buy.Cost"), Material.getMaterial(cfg.getString("Items." + a + ".Buy.Type")));
        addItem(p, cfg.getInt("Items." + a + ".Type.Amount"), item);
    }

    @EventHandler
    void onInteract(PlayerInteractAtEntityEvent e) {
        Player p = e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if (((e.getRightClicked() instanceof Villager)) &&
                (m != null)) {
            Villager v = (Villager)e.getRightClicked();
            e.setCancelled(true);
            if (m.getVillagers().contains(v)) {
                v.setLootTable(null);
                v.setRecipes(new ArrayList<>());
                p.closeInventory();
                new org.bukkit.scheduler.BukkitRunnable()
                {
                    public void run() {
                        loadItems(m.getTeam(p) == null ? m.getTeams().get(0) : m.getTeam(p));
                        p.openInventory(main.getInventory()
                        );
                    }
                }.runTaskLater(EggWars.getInstance(), 1L);
            }
        }
    }

    public void addItem(Player user, int amount, ItemStack item) {
        HashMap<Integer, ItemStack> excess = user.getInventory().addItem(item);
        for (java.util.Map.Entry<Integer, ItemStack> me : excess.entrySet())
            user.getWorld().dropItem(user.getLocation(), me.getValue());
    }

    public static boolean removeItem(Player user, int count, Material mat) {
        Map<Integer, ? extends ItemStack> ammo = user.getInventory().all(mat);

        int found = 0;
        for (ItemStack stack : ammo.values())
            found += stack.getAmount();
        if (count > found) {
            return false;
        }
        int i = 1;
        for (Integer index : ammo.keySet()) {
            ItemStack stack = ammo.get(index);

            int removed = Math.min(count, stack.getAmount());
            count -= removed;
            if (stack.getAmount() == removed) {
                user.getInventory().setItem(index, null);
            } else {
                stack.setAmount(stack.getAmount() - removed);
            }
            if (count <= 0)
                break;
        }
        user.updateInventory();
        return true;
    }

    private static int getItems(Player user, ItemStack mat) {
        ItemStack[] g = user.getInventory().getContents();

        int cuantity = 0;
        for (int i = 0; i < g.length; i++) {
            if ((g[i] != null) && (
                    (g[i].isSimilar(mat)) || (g[i].getType() == mat.getType()))) {
                int cant = g[i].getAmount();
                cuantity += cant;
            }
        }

        return cuantity;
    }
}
