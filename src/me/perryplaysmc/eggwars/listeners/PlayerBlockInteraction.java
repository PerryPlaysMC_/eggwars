package me.perryplaysmc.eggwars.listeners;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.config.Config;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.block.data.type.TNT;
import org.bukkit.entity.*;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class PlayerBlockInteraction implements Listener {

    public static HashMap<EggWarsMap, HashMap<Block, BlockState>> placed = new HashMap<>();
    public static HashMap<EggWarsMap, List<Block>> blocks = new HashMap<>();

    @EventHandler
    void onEggClick(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if ((m != null) &&
                ((e.getAction() == Action.RIGHT_CLICK_BLOCK) || (e.getAction() == Action.LEFT_CLICK_BLOCK)) &&
                (e.getClickedBlock().getType() == Material.DRAGON_EGG)) {
            e.setCancelled(true);
            EggWarsTeam te = m.getTeam(p);
            if(Utils.checkLocations(te.getEgg(), e.getClickedBlock().getLocation())) {
                p.sendMessage("§cYou can't break your own egg");
                e.setCancelled(true);
                e.setUseInteractedBlock(Event.Result.DENY);
                e.getClickedBlock().setType(Material.DRAGON_EGG);
                e.getClickedBlock().getState().update(true);
                return;
            }
            for (EggWarsTeam t : m.getTeams()) {
                if ((Utils.checkLocations(t.getEgg(), e.getClickedBlock().getLocation())) && (m.getState() == GameState.RUNNING)) {
                    if (t.getPlayers().contains(p)) {
                        p.sendMessage("§cYou can't break your own egg");
                        e.setCancelled(true);
                        e.setUseInteractedBlock(Event.Result.DENY);
                        e.getClickedBlock().setType(Material.DRAGON_EGG);
                        e.getClickedBlock().getState().update(true);
                        return;
                    }
                    t.getEgg().getBlock().setType(Material.AIR);
                    t.getEgg().getBlock().getState().update(true);
                    for (Player aa : m.getPlayers()) {
                        if (!t.getPlayers().contains(aa)) {
                            Utils.sendMessageAll(aa, t.getColor() + t.getName() + " &cTeam's &eEgg has been broken! By: &c" + e.getPlayer().getName());
                        } else {
                            t.broadcast("&4&lYOUR EGG HAS BEEN BROKEN! BY " + e.getPlayer().getName());
                        }
                    }
                    break;
                }
            }
        }
    }

    HashMap<EggWarsMap, TNTPrimed> tnt = new HashMap<>();

    @EventHandler
    void onTntExlpode(EntityExplodeEvent e) {
        if(e.getEntity() instanceof TNTPrimed) {
            for(EggWarsMap map : MapManager.getMaps()) {
                if(!tnt.containsKey(map)) continue;
                if(map.getPlayers() != null) {
                    if(map.getState() != GameState.LOBBY) {
                        if(e.blockList().isEmpty()) {
                            return;
                        }
                        for(Block block : e.blockList()) {
                            if(placed.get(map).containsKey(block))
                                block.breakNaturally();
                        }
                    }else {
                        e.setCancelled(true);
                    }
                    e.blockList().clear();
                }
            }
        }
    }

    @EventHandler
    void onPlace(BlockPlaceEvent e) {
        Player p = e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if (m != null) {
            if (isNearGeneratorOrSpawn(m, e.getBlockPlaced().getLocation())) {
                e.setCancelled(true);
                Utils.sendMessage(e.getPlayer(), "Map.tooCloseToGeneratorOrSpawn");
                return;
            }
            HashMap<Block, BlockState> pa = placed.containsKey(m) ? placed.get(m) : new HashMap<>();
            List<Block> a = !blocks.containsKey(m) || blocks.get(m) == null ? new ArrayList<>() : blocks.get(m);
            if(e.getBlock().getType() == Material.TNT) {
                TNTPrimed t = e.getBlock().getWorld().spawn(e.getBlock().getLocation().add(0.5, 0, 0.5), TNTPrimed.class);
                t.setVelocity(new Vector(0,0,0));
                tnt.put(m, t);
                e.getBlock().setType(Material.AIR);
                e.getBlock().getState().update(true);
                return;
            }
            if(e.getItemInHand()!=null && e.getItemInHand().getType()==Material.BRICKS &&
                    e.getItemInHand().getItemMeta().getDisplayName().equalsIgnoreCase(Utils.translate("[c]Bridge"))) {
                int i = 0;
                Block b = e.getBlock().getRelative(BlockFace.DOWN);
                if(!p.getInventory().contains(Utils.terracottaFromDye(m.getTeam(p).getDyeColor()))) {
                    String color = m.getTeam(p).getDyeColor().name().charAt(0) + m.getTeam(p).getDyeColor().name().substring(1).toLowerCase();
                    p.sendMessage(Utils.translate("[c]You don't have any " + color + " Clay!"));
                    e.setCancelled(true);
                    return;
                }
                if(b.getRelative(Utils.getBlockFaceOpposite(p).getOppositeFace()).getType() == Material.AIR) {
                    while(i <= 10 && p.getInventory().contains(Utils.terracottaFromDye(m.getTeam(p).getDyeColor()))
                            && b.getRelative(Utils.getBlockFaceOpposite(p).getOppositeFace()).getType() == Material.AIR) {
                        b = b.getRelative(Utils.getBlockFaceOpposite(p).getOppositeFace());
                        BlockState s = b.getState();
                        b.setType(Utils.terracottaFromDye(m.getTeam(p).getDyeColor()));
                        b.getState().update(true);
                        pa.put(b, s);
                        a.add(b);
                        ShopInteraction.removeItem(p, 1, Utils.terracottaFromDye(m.getTeam(p).getDyeColor()));
                        i++;
                    }
                }else {
                    e.setCancelled(true);
                    return;
                }
                blocks.put(m, a);
                placed.put(m, pa);
                e.getBlock().setType(Material.AIR);
                return;
            }
            pa.put(e.getBlockPlaced(), e.getBlockReplacedState());
            a.add(e.getBlock());
            blocks.put(m, a);
            placed.put(m, pa);
        }
    }

    @EventHandler
    void onBreak(BlockBreakEvent e) {
        Player p = e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if (m != null) {
            if (e.getBlock().getType() == Material.DRAGON_EGG) return;
            List<Block> a = !blocks.containsKey(m) || blocks.get(m) == null ? new ArrayList<>() : blocks.get(m);
            if (!a.contains(e.getBlock())) {
                e.setCancelled(true);
                Utils.sendMessage(e.getPlayer(), "Map.cantBreakMapBlock");
                return;
            }
            blocks.remove(a);
            blocks.put(m, a);
        }
    }

    boolean isNearGeneratorOrSpawn(EggWarsMap m, Location l) {
        for (Block b : getNearbyBlocks(l, Utils.getConfig().getInt("Map-Settings.antiBuildRadius") + -2)) {
            Sign s = null;
            if ((b.getState() instanceof Sign)) {
                s = (Sign)b.getState();
            }
            if ((b.getRelative(BlockFace.UP).getState() instanceof Sign)) {
                s = (Sign)b.getRelative(BlockFace.UP).getState();
            }
            if (s != null) {
                return m.getGenerator(s) != null;
            }
            for(Villager v : m.getVillagers()) {
                if(l.distance(v.getLocation()) < Utils.getConfig().getInt("Map-Settings.antiBuildRadius")) return true;
            }
            for (Location a : m.getRespawnLocations()) {
                if (l.distance(a) < Utils.getConfig().getInt("Map-Settings.antiBuildRadius")) {
                    return true;
                }
            }
        }

        return false;
    }

    List<Block> getNearbyBlocks(Location location, int radius) {
        List<Block> blocks = new ArrayList<>();
        for (int x = location.getBlockX() - radius; x <= location.getBlockX() + radius; x++) {
            for (int y = location.getBlockY() - radius; y <= location.getBlockY() + radius; y++) {
                for (int z = location.getBlockZ() - radius; z <= location.getBlockZ() + radius; z++) {
                    blocks.add(location.getWorld().getBlockAt(x, y, z));
                }
            }
        }
        return blocks;
    }
}
