package me.perryplaysmc.eggwars.listeners;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.inventory.Inventory;
import me.perryplaysmc.eggwars.utils.items.Items;
import me.perryplaysmc.eggwars.utils.items.Kits;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.material.Wool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SuppressWarnings("all")
public class LobbyEvents implements Listener {
    private Inventory team;
    private int s = Kits.kits.size();
    private Inventory kit;

    public void loadTeams(EggWarsMap m) { team(m); }


    List<Kits.KitHolder> kits = new ArrayList<>();

    public void loadKit() {
        int x = 9;
        if(s <= 54) x = 54;
        if(s <= 45) x = 45;
        if(s <= 27) x = 27;
        if(s <= 18) x = 18;
        if(s <= 9) x = 9;
        kit = new Inventory(x, "§bKit Selection");
        for(Kits.KitHolder k : Kits.kits) {
            if(!kits.contains(k)) {
                kits.add(k);
            }
            if(!kit.contains(k.getDisplay()))
            kit.addItem(k.getDisplay());
        }
    }

    @EventHandler
    void onHunger(FoodLevelChangeEvent e) {
        if(!(e.getEntity() instanceof Player))return;
        EggWarsMap m = MapManager.byPlayer((Player)e.getEntity());
        if((m != null) && ((m.getState() == GameState.LOBBY) || (m.getState() == GameState.END))) {
            e.setCancelled(true);
            e.setFoodLevel(800);
            ((Player)e.getEntity()).setFoodLevel(800);
            ((Player)e.getEntity()).setSaturation(8060.0F);
            e.getEntity().setHealth(e.getEntity().getMaxHealth());
        }
    }
    @EventHandler
    void onMove(PlayerMoveEvent e) {
        EggWarsMap m = MapManager.byPlayer((Player)e.getPlayer());
        if((m != null) && (m.getState() == GameState.PRE_START)) {
            if(e.getTo().getBlockX()!=e.getFrom().getBlockX() || e.getTo().getBlockZ() != e.getFrom().getBlockZ()) e.setTo(e.getFrom());
        }
    }

    @EventHandler
    void onInteract(PlayerInteractEvent e) {
        EggWarsMap m = MapManager.byPlayer(e.getPlayer());
        if(m != null) {
            loadTeams(m);
            loadKit();
            if(e.getAction().name().contains("RIGHT")) {
                ItemStack i = Items.getLobbyItems()[0];
                ItemStack i2 = Items.getLobbyItems()[1];
                if((e.getItem() != null) && (e.getItem().isSimilar(i)))
                    e.getPlayer().openInventory(team.getInventory());
                if((e.getItem() != null) && (e.getItem().isSimilar(i2))) {
                    e.getPlayer().openInventory(kit.getInventory());
                }
            }
        }
    }

    @EventHandler
    void playerDrop(PlayerDropItemEvent e) {
        Player p = e.getPlayer();
        EggWarsMap m = MapManager.byPlayer(p);
        if((m != null) && ((m.getState() == GameState.LOBBY) || (m.getState() == GameState.END))) e.setCancelled(true);
    }

    @EventHandler
    void onClick2(InventoryClickEvent e) {
        EggWarsMap m = MapManager.byPlayer((Player)e.getWhoClicked());
        Player p = (Player)e.getWhoClicked();
        if(m != null) {
            if((p.getOpenInventory().getTopInventory() == null) && ((m.getState() == GameState.LOBBY) || (m.getState() == GameState.END))) e.setCancelled(true);
            if((e.getAction() == InventoryAction.MOVE_TO_OTHER_INVENTORY) &&p.getOpenInventory().getTopInventory()!=null&&
                    (p.getOpenInventory().getTitle().equalsIgnoreCase("§bKit Selection"))) e.setCancelled(true);
            if((e.getClickedInventory() != null) && (new Inventory(e.getView()).getName().equalsIgnoreCase("§bKit Selection"))) {
                e.setCancelled(true);
                for(Kits.KitHolder k : Kits.kits) {
                    if(e.getCurrentItem().isSimilar(k.getDisplay())) {
                        m.kits().put(p, k);
                        Utils.sendMessage(p, "Map.selectKit", k.getName());
                        break;
                    }
                }
            }
        }
    }

    @EventHandler
    void onClick(InventoryClickEvent e) {
        EggWarsMap m = MapManager.byPlayer((Player)e.getWhoClicked());
        Player p = (Player)e.getWhoClicked();
        List<EggWarsTeam> add; if((m != null) &&
                (e.getClickedInventory() != null) && (new Inventory(e.getView()).getName().equalsIgnoreCase("§bTeam Selection"))) {
            e.setCancelled(true);
            add = new ArrayList<>();
            for(EggWarsTeam t : m.getTeams()) {
                if(!add.contains(t)) {
                    add.add(t);
                    ItemStack te = new Wool(t.getDyeColor()).toItemStack(1);
                    ItemMeta im = te.getItemMeta();
                    im.setDisplayName(t.getColor() + t.getName() + "§7(§8" + t.getPlayers().size() + "§7/§8" + m.getMaxPlayersPerTeam() + "§7)");
                    List<String> names = new ArrayList<>();
                    if(t.getPlayers().size() > 0) {
                        for(Player p2 : t.getPlayers())
                            names.add("§7- §9§l" + p2.getDisplayName());
                    }
                    Collections.sort(names);
                    im.setLore(names);
                    te.setItemMeta(im);
                    if(e.getCurrentItem().isSimilar(te)) {
                        if(m.getTeam(p) != null) {
                            m.getTeam(p).getPlayers().remove(p);
                        }
                        t.joinTeam(p);
                        team(m);
                        im.setDisplayName(t.getColor() + t.getName() + "§7(§8" + t.getPlayers().size() + "§7/§8" + m.getMaxPlayersPerTeam() + "§7)");
                        names = new ArrayList<>();
                        if(t.getPlayers().size() > 0) {
                            for(Player p2 : t.getPlayers())
                                names.add("§7- §9§l" + p2.getDisplayName());
                        }
                        Collections.sort(names);
                        im.setLore(names);
                        te.setItemMeta(im);
                        e.setCurrentItem(te);
                        e.getClickedInventory().setItem(e.getSlot(), te);
                        p.updateInventory();
                        break;
                    }
                }
            }
        }
    }

    public void team(EggWarsMap m) {
        int s = m.getTeams().size();
        int x = 9;
        if(s <= 54) x = 54;
        if(s <= 45) x = 45;
        if(s <= 27) x = 27;
        if(s <= 18) x = 18;
        if(s <= 9) x = 9;
        if(team == null)
            team = new Inventory(x,"§bTeam Selection");
        List<EggWarsTeam> add = new ArrayList<>();
        List<ItemStack> it = new ArrayList<>();
        for(EggWarsTeam t : m.getTeams())
            if(!add.contains(t)) {
                add.add(t);
                ItemStack te = new Wool(t.getDyeColor()).toItemStack(1);
                ItemMeta im = te.getItemMeta();
                im.setDisplayName(t.getColor() + t.getName() + "§7(§8" + t.getPlayers().size() + "§7/§8" + m.getMaxPlayersPerTeam() + "§7)");
                List<String> names = new ArrayList<>();
                if(t.getPlayers().size() > 0) {
                    for(Player p : t.getPlayers())
                        names.add("§7- §9§l" + p.getDisplayName());
                }
                Collections.sort(names);
                im.setLore(names);
                te.setItemMeta(im);
                it.add(te);
            }
        team.setContents(it.toArray(new ItemStack[it.size()]));
    }
}
