package me.perryplaysmc.eggwars.utils.placeholders;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import me.perryplaysmc.eggwars.EggWarsAPI;
import org.bukkit.OfflinePlayer;

public class EggWarsPlaceHolder extends PlaceholderExpansion {

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public String getIdentifier() {
        return "eggwars";
    }

    @Override
    public String getAuthor() {
        return "PerryPlaysMC";
    }

    @Override
    public String getVersion() {
        return "v1.0";
    }



    @Override
    public String onRequest(OfflinePlayer player, String identifier){

        if(identifier.equals("time_played")){
            return EggWarsAPI.getFormattedPlayerTime(player);
        }

        if(identifier.equals("games_played")){
            return String.valueOf(EggWarsAPI.getGamesPlayed(player));
        }

        if(identifier.equals("solo_wins_count") || identifier.equals("solo_wins")){
            return String.valueOf(EggWarsAPI.getSoloWins(player));
        }

        if(identifier.equals("team_wins_count") || identifier.equals("team_wins")){
            return String.valueOf(EggWarsAPI.getTeamWins(player));
        }

        return "Error invalid identifier '" + identifier + "'\n" +
                "Valid identifiers: [games_played, time_played, solo_wins_count, team_wins_count, solo_wins, team_wins]";
    }

}
