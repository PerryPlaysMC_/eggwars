package me.perryplaysmc.eggwars.utils;

import org.bukkit.inventory.ItemStack;

public class EggWarsInventory {
    
    private ItemStack[] contents, storageContents, armorContents, extraContents;

    public EggWarsInventory(ItemStack[] contents, ItemStack[] storageContents, ItemStack[] armorContents, ItemStack[] extraContents) {
        this.contents = contents;
        this.storageContents = storageContents;
        this.armorContents = armorContents;
        this.extraContents = extraContents;
    }

    public ItemStack[] getContents() {
        return contents;
    }

    public ItemStack[] getStorageContents() {
        return storageContents;
    }

    public ItemStack[] getArmorContents() {
        return armorContents;
    }

    public ItemStack[] getExtraContents() {
        return extraContents;
    }
}
