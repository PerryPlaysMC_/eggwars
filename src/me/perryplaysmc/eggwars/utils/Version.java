package me.perryplaysmc.eggwars.utils;

import org.bukkit.Bukkit;

public class Version {

    public static Versions getVersionExact() {
        String pack =  Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.')+1).replaceFirst("v", "");
        Versions ret = Versions.NULL;
        switch(version) {
            case "1_8_R1": {
                ret = Versions.v1_8_1;
                break;
            }
            case "1_8_R2": {
                ret = Versions.v1_8_2;
                break;
            }
            case "1_8_R3": {
                ret = Versions.v1_8_3;
                break;
            }
            case "1_9_R1": {
                ret = Versions.v1_9_1;
                break;
            }
            case "1_9_R2": {
                ret = Versions.v1_9_2;
                break;
            }
            case "1_10_R1": {
                ret = Versions.v1_10_1;
                break;
            }
            case "1_11_R1": {
                ret = Versions.v1_11_1;
                break;
            }
            case "1_12_R1": {
                ret = Versions.v1_12_1;
                break;
            }
            case "1_13_R1": {
                ret = Versions.v1_13_1;
                break;
            }
            case "1_13_R2": {
                ret = Versions.v1_13_2;
                break;
            }
            case "1_14_R1": {
                ret = Versions.v1_14_1;
                break;
            }
        }
        return ret;
    }
    public static Versions getVersion() {
        String pack =  Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.')+1).replaceFirst("v", "");
        Versions ret = Versions.NULL;
        switch(version) {
            case "1_8_R1": {
                ret = Versions.v1_8;
                break;
            }
            case "1_8_R2": {
                ret = Versions.v1_8;
                break;
            }
            case "1_8_R3": {
                ret = Versions.v1_8;
                break;
            }
            case "1_9_R1": {
                ret = Versions.v1_9;
                break;
            }
            case "1_9_R2": {
                ret = Versions.v1_9;
                break;
            }
            case "1_10_R1": {
                ret = Versions.v1_10;
                break;
            }
            case "1_11_R1": {
                ret = Versions.v1_11;
                break;
            }
            case "1_12_R1": {
                ret = Versions.v1_12;
                break;
            }
            case "1_13_R1": {
                ret = Versions.v1_13;
                break;
            }
            case "1_13_R2": {
                ret = Versions.v1_13;
                break;
            }
            case "1_14_R1": {
                ret = Versions.v1_14;
                break;
            }
        }
        return ret;
    }



    public static enum Versions {
        v1_8, v1_8_1, v1_8_2, v1_8_3,
        v1_9, v1_9_1, v1_9_2,
        v1_10, v1_10_1,
        v1_11, v1_11_1,
        v1_12, v1_12_1,
        v1_13, v1_13_1, v1_13_2,
        v1_14, v1_14_1, NULL;
    }

}
