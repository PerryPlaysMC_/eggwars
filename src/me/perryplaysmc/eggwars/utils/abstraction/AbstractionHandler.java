package me.perryplaysmc.eggwars.utils.abstraction;

import me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_8.*;
import me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_9.*;
import me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_10.*;
import me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_11.*;
import me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_12.*;
import me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_13.*;
import me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_14.*;
import org.bukkit.Bukkit;

@SuppressWarnings("all")
public class AbstractionHandler {

    private static JsonMessager messager;

    public static void load() {
        String pack =  Bukkit.getServer().getClass().getPackage().getName();
        String version = pack.substring(pack.lastIndexOf('.')+1).replaceFirst("v", "");
        boolean isSupported = true;
        long start = System.currentTimeMillis();
        System.out.println("Loading " + version + " Packet Data");
        switch(version) {
            case "1_8_R1": {
                messager = new JsonMessager_1_8_R1();
                break;
            }
            case "1_8_R2": {
                messager = new JsonMessager_1_8_R2();
                break;
            }
            case "1_8_R3": {
                messager = new JsonMessager_1_8_R3();
                break;
            }
            case "1_9_R1": {
                messager = new JsonMessager_1_9_R1();
                break;
            }
            case "1_9_R2": {
                messager = new JsonMessager_1_9_R2();
                break;
            }
            case "1_10_R1": {
                messager = new JsonMessager_1_10_R1();
                break;
            }
            case "1_11_R1": {
                messager = new JsonMessager_1_11_R1();
                break;
            }
            case "1_12_R1": {
                messager = new JsonMessager_1_12_R1();
                break;
            }
            case "1_13_R1": {
                messager = new JsonMessager_1_13_R1();
                break;
            }
            case "1_13_R2": {
                messager = new JsonMessager_1_13_R2();
                break;
            }
            case "1_14_R1": {
                messager = new JsonMessager_1_14_R1();
                break;
            }
            default: {
                isSupported = false;
                System.out.println("Unsupported version '" + version + "'");
                break;
            }
        }
        if(isSupported) {
            long end = System.currentTimeMillis();
            System.out.println("Loaded Packet Data for version '" + version + "' took " + (end-start) + "ms");
        }
    }


    public static JsonMessager getMessager() {
        return messager;
    }
}
