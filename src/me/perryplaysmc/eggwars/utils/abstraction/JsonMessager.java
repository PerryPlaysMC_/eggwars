package me.perryplaysmc.eggwars.utils.abstraction;

import org.bukkit.entity.Player;

public interface JsonMessager {

    void sendMessage(Player player, String json);

}
