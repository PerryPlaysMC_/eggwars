package me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_11;

import me.perryplaysmc.eggwars.utils.abstraction.JsonMessager;
import net.minecraft.server.v1_11_R1.IChatBaseComponent;
import net.minecraft.server.v1_11_R1.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_1_11_R1 implements JsonMessager {

    @Override
    public void sendMessage(Player player, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, (byte)0);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
