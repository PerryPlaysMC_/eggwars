package me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_12;

import net.minecraft.server.v1_12_R1.ChatMessageType;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.PacketPlayOutChat;
import me.perryplaysmc.eggwars.utils.abstraction.JsonMessager;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_1_12_R1 implements JsonMessager {

    @Override
    public void sendMessage(Player player, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, ChatMessageType.CHAT);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
