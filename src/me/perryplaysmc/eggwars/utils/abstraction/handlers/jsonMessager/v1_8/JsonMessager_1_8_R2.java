package me.perryplaysmc.eggwars.utils.abstraction.handlers.jsonMessager.v1_8;

import net.minecraft.server.v1_8_R2.IChatBaseComponent;
import net.minecraft.server.v1_8_R2.PacketPlayOutChat;
import me.perryplaysmc.eggwars.utils.abstraction.JsonMessager;
import org.bukkit.craftbukkit.v1_8_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class JsonMessager_1_8_R2 implements JsonMessager {

    @Override
    public void sendMessage(Player player, String json) {
        IChatBaseComponent c = IChatBaseComponent.ChatSerializer.a(json);
        PacketPlayOutChat p = new PacketPlayOutChat(c, (byte)0);
        ((CraftPlayer)player).getHandle().playerConnection.sendPacket(p);
    }
}
