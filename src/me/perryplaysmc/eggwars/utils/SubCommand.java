package me.perryplaysmc.eggwars.utils;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import org.bukkit.command.CommandSender;

import java.util.HashMap;

import static me.perryplaysmc.eggwars.utils.Utils.*;

@SuppressWarnings("all")
public interface SubCommand {

  HashMap<CommandSender, EggWarsTeam> teams = EggWarsCommand.getTeams();
  HashMap<CommandSender, EggWarsMap> selected = EggWarsCommand.getSelected();

  String getName();
  
  String getInfo();

  default String[] getAliases() {
    return null;
  }

  default Integer[] argsLength() {
    return null;
  }

  default Integer argLength() {
    return 0;
  }
  
  void execute(CommandSender s, String[] args);

  default String[] getArguments() {
    return null;
  }

  default boolean selectMap() {
    return false;
  }

  default boolean selectTeam() {
    return false;
  }

  default String permission() {
    return getName().toLowerCase();
  }
  
  default boolean isPlayer() {
    return false;
  }
}
