package me.perryplaysmc.eggwars.utils.inventory;

import java.io.File;
import java.util.List;
import me.perryplaysmc.eggwars.utils.Utils;

public class InventoryManager {


    private static List<Gui> guis = new java.util.ArrayList<>();

    public static void addGui(Gui gui) {
        if (!guis.contains(gui)) guis.add(gui);
    }

    public static Gui getGui(String display) {
        for (Gui gui : guis)
            if (!display.isEmpty() && (display.equalsIgnoreCase(gui.getDisplayName())) || (Utils.translate(display).equalsIgnoreCase(gui.getDisplayName())) ||
                    (display.equalsIgnoreCase(gui.getName()))) return gui;
        return null;
    }

    public static void reloadGuis() {
        guis.clear();
        File d = new File("plugins/EggWars/Guis");
        if ((!d.exists()) || (!d.isDirectory())) return;
        File[] files = d.listFiles();
        if (files == null) return;
        for (File f : files) {
            if(!f.exists()) continue;
            if(!f.getName().endsWith(".yml")) continue;
            new Gui(f.getName().split(".yml")[0]);
        }
    }

    public static List<Gui> getGuis() {
        return guis;
    }
}
