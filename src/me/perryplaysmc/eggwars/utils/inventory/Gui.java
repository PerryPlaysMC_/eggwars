package me.perryplaysmc.eggwars.utils.inventory;

import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.config.Config;
import me.perryplaysmc.eggwars.utils.items.Items;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.material.Wool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
@SuppressWarnings("all")
public class Gui {
    private Config cfg;
    private String name;
    private String displayName;
    private Inventory main;
    private EggWarsTeam t;

    public Gui(String name) {
        this.name = name;
        cfg = new Config("plugins/EggWars/Guis", name + ".yml");
        main = new Inventory(cfg.getInt("Size"), Utils.translate(cfg.getString("DisplayName")));
        InventoryManager.addGui(this);
    }

    public String getDisplayName() {
        return main.getName();
    }

    public Inventory getInventory() { return main; }

    public void loadItems() {
        for (String a : cfg.getSection("Items").getKeys(false)) {
            String m = cfg.getString("Items." + a + ".Type.Material");
            ItemStack item;
            if(m.equalsIgnoreCase("Bridge")) {
                item = new ItemStack(Material.BRICKS);
                ItemMeta im = item.getItemMeta();
                im.setDisplayName(Utils.translate("[c]Bridge"));
                im.setLore(Collections.singletonList(Utils.translate("[c]Use this to make an instant bridge!")));
                item.setItemMeta(im);
            }else
                item = new ItemStack(Material.getMaterial(m), cfg.getInt("Items." + a + ".Type.Amount"));
            if (m.contains("HARD_CLAY")) {
                DyeColor color = null;
                if(cfg.isSet("Items." + a + ".Type.Color"))
                    if (cfg.getString("Items." + a + ".Type.Color").contains("{TEAM_COLOR}")) color = t.getDyeColor(); else color = DyeColor.valueOf(cfg.getString("Items." + a + ".Type.Color"));
                else color = DyeColor.WHITE;
                item = new ItemStack(Utils.terracottaFromDye(color), cfg.getInt("Items." + a + ".Type.Amount"));
            }
            if (item.getType().name().contains("WOOL")) {
                DyeColor color = null;
                if(cfg.isSet("Items." + a + ".Type.Color"))
                    if (cfg.getString("Items." + a + ".Type.Color").contains("{TEAM_COLOR}")) color = t.getDyeColor(); else color = DyeColor.valueOf(cfg.getString("Items." + a + ".Type.Color"));
                else color = DyeColor.WHITE;
                item = new Wool(color).toItemStack(cfg.getInt("Items." + a + ".Type.Amount"));
            }
            if ((item.getType().name().contains("LEATHER")) && (!Material.getMaterial(m).name().equalsIgnoreCase("LEATHER"))) {
                DyeColor color = null;
                if (cfg.getString("Items." + a + ".Type.Color").contains("{TEAM_COLOR}")) color = t.getDyeColor(); else color = DyeColor.valueOf(cfg.getString("Items." + a + ".Type.Color"));
                item = new ItemStack(Material.getMaterial(m));
                LeatherArmorMeta im = (LeatherArmorMeta)item.getItemMeta();
                im.setColor(color.getColor());
                item.setItemMeta(im);
            }
            ItemMeta im = item.getItemMeta();
            im.setDisplayName(Utils.translate(cfg.getString("Items." + a + ".Name")));
            List<String> lore = new ArrayList<>();
            String b; for (Iterator localIterator2 = cfg.getStringList("Items." + a + ".Lore").iterator(); localIterator2.hasNext(); lore.add(Utils.translate(b))) b = (String)localIterator2.next();
            im.setLore(lore);
            for (String x : cfg.getStringList("Items." + a + ".Enchantments"))
                if (x.contains(",")) {
                    String[] sp = x.split(",");

                    Enchantment ench = Enchantment.getByKey(NamespacedKey.minecraft(sp[0].toLowerCase())) != null ?
                            Enchantment.getByKey(NamespacedKey.minecraft(sp[0].toLowerCase())) : Enchantment.getByName(sp[0].toUpperCase());
                    if (ench == null) {
                        System.out.println("Invalid enchantment '" + sp[0] + "'");
                    }
                    else {
                        try {
                            Integer.parseInt(sp[1]);
                        } catch (Exception e) {
                            System.out.println("Invalid enchantment level '" + sp[1] + "'");
                            break;
                        }

                        if (Integer.parseInt(sp[1]) < 1) {
                            System.out.println("Invalid enchantment level '" + sp[1] + "' must be higher than 0");
                        }
                        else
                            im.addEnchant(ench, Integer.parseInt(sp[1]), true);
                    }
                }
            item.setItemMeta(im);
            main.setItem(cfg.getInt("Items." + a + ".Slot"), item);
        }
        main.setItem(main.getSize()+-5, Items.getBook());
    }

    public void setTeam(EggWarsTeam t) {
        this.t = t;
    }

    public String getName() {
        return name;
    }

    public Config getConfig() {
        return cfg;
    }

    public EggWarsTeam getTeam() {
        return t;
    }
}
