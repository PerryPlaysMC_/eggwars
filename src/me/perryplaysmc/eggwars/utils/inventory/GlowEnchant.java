package me.perryplaysmc.eggwars.utils.inventory;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;

public class GlowEnchant extends Enchantment
{
  public GlowEnchant(String key)
  {
    super(new org.bukkit.NamespacedKey("eggwars", key));
  }
  
  public String getName()
  {
    return getKey().getNamespace();
  }
  
  public int getMaxLevel()
  {
    return 1;
  }
  
  public int getStartLevel()
  {
    return 0;
  }
  
  public org.bukkit.enchantments.EnchantmentTarget getItemTarget()
  {
    return null;
  }
  
  public boolean isTreasure()
  {
    return false;
  }
  
  public boolean isCursed()
  {
    return false;
  }
  
  public boolean conflictsWith(Enchantment enchantment)
  {
    return false;
  }
  
  public boolean canEnchantItem(ItemStack itemStack)
  {
    return false;
  }
}
