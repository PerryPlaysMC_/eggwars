package me.perryplaysmc.eggwars.utils;

import org.bukkit.block.BlockFace;

public class BlockFacing {
  public BlockFacing() {}
  
  public static final BlockFace[] radial = { BlockFace.NORTH, BlockFace.NORTH_EAST, BlockFace.EAST, BlockFace.SOUTH_EAST, BlockFace.SOUTH, BlockFace.SOUTH_WEST, BlockFace.WEST, BlockFace.NORTH_WEST }; public static final BlockFace[] axis = { BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST };
  
  public static BlockFace locationToFace(org.bukkit.Location location) {
    return yawToFace(location.getYaw());
  }
  
  public static BlockFace locationToFace(org.bukkit.Location location, boolean use) { return yawToFace(location.getYaw(), use); }
  
  public static BlockFace yawToFace(float yaw)
  {
    return yawToFace(yaw, true);
  }
  
  public static BlockFace yawToFace(float yaw, boolean useSubCardinalDirections) {
    if (useSubCardinalDirections) {
      return radial[(Math.round(yaw / 45.0F) & 0x7)];
    }
    return axis[(Math.round(yaw / 90.0F) & 0x3)];
  }
}
