package me.perryplaysmc.eggwars.utils;

import java.util.Random;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.generator.ChunkGenerator;
import org.bukkit.generator.ChunkGenerator.BiomeGrid;
import org.bukkit.generator.ChunkGenerator.ChunkData;

public class WorldGenerator extends WorldCreator {
  public WorldGenerator(String name) {
    super(name);
    generator(new chunkGen());
  }
  
  class chunkGen extends ChunkGenerator {
    
    public boolean canSpawn(World world, int x, int z)
    {
      return true;
    }
    
    public ChunkData generateChunkData(World world, Random random, int cx, int cz, BiomeGrid biome) {
      ChunkData d = createChunkData(world);
      
      for (int x = 0; x < 16; x++) {
        for (int y = 0; y < 16; y++) {
          for (int z = 0; z < 16; z++) {
            d.setBlock(x, y, z, Material.AIR);
          }
        }
      }
      
      return d;
    }
  }
}
