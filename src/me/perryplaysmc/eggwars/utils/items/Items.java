package me.perryplaysmc.eggwars.utils.items;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Collections;

public class Items {

    public static ItemStack[] getLobbyItems() {
        ItemStack i = new ItemStack(Material.NETHER_STAR, 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§bTeam Selector");
        i.setItemMeta(im);

        ItemStack i2 = new ItemStack(Material.PAPER, 1);
        ItemMeta im2 = i.getItemMeta();
        im2.setDisplayName("§bKit Selector");
        i2.setItemMeta(im2);
        return new ItemStack[] { i, i2 };
    }

    public static ItemStack getDiamondGenerator() {
        ItemStack i = new ItemStack(Material.DIAMOND_BLOCK, 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§bDiamond generator");
        im.setLore(Arrays.asList("§9Place this block", "§9And a generator will be created."));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getEmeraldGenerator() {
        ItemStack i = new ItemStack(Material.EMERALD_BLOCK, 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§aEmerald generator");
        im.setLore(Arrays.asList("§9Place this block", "§9And a generator will be created."));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getGoldGenerator() {
        ItemStack i = new ItemStack(Material.GOLD_BLOCK, 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§6Gold generator");
        im.setLore(Arrays.asList("§9Place this block", "§9And a generator will be created."));
        i.setItemMeta(im);
        return i;
    }

    public static ItemStack getIronGenerator() {
        ItemStack i = new ItemStack(Material.IRON_BLOCK, 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§7Iron generator");
        im.setLore(Arrays.asList("§9Place this block", "§9And a generator will be created."));
        i.setItemMeta(im);
        return i;
    }
    
    public static ItemStack getBook() {
        ItemStack i = new ItemStack(Material.BOOK, 1);
        ItemMeta im = i.getItemMeta();
        im.setDisplayName("§9Back");
        im.setLore(Collections.singletonList("Return to main page."));
        i.setItemMeta(im);
        return i;
    }
    
}
