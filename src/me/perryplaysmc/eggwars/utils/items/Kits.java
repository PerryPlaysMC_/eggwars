package me.perryplaysmc.eggwars.utils.items;

import java.util.ArrayList;
import java.util.List;
import me.perryplaysmc.eggwars.utils.config.Config;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class Kits
{
    public static List<KitHolder> kits = new ArrayList<>();

    public Kits() {}

    public static void loadKits() { Config cfg = new Config(me.perryplaysmc.eggwars.EggWars.getInstance(), null, "kits.yml");
        if (cfg.getSection("Kits") != null) {
            for (String name : cfg.getSection("Kits").getKeys(false)) {
                List<ItemStack> items = new ArrayList<>();List<ItemStack> armor = new ArrayList<>();
                ItemStack display = cfg.getItemStack("Kits." + name + ".displayItem");
                for (String line : cfg.getSection("Kits." + name).getKeys(false)) {
                    if (!line.equalsIgnoreCase("displayItem"))
                    {

                        if (!cfg.getBoolean("Kits." + name + "." + line + ".isArmor")) {
                            ItemStack i = cfg.getItemStack("Kits." + name + "." + line);
                            items.add(i);
                        } else {
                            ItemStack i = cfg.getItemStack("Kits." + name + "." + line);
                            armor.add(i);
                        } }
                }
                KitHolder k = new KitHolder(name, display);
                if (items.size() > 0) {
                    k.setInventory(items.toArray(new ItemStack[items.size()]));
                }
                if (armor.size() > 0) {
                    k.setArmor(armor.toArray(new ItemStack[armor.size()]));
                }
                kits.add(k);
            }
        }
    }

    public static void createKit(String name, Player p) {
        ItemStack inHand = p.getInventory().getItemInMainHand();
        p.getInventory().setItemInMainHand(null);
        ItemStack[] items = p.getInventory().getStorageContents();
        ItemStack[] armor = p.getInventory().getArmorContents();

        Config cfg = new Config(me.perryplaysmc.eggwars.EggWars.getInstance(), null, "kits.yml");
        int length = 0;
        KitHolder k = new KitHolder(name.toLowerCase(), inHand);
        if (items.length > 0) {
            k.setInventory(items);
        }
        if (armor.length > 0) {
            k.setArmor(armor);
        }
        List<ItemStack> item = new ArrayList<>();List<ItemStack> arm = new ArrayList<>();
        if ((items != null) && (items.length > 0)) {
            length = items.length;
            for (int i = 0; i < items.length; i++) {
                if ((items[i] != null) && (items[i].getType() != Material.AIR))
                    item.add(items[i]);
            }
        }
        if ((armor != null) && (armor.length > 0)) {
            for (int i = length; i < armor.length + length; i++) {
                if ((armor[(i + -length)] != null) && (armor[(i + -length)].getType() != Material.AIR))
                    arm.add(armor[(i + -length)]);
            }
        }
        length = 0;
        if (item.size() > 0) {
            length = item.size();
            for (int i = 0; i < item.size(); i++) {
                cfg.setItemStack("Kits." + name + "." + i, item.get(i), false);
            }
        }
        if (arm.size() > 0) {
            for (int i = length; i < arm.size() + length; i++)
                cfg.setItemStack("Kits." + name + "." + i, arm.get(i + -length), true);
        }
        cfg.setItemStack("Kits." + name + ".displayItem", inHand);
        p.getInventory().setItemInMainHand(inHand);
    }

    private static KitHolder getHolder(String name) {
        for (KitHolder k : kits) {
            if (k.getName().equalsIgnoreCase(name)) return k;
        }
        return null;
    }

    public static ItemStack[] getArmor(String name) {
        KitHolder k = getHolder(name);
        if (k == null) return null;
        return k.getArmor();
    }

    public static ItemStack[] getKit(String name) {
        KitHolder k = getHolder(name);
        if (k == null) return null;
        return k.getInventory();
    }

    public static void giveKit(Player p, String kit) {
        if (getHolder(kit) == null) return;
        p.getInventory().setStorageContents(getKit(kit));
        if ((getArmor(kit) == null) || (getArmor(kit).length < 0)) return;
        ItemStack helmet = null;ItemStack chest = null;ItemStack pants = null;ItemStack boots = null;

        for (ItemStack i : getArmor(kit)) {
            if (i.getType().name().contains("HELMET")) helmet = i;
            if (i.getType().name().contains("CHEST")) chest = i;
            if (i.getType().name().contains("LEGGINGS")) pants = i;
            if (i.getType().name().contains("BOOTS")) boots = i;
        }
        if (helmet != null) {
            p.getInventory().setHelmet(helmet);
        }
        if (chest != null) {
            p.getInventory().setChestplate(chest);
        }
        if (pants != null) {
            p.getInventory().setLeggings(pants);
        }
        if (boots != null) {
            p.getInventory().setBoots(boots);
        }
    }

    public static class KitHolder
    {
        private String name;
        private ItemStack display;
        private ItemStack[] inventory;
        private ItemStack[] armor;

        public KitHolder(String name, ItemStack display, ItemStack[] inventory, ItemStack[] armor) {
            this.name = name;
            this.display = display;
            this.inventory = inventory;
            this.armor = armor;
        }

        public KitHolder(String name, ItemStack display, ItemStack[] inventory) {
            this.name = name;
            this.display = display;
            this.inventory = inventory;
        }

        public KitHolder(String name, ItemStack display) { this.name = name;
            this.display = display;
        }

        public ItemStack[] getInventory()
        {
            return inventory;
        }

        public void setInventory(ItemStack[] inventory)
        {
            this.inventory = inventory;
        }

        public ItemStack[] getArmor() {
            return armor;
        }

        public void setArmor(ItemStack[] armor) {
            this.armor = armor;
        }

        public ItemStack getDisplay() {
            return display;
        }

        public String getName() {
            return name;
        }
    }
}
