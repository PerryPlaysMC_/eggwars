package me.perryplaysmc.eggwars.utils;

import me.perryplaysmc.eggwars.EggWarsAPI;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.utils.reflection.ReflectionUtil;
import org.bukkit.*;
import me.perryplaysmc.eggwars.map.WorldManager;

import java.lang.reflect.Method;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.io.File;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.command.CommandSender;
import me.perryplaysmc.eggwars.utils.config.Config;
import org.bukkit.block.BlockFace;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.plugin.Plugin;
import me.perryplaysmc.eggwars.utils.items.Items;
import me.perryplaysmc.eggwars.EggWars;
import org.bukkit.entity.Player;
import me.perryplaysmc.eggwars.map.EggWarsMap;

@SuppressWarnings("all")
public class Utils {

    public static HashMap<Player, EggWarsInventory> inventories = new HashMap<>();
    public static EggWarsMap loadMap(String name) {
        return new EggWarsMap(name);
    }

    public static void addWin(Player player) {
        EggWarsMap map = MapManager.byPlayer(player);
        Config c = EggWarsAPI.getDataConfig();
        String path = "Data."+player.getUniqueId();
        if(map != null&c!=null) {
            if(map.getMaxPlayersPerTeam() == 1) {
                c.set(path+".Solo", c.isSet(path+".Solo") ? c.getInt(path+".Solo")+1 : 1);
            }else {
                c.set(path+".Team", c.isSet(path+".Team") ? c.getInt(path+".Teama")+1 : 1);
            }
        }
    }


    public static void scheduleJoin(EggWarsMap ma, Player p) {
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(EggWars.getInstance(), () -> {
            if (ma.getLobbyLoc() != null) {
                p.teleport(ma.getLobbyLoc());
                p.getInventory().setHeldItemSlot(0);
                p.updateInventory();
                p.setHealth(p.getMaxHealth());
                p.setFoodLevel(20);
                p.setSaturation(20);
            }
            if ((ma.getPlayers().size() >= ma.getMinPlayers() && ma.getPlayers().size() > 0) || (ma.getPlayers().size() == ma.getMaxPlayers() && ma.getPlayers().size() > 0)) {
                ma.getTimer().setPaused(false);
            }
            ma.broadCast(translate("Messages.Join.join", p.getName(), ma.getName(), ma.getMinPlayers(), ma.getMaxPlayers(), ma.getPlayers().size(), ma.getMaxPlayersPerTeam(), ma.getState().getLocalizedName()));
            ma.updateSign();
            ma.update(p);
            inventories.put(p, new EggWarsInventory(p.getInventory().getContents(),
                    p.getInventory().getStorageContents(),
                    p.getInventory().getArmorContents(),
                    p.getInventory().getExtraContents()));
            p.getInventory().setContents(Items.getLobbyItems());
            if(ma.getState() == GameState.LOBBY) p.setGameMode(GameMode.ADVENTURE);
            else p.setGameMode(GameMode.SURVIVAL);
            for(Player a : Bukkit.getOnlinePlayers()) {
                if (!ma.getPlayers().contains(a)) {
                    a.hidePlayer(p);
                    p.hidePlayer(a);
                }
            }
            for(Player p2 : ma.getPlayers()) {
                p.showPlayer(p2);
                p2.showPlayer(p);
            }
        }, 5L);
    }

    public static String translateChatColor(String str) {
        return translateColors('&', str);
    }


    public static String translateColors(char altColor, String text) {
        for(ChatColor c : ChatColor.values()) {
            text = text.replace((altColor+"") + c.getChar(), "§" + c.getChar());
        }
        return text;
    }

    public static String changeColor(String str) {
        String r = str;
        if(str.contains("§") || str.contains("&"))
            for(ChatColor c : ChatColor.values()) {
                if(str.contains("§"+c.getChar()))
                    r = r.replace("§" + c.getChar(), "&" + c.getChar());
            }
        return r;
    }


    public static boolean isColor(String last) {
        return !isBold(last) && !isItalic(last) && !isStrikThrough(last) && !isUnderline(last);
    }

    public static boolean hasColor(String last) {
        for(String x : changeColor(last).split("&")) {
            if(isColor(x)) {
                return true;
            }
        }
        return false;
    }
    public static String replaceFirstColor(String last) {
        String f = changeColor(last);
        if(f.length()>0) {
            f.split("&")[0] = "";
            return f.substring(2);
        }
        return f;
    }
    public static String stripColor(String str) {
        String r = translate(str);
        for(ChatColor c : ChatColor.values()) {
            if(str.contains("§"+c.getChar()))
                r = r.replace("§" + c.getChar(), "");
            if(str.contains("&"+c.getChar()))
                r = r.replace("&" + c.getChar(), "");
        }
        return ChatColor.stripColor(r);
    }


    private static boolean isBold(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'l')
            return true;
        return false;
    }
    private static boolean isItalic(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'o')
            return true;
        return false;
    }
    private static boolean isUnderline(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'n')
            return true;
        return false;
    }
    private static boolean isStrikThrough(String last) {
        String x = (last);
        if(x.isEmpty() || x == "") return false;
        if(x.charAt(x.length()+-1) == 'm')
            return true;
        return false;
    }

    public static boolean checkForDomain(String str) {
        String domainPattern = "[a-zA-Z0-9](([a-zA-Z0-9\\-]{0,61}[A-Za-z0-9])?\\.)+(com|net|org|co|gg|io|pro)";
        Pattern r = Pattern.compile(domainPattern);
        Matcher m = r.matcher(str);
        Pattern invPat = Pattern.compile("(?:https?://)?discord(?:app\\.com/invite|\\.gg)/([a-z0-9-]+)", 2);
        Matcher m2 = invPat.matcher(str);
        return m.find() || m2.find();
    }

    public static ChatColor getLastColorForChat(String message, ChatColor def) {
        String a = stripColor(message);
        if(a.toCharArray().length < 1) return def;
        int l = a.toCharArray().length;
        ChatColor c = ChatColor.getByChar(a.charAt(l+-1));
        int i = l+-1;
        while(c == null || !c.isColor()) {
            i = i+-2;
            if(l >= l+- i) {
                c = ChatColor.getByChar(a.charAt(l +- i));
            }else {
                break;
            }
        }
        if(c == null || !c.isColor()) return def;
        return c;
    }

    public static String getLastColors(String msg) {
        String last = "", l2 = "";
        if(msg.contains("&") || msg.contains("§")) {
            msg = msg.replace("&", "§");
            for (int c = 1; c < msg.split("§").length; c++) {
                String[] d = msg.split("§");
                if(d.length == 0 || d[c] == null || d[c].length() == 0) continue;
                String ch = d[c].charAt(0) + "";
                if(ChatColor.getByChar(d[c].charAt(0)) != null) {
                    if(isColor(last)) {
                        if(last.length()+-2>0)
                            last = last.substring(0, last.length()+-2);
                    }
                    if(!last.contains("§"+ch))
                        if(isColor("§"+ch))
                            last = "§" + ch;
                        else
                            last += "§" + ch;
                }
            }
        }
        return last.replace("§§","§");
    }



    public static String translateUserPerms(CommandSender u, String original, String ms) {
        if(u == null) {
            return translateChatColor(ms);
        }
        String og = !original.startsWith("&") & !original.startsWith("§") ? "§"+original : original;
        for(ChatColor c : ChatColor.values()) {
            if(ms.contains("&" + c.getChar())) {
                if(hasPermission(u, "allcodes", "chatcolor." + c.getChar())) {
                    if(c.getChar()=='r') {
                        ms = ms.replace("&r", og).replace("§r", og).replace(og, og.replace("&", "§"));
                        continue;
                    }
                    ms = ms.replace("&" + c.getChar(), "§" + c.getChar());
                }
            }
        }
        return ms;
    }

    public static Material terracottaFromDye(DyeColor c) {
        for (Material m : Material.values()) {
            if (m.name().contains(Material.TERRACOTTA.name()) && m.name().contains(c.name()) && !m.name().contains("GLAZED")) {
                return m;
            }
        }
        return Material.TERRACOTTA;
    }

    public static String getString(String path) {
        return translate(getConfig().getString(path));
    }

    public static BlockFace getBlockFaceOpposite(Player p) {
        BlockFace b = BlockFacing.locationToFace(p.getLocation(), false);
        return b;
    }

    public static Config getConfig() {
        return EggWars.getInstance().getConfiguration();
    }

    public static void sendRaw(CommandSender s, String... messages) {
        for(String msg : messages) {
            s.sendMessage(Utils.translate(msg));
        }
    }

    public static void sendMessage(CommandSender s, String path, Object... objects) {
        if(!getConfig().isSet("Messages." + path)) {
            s.sendMessage(translate("[c]Couldn't find Config Location: '[pc]Messages." + path + "[c]'"));
            return;
        }
        String res = getConfig().getString("Messages." + path);
        for (int i = 0; i < objects.length; ++i) {
            res = res.replace("{" + i + "}", objects[i].toString());
        }
        res = translate(res);
        s.sendMessage(res);
    }



    public static void sendMessageAll(CommandSender s, String path, Object... objects) {
        String res = path;
        for (int i = 0; i < objects.length; ++i) {
            res = res.replace("{" + i + "}", objects[i].toString());
        }
        s.sendMessage(translate(res));
    }

    public static String serializeLoc(Location loc) {
        String s = "/";
        DecimalFormat df = new DecimalFormat("###.###");
        String ret = loc.getWorld().getName() + s + df.format(loc.getX()) + s + df.format(loc.getY()) + s + df.format(loc.getZ()) + s + loc.getYaw() + s + loc.getPitch();
        return ret;
    }

    public static boolean isInt(String check) {
        try {
            Integer.parseInt(check);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public static Location deserializeLoc(String loc) {
        String s = "/";
        if (loc == null) {
            return null;
        }
        if (loc.contains(s) && loc.split(s).length == 6) {
            String[] l = loc.split(s);
            Location ret = new Location(Bukkit.getWorld(l[0]), Double.parseDouble(l[1]), Double.parseDouble(l[2]), Double.parseDouble(l[3]), Float.parseFloat(l[4]), Float.parseFloat(l[5]));
            return ret;
        }
        return null;
    }

    public static boolean checkLocations(Location a, Location b) {
        return a.getBlockX() == b.getBlockX() && a.getBlockY() == b.getBlockY() && a.getBlockZ() == b.getBlockZ();
    }

    public static List<Location> deserializeLoc(List<String> locationList) {
        List<Location> ret = new ArrayList<>();
        for (String l : locationList) {
            ret.add(deserializeLoc(l));
        }
        return ret;
    }

    public static Config getInventoryMain() {
        return EggWars.getInstance().getInventoryMain();
    }

    public static Set<Class<?>> getClasses(File jarFile, String packageName) {
        Set<Class<?>> classes = new HashSet<>();
        List<String> names = new ArrayList<>();
        try {
            JarFile file = new JarFile(jarFile);
            Enumeration<JarEntry> entry = file.entries();
            while (entry.hasMoreElements()) {
                JarEntry jarEntry = entry.nextElement();
                String name = jarEntry.getName().replace("/", ".");
                if (name.startsWith(packageName) && name.endsWith(".class") && !names.contains(name.split(packageName + ".")[1].replace(".class", ""))) {
                    names.add(name.split(packageName + ".")[1].replace(".class", ""));
                }
            }
            Collections.sort(names);
            for (String name2 : names) {
                classes.add(Class.forName(packageName + "." + name2));
            }
            file.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return classes;
    }

    public static List<String> serializeLoc(List<Location> locationList) {
        List<String> ret = new ArrayList<>();
        for (Location l : locationList) {
            ret.add(serializeLoc(l));
        }
        return ret;
    }

    public static String translate2(String path, Object... objects) {
        String a = path;
        for (int i = 0; i < objects.length; ++i) {
            a = a.replace("{" + i + "}", objects[i].toString());
        }
        return translate(a);
    }

    public static String translate(String path, Object... objects) {
        if(!getConfig().isSet(path)) {
            return translate("[c]Couldn't find Config Location: '[pc]" + path + "[c]'");
        }
        String a = getConfig().getString(path);
        for (int i = 0; i < objects.length; ++i) {
            a = a.replace("{" + i + "}", objects[i].toString());
        }
        return translate(a);
    }

    public static boolean hasPermission(CommandSender s, String... permissions) {
        for (String perm : permissions) {
            if (s.isOp() || s.hasPermission("*") || s.hasPermission(perm) || s.hasPermission("eggwars." + perm)
                    || s.hasPermission("eggwars.command." + perm) ||
                    s.hasPermission("eggwars.command.*") || s.hasPermission("eggwars.*")) {
                return true;
            }
        }
        return false;
    }

    public static void removeDupedMap(EggWarsMap m) {
        if (m.isDuped() || m.getConfig().getBoolean("Duped")) {
            File map = m.getConfig().getFile();
            if (m.getWorld() != null) {
                WorldManager.removeWorld(m.getWorld());
            }
            else {
                WorldManager.removeWorld(m.getConfig().getString("World"));
            }
            map.delete();
        }
    }

    public static String translate(String toTrans) {
        String prefix = "";
        if (toTrans != null && getConfig() != null && getConfig().getString("prefix") != "" && getConfig().getString("prefix") != null) {
            prefix = ((toTrans.contains("[p] ") || getConfig().getString("prefix").endsWith(" ")) ? getConfig().getString("prefix") : (getConfig().getString("prefix") + " "));
        }
        if (toTrans.contains("[p]")) {
            toTrans = toTrans.replace("[p]", prefix);
        }
        if (toTrans.contains("[c]")) {
            toTrans = toTrans.replace("[c]", getConfig().getString("chatColor"));
        }
        if (toTrans.contains("[pc]")) {
            toTrans = toTrans.replace("[pc]", getConfig().getString("playerColor"));
        }
        return ChatColor.translateAlternateColorCodes('&', toTrans).replace("\n", "\n").replace("[n]", "\n").replace("\\n", "\n");
    }


    public static String convertItemStackToJson(ItemStack itemStack) {
        // ItemStack methods to get a net.minecraft.server.ItemStack object for serialization
        Class<?> craftItemStackClazz = ReflectionUtil.getOBCClass("inventory.CraftItemStack");
        Method asNMSCopyMethod = ReflectionUtil.getMethod(craftItemStackClazz, "asNMSCopy", ItemStack.class);

        // NMS Method to serialize a net.minecraft.server.ItemStack to a valid Json string
        Class<?> nmsItemStackClazz = ReflectionUtil.getNMSClass("ItemStack");
        Class<?> nbtTagCompoundClazz = ReflectionUtil.getNMSClass("NBTTagCompound");
        Method saveNmsItemStackMethod = ReflectionUtil.getMethod(nmsItemStackClazz, "save", nbtTagCompoundClazz);

        Object nmsNbtTagCompoundObj; // This will just be an empty NBTTagCompound instance to invoke the saveNms method
        Object nmsItemStackObj; // This is the net.minecraft.server.ItemStack object received from the asNMSCopy method
        Object itemAsJsonObject; // This is the net.minecraft.server.ItemStack after being put through saveNmsItem method

        try {
            nmsNbtTagCompoundObj = nbtTagCompoundClazz.newInstance();
            nmsItemStackObj = asNMSCopyMethod.invoke(null, itemStack);
            itemAsJsonObject = saveNmsItemStackMethod.invoke(nmsItemStackObj, nmsNbtTagCompoundObj);
        } catch (Throwable t) {
            Bukkit.getLogger().log(Level.SEVERE, "failed to serialize itemstack to nms item", t);
            return "Error occurred whilst parsing itemstack";
        }

        // Return a string representation of the serialized object
        return itemAsJsonObject.toString();
    }

    public static ChatColor byNameChat(String name) {
        for (ChatColor c : ChatColor.values()) {
            if (c.name().equalsIgnoreCase(name)) {
                return c;
            }
        }
        return null;
    }

    public static DyeColor byNameDye(String name) {
        for (DyeColor c : DyeColor.values()) {
            if (c.name().equalsIgnoreCase(name)) {
                return c;
            }
        }
        return null;
    }
}
