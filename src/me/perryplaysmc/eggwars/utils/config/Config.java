package me.perryplaysmc.eggwars.utils.config;

import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class Config {

    private File f;
    private File dir;
    private YamlConfiguration cfg;
    private String name;
    private String copy;
    private EggWars core = null;
    private boolean reloadOnce = false;
    private boolean isCopy;

    public Config(EggWars core, File dir, String copy, String name) {
        this.core = core;
        if(dir == null) {
            dir = new File("plugins", "EggWars");
        }
        if(!dir.isDirectory()) {
            dir.mkdirs();
            this.dir = dir;
        }
        this.name = name;
        f = new File(dir, name);
        if((copy == null) || (copy.isEmpty())) {
            if(!f.exists()) {
                try {
                    if(core != null) {
                        if(core.getResource(f.getName()) != null) {
                            FileUtils.copyInputStreamToFile(core.getResource(f.getName()), f);
                        } else {
                            f.createNewFile();
                        }
                    } else {
                        f.createNewFile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            isCopy = true;
            this.copy = copy;
            if(!f.exists()) {
                try {
                    if(core != null) {
                        if(core.getResource(copy) != null) {
                            FileUtils.copyInputStreamToFile(core.getResource(copy), f);
                        } else {
                            f.createNewFile();
                        }
                    } else {
                        f.createNewFile();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        if(name.endsWith(".yml")) {
            reloadOnce();
        }
        ConfigManager.addConfig(this);
    }

    public Config(EggWars core, File dir, String name) {
        this(core, dir, "", name);
    }

    public Config(File dir, String fileCopy, String name) {
        this(EggWars.getInstance(), dir, fileCopy, name);
    }

    public Config(File dir, String name) {
        this(EggWars.getInstance(), dir, "", name);
    }

    public Config(String dir, String name) {
        this(EggWars.getInstance(), new File(dir), "", name);
    }

    public Config(String name) {
        this(EggWars.getInstance(), null, "", name);
    }

    public EggWars getCore() {
        return core;
    }


    public Config resetConfig() {
        try {
            f = new File(getDirectory(), getName());
            if(isCopy) {
                FileUtils.copyInputStreamToFile(core.getResource(copy), f);
                f = f;
                return this;
            }
            FileUtils.copyInputStreamToFile(core.getResource(getName()), f);
            f = f;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this;
    }

    public String getName() {
        return name;
    }

    public String getLocalName() {
        if(name.endsWith(".yml")) {
            return name.replace(".yml", "");
        }
        return name;
    }

    public File getDirectory() {
        return dir;
    }

    public File getFile() {
        return f;
    }

    public YamlConfiguration getConfig() {
        return cfg;
    }

    public String getString(String path) {
        return cfg.getString(path);
    }

    public boolean getBoolean(String path) {
        return cfg.getBoolean(path);
    }

    public Object get(String path) {
        if(cfg.get(path) instanceof String) {
            if((isSet(path) && getString(path).contains("/") && getString(path).split("/").length == 6)) {
                return getLocation(getString(path));
            }
            return getString(path);
        }
        return cfg.get(path);
    }

    public ConfigurationSection getSection(String path) {
        return cfg.getConfigurationSection(path);
    }

    public ConfigurationSection createSection(String path) {
        return cfg.createSection(path);
    }

    public int getInt(String path) {
        return cfg.getInt(path);
    }

    public double getDouble(String path) {
        return cfg.getDouble(path);
    }

    public List<String> getStringList(String path) {
        return cfg.isSet(path) ? cfg.getStringList(path) : new ArrayList();
    }

    public Location getLocation(String path) {
        return (isSet(path) && getString(path).contains("/") && getString(path).split("/").length == 6 ? Utils.deserializeLoc(getString(path)) : null);
    }


    public List<Object> getObjectList(String path) {
        return (List<Object>) cfg.getList(path);
    }

    public Config set(String path, Object var) {
        if((var instanceof Location)) {
            cfg.set(path, Utils.serializeLoc((Location) var));
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }

    public List<?> getList(String path) {
        return cfg.getList(path);
    }

    private void saveItem(ConfigurationSection section, ItemStack item) {
        if(item == null) return;
        section.set("type", item.getType().name());
        section.set("amount", item.getAmount());
        if(item.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta) item.getItemMeta();
            if(im.getOwner() != null)
                section.set("Owner", im.getOwner());
            if(im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for(Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if(enchants.size() > 0)
                section.set("enchants", enchants);
            if(im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
            return;
        }
        if(item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if(im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList<>();
            for(Enchantment i : item.getEnchantments().keySet()) {
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            }
            if(enchants.size() > 0)
                section.set("enchants", enchants);
            if(im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", im.isUnbreakable());
        }
        section.set("data", item.getDurability());
    }

    private ItemStack loadItem(ConfigurationSection section) {
        ItemStack i = new ItemStack(Material.valueOf(section.getString("type")), section.getInt("amount"));
        if(i.getType() == Material.PLAYER_HEAD) {
            SkullMeta im = (SkullMeta) i.getItemMeta();
            if(section.getString("Owner") != null)
                im.setOwner(section.getString("Owner"));
            if(section.getStringList("enchants") != null) {
                for(String a : section.getStringList("enchants"))
                    im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
            }
            if(section.getStringList("lore") != null) {
                im.setLore(section.getStringList("lore"));
            }
            im.setDisplayName(section.getString("displayname"));
            im.setUnbreakable(section.getBoolean("unbreakable"));
            i.setDurability((short) section.getInt("data"));
            i.setItemMeta(im);
            return i;
        }
        ItemMeta im = i.getItemMeta();
        if(section.getStringList("enchants") != null) {
            for(String a : section.getStringList("enchants"))
                im.addEnchant(Enchantment.getByName(a.split("--")[0]), Integer.parseInt(a.split("--")[1]), true);
        }
        if(section.getStringList("lore") != null) {
            im.setLore(section.getStringList("lore"));
        }
        im.setDisplayName(section.getString("displayname"));
        im.setUnbreakable(section.getBoolean("unbreakable"));
        i.setDurability((short) section.getInt("data"));
        i.setItemMeta(im);
        return i;
    }

    public Config setItemStack(String path, ItemStack item) {
        saveItem(createSection(path), item);
        save();
        return this;
    }

    public boolean isSet(String path) {
        return cfg.isSet(path);
    }

    public ItemStack getItemStack(String path) {
        return loadItem(getSection(path));
    }

    public void reloadOnce() {
        if(!reloadOnce) {
            reloadOnce = true;
            cfg = YamlConfiguration.loadConfiguration(f);
        }
    }

    public void deleteConfig() {
        String name = getLocalName();
        getFile().delete();
        this.name = "";
    }

    public Long getLong(String path) {
        return cfg.getLong(path);
    }

    public void reload() {
        cfg = YamlConfiguration.loadConfiguration(f);
    }

    public void save() {
        try {
            cfg.save(f);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object getMap(String name, String path) {
        Object j = null;
        if(getFile().getName().equalsIgnoreCase("maps.yml")) {
            j = cfg.get("Maps." + name + "." + path);
        } else {
            j = cfg.get(path);
        }
        if(j == null) return null;
        return j;
    }

    public ConfigurationSection getMapSection(String name, String path) {
        if(getFile().getName().equalsIgnoreCase("maps.yml")) {
            return cfg.getConfigurationSection("Maps." + name + "." + path);
        }
        return cfg.getConfigurationSection(path);
    }

    public boolean isSetMap(String name, String path) {
        if(getFile().getName().equalsIgnoreCase("maps.yml")) {
            return cfg.get("Maps." + name + "." + path) != null;
        }
        return cfg.get(path) != null;
    }

    public List<String> getMapStringList(String name, String path) {
        if(getFile().getName().equalsIgnoreCase("maps.yml")) {
            return cfg.getStringList("Maps." + name + "." + path);
        }
        return cfg.getStringList(path);
    }

    public Config setMap(String map, String path, Object var) {
        if(getFile().getName().equalsIgnoreCase("maps.yml")) {
            cfg.set("Maps." + map + "." + path, var);
            save();
            return this;
        }
        cfg.set(path, var);
        save();
        return this;
    }


    private void saveItem(ConfigurationSection section, ItemStack item, boolean isArmor) {
        if(item == null) return;
        if(item.getType() == Material.POTION) {
            PotionMeta pm = (PotionMeta) item.getItemMeta();
            section.set("type", item.getType().name());
            section.set("amount", Integer.valueOf(item.getAmount()));
            section.set("isArmor", Boolean.valueOf(isArmor));
            section.set("data", Short.valueOf(item.getDurability()));
            if(pm.hasDisplayName())
                section.set("displayname", pm.getDisplayName());
            if(pm.hasLore())
                section.set("lore", pm.getLore());
            if(pm.hasColor())
                section.set("color", pm.getColor());
            if(pm.hasCustomEffects()) {
                List<String> effects = new ArrayList();
                for(PotionEffect pe : pm.getCustomEffects())
                    effects.add(pe.getType() + ">>" + pe.getDuration() + ">>" + pe.getAmplifier() + ">>" + pe.isAmbient() + ">>" + pe.hasParticles());
                section.set("effects", effects);
            }

            return;
        }
        section.set("type", item.getType().name());
        section.set("amount", Integer.valueOf(item.getAmount()));
        section.set("isArmor", Boolean.valueOf(isArmor));
        section.set("data", Short.valueOf(item.getDurability()));
        if(item.hasItemMeta()) {
            ItemMeta im = item.getItemMeta();
            if(im.hasDisplayName())
                section.set("displayname", item.getItemMeta().getDisplayName());
            List<String> enchants = new ArrayList();
            for(Enchantment i : item.getEnchantments().keySet())
                enchants.add(i.getName() + "--" + item.getEnchantments().get(i));
            if(enchants.size() > 0)
                section.set("enchants", enchants);
            if(im.hasLore())
                section.set("lore", im.getLore());
            section.set("unbreakable", Boolean.valueOf(im.isUnbreakable()));
        }
    }


    public Config setItemStack(String path, ItemStack item, boolean isArmor) {
        saveItem(createSection(path), item, isArmor);
        save();
        return this;
    }

}