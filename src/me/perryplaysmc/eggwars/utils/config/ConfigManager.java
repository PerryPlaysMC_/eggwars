package me.perryplaysmc.eggwars.utils.config;

import java.util.ArrayList;
import java.util.List;

public class ConfigManager {

    private static List<Config> configs;

    public static void addConfig(Config cfg) {
        if (configs == null) {
            configs = new ArrayList<>();
        }
        if ((!configs.contains(cfg)) && (cfg != null))
            configs.add(cfg);
    }

    public static List<Config> getConfigs() {
        if (configs == null) {
            configs = new ArrayList<>();
        }
        return configs;
    }

    public static void reloadAll() {
        if (configs == null) {
            configs = new ArrayList<>();
            return;
        }
        for (Config cfg : configs) {
            cfg.reload();
        }
    }
}
