package me.perryplaysmc.eggwars.utils.config;

import java.io.File;
import java.util.List;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.Location;
import org.bukkit.configuration.ConfigurationSection;

public class MapConfig {

    private String name;
    private Config cfg;

    public MapConfig(EggWarsMap map, String name)
    {
        this.name = name;
        String newName = "/maps/" + name + ".yml";
        cfg = new Config("plugins/EggWars", newName);
    }


    public int getMaxPlayers()
    {
        if (!isSet("Players.Max"))
            set("Players.Max", Utils.getConfig().getInt("Map-Settings.Defaults.MaxPlayers"));
        return getInt("Players.Max");
    }

    public int getMinPlayers() {
        if (!isSet("Players.Min"))
            set("Players.Min", Utils.getConfig().getInt("Map-Settings.Defaults.MinPlayers"));
        return getInt("Players.Min");
    }

    public int getMaxPlayersPerTeam() {
        if (!isSet("Players.MaxTeamSize"))
            set("Players.MaxTeamSize", Utils.getConfig().getInt("Map-Settings.Defaults.TeamSize"));
        return getInt("Players.MaxTeamSize");
    }

    public int getLobby() {
        if (!isSet("Timer.Lobby"))
            set("Timer.Lobby", Utils.getConfig().get("Map-Settings.Defaults.Timer.Lobby"));
        return getInt("Timer.Lobby");
    }

    public int getEnd() {
        if (!isSet("Timer.End"))
            set("Timer.End", Utils.getConfig().get("Map-Settings.Defaults.Timer.End"));
        return getInt("Timer.End");
    }

    public int getStarting() {
        if (!isSet("Timer.PreStart"))
            set("Timer.PreStart", Utils.getConfig().get("Map-Settings.Defaults.Timer.PreStart"));
        return getInt("Timer.PreStart");
    }

    public int getMaxPlayerTimer() {
        if (!isSet("Timer.MaxPlayerTimer"))
            set("Timer.MaxPlayerTimer", Utils.getConfig().get("Map-Settings.Defaults.Timer.MaxPlayerTimer"));
        return getInt("Timer.MaxPlayerTimer");
    }

    public boolean isSet(String path)
    {
        return cfg.isSetMap(name, path);
    }

    public MapConfig set(String path, Object obj) {
        if ((obj instanceof Location)) {
            cfg.setMap(name, path, Utils.serializeLoc((Location)obj));
            return this;
        }
        cfg.setMap(name, path, obj);
        return this;
    }

    public Object get(String path) {
        return cfg.getMap(name, path);
    }

    public ConfigurationSection getSection(String path) { return cfg.getMapSection(name, path); }


    public File getFile()
    {
        return cfg.getFile();
    }

    public Config getConfig() {
        return cfg;
    }

    public Boolean getBoolean(String path) {
        boolean j;
        if (cfg.getFile().getName().equalsIgnoreCase("maps.yml")) {
            j = cfg.getBoolean("Maps." + name + "." + path);
        } else {
            j = cfg.getBoolean(path);
        }
        return j;
    }

    public Integer getInt(String path) {
        int j;
        if (cfg.getFile().getName().equalsIgnoreCase("maps.yml")) {
            j = cfg.getInt("Maps." + name + "." + path);
        } else {
            j = cfg.getInt(path);
        }
        return j;
    }

    public List<String> getStringList(String path) {
        List<String> j;
        if (cfg.getFile().getName().equalsIgnoreCase("maps.yml")) {
            j = cfg.getStringList("Maps." + name + "." + path);
        } else {
            j = cfg.getStringList(path);
        }
        return j;
    }

    public String getString(String path) {
        String j;
        if (cfg.getFile().getName().equalsIgnoreCase("maps.yml")) {
            j = cfg.getString("Maps." + name + "." + path);
        } else {
            j = cfg.getString(path);
        }
        return j;
    }

    public Location getLocation(String path) {
        String j;
        if (cfg.getFile().getName().equalsIgnoreCase("maps.yml")) {
            j = cfg.getString("Maps." + name + "." + path);
        } else {
            j = cfg.getString(path);
        }
        return Utils.deserializeLoc(j);
    }
}
