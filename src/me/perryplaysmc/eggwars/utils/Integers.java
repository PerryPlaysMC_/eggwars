package me.perryplaysmc.eggwars.utils;

import me.perryplaysmc.eggwars.map.timer.GeneratorType;

public class Integers {
  private int int1;
  private int int2;
  private GeneratorType type;
  private org.bukkit.command.CommandSender s;
  
  public Integers(org.bukkit.command.CommandSender s, GeneratorType type, int int1, int int2) {
    this.int1 = int1;
    this.int2 = int2;
    this.type = type;
    this.s = s;
  }
  
  public int getInt2() {
    return int2;
  }
  
  public int getInt1() {
    return int1;
  }
  
  public GeneratorType getType() {
    return type;
  }
  
  public org.bukkit.command.CommandSender getS() {
    return s;
  }
}
