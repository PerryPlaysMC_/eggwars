package me.perryplaysmc.eggwars.utils;

import org.bukkit.scoreboard.Scoreboard;

public class ScoreboardUtil {
    private Scoreboard b;
    private org.bukkit.scoreboard.Objective ob;
    private ScoreboardUtil inst;
    private String name;
    private String SBcriteria;
    private String display;

    public ScoreboardUtil(String name, String SBcriteria) {
        inst = this;
        b = me.perryplaysmc.eggwars.EggWars.getInstance().registerNewScoreBoard();
        ob = b.registerNewObjective(name, SBcriteria);
        ob.setDisplaySlot(org.bukkit.scoreboard.DisplaySlot.SIDEBAR);
        this.name = name;
        this.SBcriteria = SBcriteria;
    }

    public ScoreboardUtil(String name, String SBcriteria, String display) {
        inst = this;
        b = me.perryplaysmc.eggwars.EggWars.getInstance().registerNewScoreBoard();
        ob = b.registerNewObjective(name, SBcriteria, Utils.translate(display));
        ob.setDisplaySlot(org.bukkit.scoreboard.DisplaySlot.SIDEBAR);
        this.name = name;
        this.display = display;
        this.SBcriteria = SBcriteria;
    }

    public ScoreboardUtil reset() {
        return new ScoreboardUtil(name, SBcriteria, display);
    }

    public ScoreboardUtil setDisplay(String name, org.bukkit.scoreboard.DisplaySlot slot)
    {
        ob.setDisplayName(Utils.translate(name));
        ob.setDisplaySlot(slot);
        return this;
    }

    public ScoreboardUtil addObjective(String name, String criteria) {
        if (b.getObjective(name) == null) {
            ob = b.registerNewObjective(name, criteria);
        } else
            ob = b.getObjective(name);
        return this;
    }

    public ScoreboardUtil setScore(int i, String id, String score) {
        org.apache.commons.lang.Validate.notNull(ob);
        setTeam(id, score);
        org.bukkit.scoreboard.Score s = ob.getScore(id);
        s.setScore(i);
        return this;
    }

    public ScoreboardUtil setScoreboard(Scoreboard b) {
        this.b = b;
        return this;
    }

    public org.bukkit.scoreboard.Team getTeam(String team) {
        return b.getTeam(Utils.translate(team));
    }

    public ScoreboardUtil setTeam(int i, String id, String prefix, String suffix) {
        org.apache.commons.lang.Validate.notNull(ob);
        org.bukkit.scoreboard.Team t = b.getTeam(id);
        if (t == null) {
            t = b.registerNewTeam(id);
        }
        t.addEntry(t(id));
        t.setPrefix(t(prefix));
        t.setSuffix(t(suffix));
        ob.getScore(t(id)).setScore(i);
        return this;
    }

    public ScoreboardUtil setTeam(int i, String teamId, String id, String prefix, String suffix) {
        org.apache.commons.lang.Validate.notNull(ob);
        org.bukkit.scoreboard.Team t = b.getTeam(teamId);
        if (t == null) {
            t = b.registerNewTeam(teamId);
        }
        t.addEntry(t(id));
        t.setPrefix(t(prefix));
        t.setSuffix(t(suffix));
        ob.getScore(t(id)).setScore(i);
        return this;
    }

    public ScoreboardUtil setTeam(String team, String score) {
        org.apache.commons.lang.Validate.notNull(ob);
        org.bukkit.scoreboard.Team t = b.getTeam(team);
        if (t != null) {
            t.unregister();
            t = null;
        }
        if (b.getTeam(team) == null) {
            org.bukkit.scoreboard.Team b1 = b.registerNewTeam(team);
            b1.addEntry(t("&f"));
            ob.getScore(t("&f")).setScore(3);
        }
        t = b.registerNewTeam(team);
        t.addEntry(Utils.translate(score));
        return this;
    }

    private String t(String a) {
        return Utils.translate(a);
    }

    public Scoreboard finish() {
        return b;
    }

    void setInstance(ScoreboardUtil u) {
        inst = u;
    }
}
