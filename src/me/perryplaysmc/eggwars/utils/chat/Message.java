package me.perryplaysmc.eggwars.utils.chat;

import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.chat.json.FancyMessage;
import me.perryplaysmc.eggwars.utils.chat.json.JsonRepresentedObject;
import me.perryplaysmc.eggwars.utils.chat.json.JsonString;
import me.perryplaysmc.eggwars.utils.chat.json.MessagePart;
import me.perryplaysmc.eggwars.utils.chat.json.command.JsonHandler;
import me.perryplaysmc.eggwars.utils.chat.json.command.Run;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

@SuppressWarnings("all")
public class Message {

    private FancyMessage base;
    public String last = "";
    public String original = "";
    private int latestSize;
    private CommandSender s;
    public Message() {
        this("");
    }

    public Message(String text) {
        this(null, text);
    }
    public Message(CommandSender u, String text) {
        base = new FancyMessage("");
        then(u, Utils.translate(text));
        this.s = u;
    }

    public Message then(String txtt) {
        return then(null, txtt, TextOptions.KEEP_COLORS);
    }

    public Message then(String txtt, TextOptions options) {
        return then(null, txtt, options);
    }

    public Message then(String perm, String txtt) {
        if(s!=null) {
            if(s.hasPermission(perm)) return then(txtt);
            else {
                return then("");
            }
        }
        return then(txtt);
    }

    public int size() {

        return base.getParts().size();
    }

    public Message then(boolean doLink, String txtt) {
        return then(doLink, null, txtt, TextOptions.KEEP_COLORS);
    }

    public Message breakLine() {
        return then(null, "\n", TextOptions.KEEP_COLORS);
    }

    public Message br() {
        return breakLine();
    }


    public Message then(boolean doLink, CommandSender u, String txtt) {
        return then(doLink, u, txtt, TextOptions.KEEP_COLORS);
    }

    public Message then(CommandSender u, String txtt) {
        return then(u, txtt, TextOptions.KEEP_COLORS);
    }
    public Message then() {
        return then(null,"", TextOptions.KEEP_COLORS);
    }

    public Message then(CommandSender u, String txtt, TextOptions options) {
        return then(false, u, txtt, options);
    }
    public Message then(boolean doLink, CommandSender u, String txtt, TextOptions options) {
        return thenRaw(doLink, u != null && u instanceof Player ? Utils.translateUserPerms(u, original, txtt) : Utils.translate(txtt), options);
    }

    private Message thenRaw(boolean doLink, String txtt, TextOptions options) {
        latestSize = 0;
        boolean isAllSpace = true;
        String space = "";
        for(char c : Utils.stripColor(txtt).toCharArray()) {
            if(c!=' ') {
                isAllSpace = false;
                break;
            }
            space+=" ";
        }
        if(isAllSpace) {
            base.then("");
            boolean addBold = false, addItalic = false, addUnderLine = false, addStrikeThrough = false, addMagic = false;
            ChatColor c = null;
            if(last.length() > 0) {
                String l = last.replace("§", "").replace("&", "");
                int id = l.length() + -1;
                set(last, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
                try {
                    while(ChatColor.getByChar(l.charAt(id)) == null || (c != null && !c.isColor())) {
                        if(id < 0) {
                            break;
                        }
                        c = ChatColor.getByChar(l.charAt(id));
                        id -= 1;
                    } ;
                }catch (Exception t){}
            }
            set(c, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
            if(addBold || addStrikeThrough || addUnderLine || addItalic || addMagic)
                base.then(last + space);
            else base.then(space);
            latestSize++;
            return this;
        }
        if(txtt.equals(" ")) {
            base.then("");
            boolean addBold = false, addItalic = false, addUnderLine = false, addStrikeThrough = false, addMagic = false;
            ChatColor c = null;
            if(last.length() > 0) {
                String l = last.replace("§", "").replace("&", "");
                int id = l.length() + -1;
                set(last, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
                try {
                    while(ChatColor.getByChar(l.charAt(id)) == null || (c != null && !c.isColor())) {
                        if(id < 0) {
                            break;
                        }
                        c = ChatColor.getByChar(l.charAt(id));
                        id -= 1;
                    } ;
                }catch (Exception t){}
            }
            set(c, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
            if(addBold || addStrikeThrough || addUnderLine || addItalic || addMagic)
                base.then(last + " ");
            else base.then(" ");
            latestSize++;
            return this;
        }
        String txt = last + txtt;
        String[] msgSplit = txt.split(" ");
        String og = Utils.getLastColors(msgSplit.length > 0 ? msgSplit[0] : "");
        if(original != "" && last.isEmpty()) last = original;
        if(last.isEmpty()) {
            last = og;
        }
        String s = "";
        for(int i = 0; i < msgSplit.length; i++) {
            String msg = msgSplit[i];
            if(options == TextOptions.KEEP_COLORS || options == TextOptions.ALL) {
                if(last.length()>3 && Utils.hasColor(last) && Utils.isColor(last))
                    last = last.substring(2);
                if(last.isEmpty()) {
                    last = Utils.getLastColors(msgSplit[i]);
                }
                if(!last.contains(Utils.getLastColors(msgSplit[i])))
                    last = Utils.getLastColors(msgSplit[i]);
            }
            ChatColor c = null;
            boolean addBold = false, addItalic = false, addUnderLine = false, addStrikeThrough = false, addMagic = false;
            if(last.length() > 0) {
                String l = last.replace("§", "").replace("&", "");
                int id = l.length() + -1;
                set(last, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
                try {
                    while(c == null || !c.isColor() || c.isFormat()) {
                        if(id < 0) {
                            c = null;
                            break;
                        }
                        c = ChatColor.getByChar(l.charAt(id));
                        id -= 1;
                        String x = c.name();
                    }
                }catch (Exception t){
                    System.out.println("Error");
                }
            }
            if(c != null) base.color(c);
            if(doLink) {
                String link = !Utils.stripColor(msg).startsWith("https://") ? "https://" + Utils.stripColor(msg) : Utils.stripColor(msg);
                if(Utils.checkForDomain(link)) {
                    base.then(last+msg).tooltip("[c]Click to go to: ", "[pc]" + link).link(link);
                }else {
                    base.then(last + msg);
                }
            }else {
                base.then(last + msg);
            }
            latestSize++;
            set(c, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
            if(options == TextOptions.KEEP_CLICK || options == TextOptions.ALL || options == TextOptions.KEEP_EVENTS) {
                String cc = base.getParts().get(base.getParts().size()+-2).clickActionName;
                String cc2 = base.getParts().get(base.getParts().size()+-2).clickActionData;
                base.onClick(cc, cc2);
            }
            if(options == TextOptions.KEEP_HOVER || options == TextOptions.KEEP_EVENTS || options == TextOptions.ALL) {
                String cc = base.getParts().get(base.getParts().size()+-2).hoverActionName;
                JsonRepresentedObject cc2 = base.getParts().get(base.getParts().size()+-2).hoverActionData;
                base.onHover(cc, cc2);
            }
            if(options != TextOptions.KEEP_COLORS && options != TextOptions.ALL)
                last = "";
            boolean isRunning = false;
            String spaces = "";
            try{
            if(!msg.isEmpty() && txtt.split(msg).length == 1 && txtt.split(msg)[1] != null)
                for(char cx : txtt.split(msg)[1].toCharArray()) {
                    if(cx == ' ') {
                        System.out.println("'"+cx+"'");
                        if(cx == ' ')
                            spaces += cx;
                    }
                }
            }catch(ArrayIndexOutOfBoundsException x) {

            }
            if(spaces == "") spaces=" ";
            if(i < (msgSplit.length+-1) || txtt.endsWith(spaces)) {
                if(addBold || addStrikeThrough || addUnderLine || addItalic || addMagic) {
                    set(c, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
                    base.then(last + spaces);
                } else {
                    base.then(last + spaces);
                }
                set(c, addBold, addStrikeThrough, addUnderLine, addItalic, addMagic);
                latestSize++;
            }
        }
        return this;
    }

    private void set(String last, boolean addBold, boolean addStrikeThrough, boolean addUnderLine, boolean addItalic, boolean addMagic) {
        if(last.toLowerCase().contains("l"))
            addBold = true;
        if(last.toLowerCase().contains("m"))
            addStrikeThrough = true;
        if(last.toLowerCase().contains("n"))
            addUnderLine = true;
        if(last.toLowerCase().contains("o"))
            addItalic = true;
        if(last.toLowerCase().contains("k"))
            addMagic = true;
    }

    private void set(ChatColor c, boolean addBold, boolean addStrikeThrough, boolean addUnderLine, boolean addItalic, boolean addMagic) {
        if(c != null) base.color(c);
        if(addBold)base.style(ChatColor.BOLD);
        if(c != null) base.color(c);
        if(addStrikeThrough)base.style(ChatColor.STRIKETHROUGH);
        if(c != null) base.color(c);
        if(addUnderLine)base.style(ChatColor.UNDERLINE);
        if(c != null) base.color(c);
        if(addItalic)base.style(ChatColor.ITALIC);
        if(c != null) base.color(c);
        if(addMagic)base.style(ChatColor.MAGIC);
        if(c != null) base.color(c);
    }

    public Message command(String text) {
        if(!text.startsWith("/")) text = "/" + text;
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = Utils.stripColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }

    public Message command(Run r, String... args) {
        String text = UUID.randomUUID().toString();
        while(JsonHandler.getCommand(text) != null) {
            text = UUID.randomUUID().toString();
        }
        JsonHandler.addCommand(text, r);
        String arguments = "";
        if(args.length > 0) {
            arguments = " ";
            for(int i = 0; i < args.length; i++) {
                arguments+=args[i]+" ";
            }
            arguments.substring(0, arguments.length()+-1);
        }
        text += arguments;
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = Utils.stripColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }

    public Message forceChat(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = Utils.stripColor(text);
            base.getParts().get(i).clickActionName = "run_command";
        }
        return this;
    }

    public Message suggest(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = Utils.stripColor(text);
            base.getParts().get(i).clickActionName = "suggest_command";
        }
        return this;
    }

    public Message insert(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).insertionData = Utils.stripColor(text);
        }
        return this;
    }

    public Message link(String text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).clickActionData = Utils.stripColor(text);
            base.getParts().get(i).clickActionName = "open_url";
        }
        return this;
    }

    public Message tooltip(ItemStack item) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            base.getParts().get(i).hoverActionData = new JsonString(Utils.convertItemStackToJson(item));
            base.getParts().get(i).hoverActionName = "show_item";
        }
        return this;
    }
    public Message hover(ItemStack item) {
        return tooltip(item);
    }

    public Message hover(String... text) {
        for(int i = base.getParts().size()+-latestSize; i < base.getParts().size(); i++) {
            StringBuilder builder = new StringBuilder();
            for (int ii = 0; ii < text.length; ii++) {
                builder.append(text[ii]);
                if (ii != text.length - 1) {
                    builder.append('\n');
                }
            }
            base.getParts().get(i).hoverActionData = new JsonString(Utils.translate(builder.toString()));
            base.getParts().get(i).hoverActionName = "show_text";
        }
        return this;
    }

    public Message tooltip(String... text) {
        return hover(text);
    }

    public void send(CommandSender u) {
        base.send(u);
    }
    public void send(Player u) {
        base.send(u);
    }



}
