package me.perryplaysmc.eggwars.utils.chat;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

public class Info {


    SubCommand cmd;
    Message fb;
    CommandSender s;
    int amount = 0;

    public Info(String cmd) {
        fb = new Message("[c]"+((cmd.charAt(0)+"").toUpperCase()+cmd.substring(1))+" Commands: ");
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(CommandSender s, String cmd) {
        this.s = s;
        fb = new Message("[c]"+((cmd.charAt(0)+"").toUpperCase()+cmd.substring(1))+" Commands: ");
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(CommandSender s, String[] permissions, String cmd) {
        this.s = s;
        fb = new Message("[c]"+((cmd.charAt(0)+"").toUpperCase()+cmd.substring(1))+" Commands: ");
        if(Utils.hasPermission(s, permissions)) {
            fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
        }else {
            fb.then("\n    [c]- You do not have permission to view these commands.");
        }
    }

    public Info(SubCommand cmd) {
        this.cmd = cmd;
        fb = new Message("[c]"+((cmd.getName().charAt(0)+"").toUpperCase()+cmd.getName().substring(1))+" Commands: ");
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(SubCommand cmd, String msg) {
        this.cmd = cmd;
        fb = new Message(msg);
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }
    public Info(SubCommand cmd, CommandSender s) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message("[c]"+((cmd.getName().charAt(0)+"").toUpperCase()+cmd.getName().substring(1))+": ");
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(SubCommand cmd, CommandSender s, String msg) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message(msg);
        fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
    }

    public Info(SubCommand cmd, CommandSender s, String msg, String[] permissions) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message(msg);
        if(Utils.hasPermission(s, permissions)) {
            fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
        }else {
            fb.then("\n    [c]- You do not have permission to view these commands.");
        }
    }

    public Info(SubCommand cmd, CommandSender s, String[] permissions) {
        this.cmd = cmd;
        this.s = s;
        fb = new Message("[c]"+((cmd.getName().charAt(0)+"").toUpperCase()+cmd.getName().substring(1))+": ");
        if(Utils.hasPermission(s, permissions)) {
            fb.then("\n\n[c]Hover for info!\n\n[c] [o][argument][c] &7= Optional \n [r]<argument>[c] &7= Required\n");
        }else {
            fb.then("\n    [c]- You do not have permission to view these commands.");
        }
    }

    public Info setPermissions(List<String> permissions) {
        this.perms = permissions.toArray(new String[permissions.size()]);
        return this;
    }

    public Info setSource(CommandSender s) {
        this.s = s;
        return this;
    }

    String[] perms = new String[]{};
    public Info addSub(String sub) {
        amount++;
        fb.then("\n [c]-/[pc]" + cmd.getName() + " " + (sub.length()>0?" " + sub:""));
        return this;
    }

    public Info addSub(String command, String sub) {
        amount++;
        fb.then("\n [c]-/[pc]" + command + (sub.length()>0?" " + sub:""));
        return this;
    }

    public Info addSub(String[] permissions, String command, String sub) {
        if(s != null) {
            perms = permissions;
            if(Utils.hasPermission(s, permissions))
                return addSub(command, sub);
            else return this;
        }
        perms = new String[]{};
        return addSub(command, sub);
    }

    public Info addSub(String[] permissions, String sub) {
        if(s != null) {
            perms = permissions;
            if(Utils.hasPermission(s, permissions))
                return addSub(sub);
            else return this;
        }
        perms = new String[]{};
        return addSub(sub);
    }

    public Info tooltip(String... messages) {
        if(s != null) {
            if(Utils.hasPermission(s, perms) || perms.length == 0) {
                fb.tooltip(messages);
            }
        }else {
            fb.tooltip(messages);
        }
        return this;
    }

    public Info command(String command) {
        if(s != null) {
            if(Utils.hasPermission(s, perms) || perms.length == 0) {
                fb.command(command);
            }
        }else {
            fb.command(command);
        }
        return this;
    }

    public Info suggest(String command) {
        if(s != null) {
            if(Utils.hasPermission(s, perms) || perms.length == 0) {
                fb.suggest(command);
            }
        }else {
            fb.suggest(command);
        }
        return this;
    }

    public Info insert(String command) {
        if(s != null) {
            if(Utils.hasPermission(s, perms) || perms.length == 0) {
                fb.insert(command);
            }
        }else {
            fb.insert(command);
        }
        return this;
    }

    public void send(CommandSender s) {
        fb.send(s);
    }
    public void send(Player s) {
        fb.send(s);
    }



}
