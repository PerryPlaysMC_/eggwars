package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.timer.GeneratorType;
import me.perryplaysmc.eggwars.utils.Integers;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.items.Items;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Generator implements SubCommand {


    public String getName() {
        return "gen";
    }

    public String getInfo()
    {
        return "Get a generator block to place.";
    }

    public String[] getAliases()
    {
        return new String[] { "generator" };
    }

    public Integer[] argsLength()
    {
        return new Integer[] {1, 3};
    }

    public String[] getArguments()
    {
        return new String[] { "<GeneratorType>", "[Integer]", "[Integer]" };
    }



    public boolean selectMap()
    {
        return true;
    }

    public boolean isPlayer()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "generator", "gen")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.generator");
            return;
        }
        if (!(s instanceof Player)) {
            s.sendMessage("You must be a player for this.");
            return;
        }
        if ((!EggWarsCommand.getSelected().containsKey(s)) || (EggWarsCommand.getSelected().get(s) == null)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        if (args[0].equalsIgnoreCase("diamond")) {
            if (!((Player)s).getInventory().contains(Items.getDiamondGenerator())) {
                ((Player)s).getInventory().addItem(Items.getDiamondGenerator());
            }
        } else if (args[0].equalsIgnoreCase("emerald")) {
            if (!((Player)s).getInventory().contains(Items.getEmeraldGenerator())) {
                ((Player)s).getInventory().addItem(Items.getEmeraldGenerator());
            }
        } else if (args[0].equalsIgnoreCase("gold")) {
            if (!((Player)s).getInventory().contains(Items.getGoldGenerator())) {
                ((Player)s).getInventory().addItem(Items.getGoldGenerator());
            }
        } else if (args[0].equalsIgnoreCase("iron")) {
            if (!((Player)s).getInventory().contains(Items.getIronGenerator()))
                ((Player)s).getInventory().addItem(Items.getIronGenerator());
        } else {
            Utils.sendMessage(s, "Map.invalid-generator");
            return;
        }
        if (args.length == 4) {
            if (!Utils.isInt(args[1])) {
                Utils.sendMessage(s, "Error.validNumber", selected.get(s).getName(), args[1]);
                return;
            }
            if (!Utils.isInt(args[2])) {
                Utils.sendMessage(s, "Error.validNumber", selected.get(s).getName(), args[2]);
                return;
            }
            for (int i = 0; i < EggWarsCommand.getGens().size(); i++) {
                if ((EggWarsCommand.getGens().get(i).getS().getName() == s.getName()) && (EggWarsCommand.getGens().get(i).getType() == GeneratorType.valueOf(args[0].toUpperCase()))) {
                    EggWarsCommand.getGens().remove(i);
                }
            }
            EggWarsCommand.getGens().add(new Integers(s, GeneratorType.valueOf(args[0].toUpperCase()), Integer.parseInt(args[1]), Integer.parseInt(args[2])));
        }
        Utils.sendMessage(s, "Map.give-generator", (""+args[0].charAt(0)).toUpperCase() + args[0].substring(1).toLowerCase());
    }
}
