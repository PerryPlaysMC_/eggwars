package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetEgg implements SubCommand {

    public String getName() {
        return "setegg";
    }

    public String getInfo()
    {
        return "Set's the egg for selected team";
    }

    public Integer argLength()
    {
        return 1;
    }

    public boolean selectMap()
    {
        return true;
    }

    public boolean selectTeam()
    {
        return true;
    }


    public boolean isPlayer()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "setegg")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setegg");
            return;
        }
        if (!(s instanceof Player)) {
            s.sendMessage("You must be a player for this.");
            return;
        }
        if (!teams.containsKey(s)) {
            Utils.sendMessage(s, "Team.selectTeam");
            return;
        }
        Player p = (Player)s;
        EggWarsTeam t = teams.get(s);
        Location l = p.getTargetBlock(null, 15).getLocation().clone().add(0.0D, 1.0D, 0.0D);
        t.setEggLocation(l);
        Utils.sendMessage(s, "Team.eggLocationSet", t.getName(), Utils.serializeLoc(l).replace(selected.get(s).getId().toString(), ""));
    }
}
