package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddSpawn implements SubCommand {

    public String getName() {
        return "addspawn";
    }

    public String getInfo()
    {
        return "Adds a spawn for a team";
    }

    public Integer argLength()
    {
        return 0;
    }

    public boolean selectMap()
    {
        return true;
    }

    public boolean selectTeam()
    {
        return true;
    }

    public boolean isPlayer()
    {
        return true;
    }


    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "addspawn")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.addspawn");
            return;
        }
        if (!(s instanceof Player)) {
            s.sendMessage("You must be a player for this.");
            return;
        }
        if (!teams.containsKey(s)) {
            Utils.sendMessage(s, "Team.selectTeam");
            return;
        }
        EggWarsTeam t = teams.get(s);
        if (t.getSpawns().size() == selected.get(s).getMaxPlayersPerTeam()) {
            Utils.sendMessage(s, "Team.spawnsMaxed", t.getName(), "§" + t.getColor().getChar());
            return;
        }
        t.addSpawn(((Player)s).getLocation());
        Utils.sendMessage(s, "Team.addedSpawn", t.getName(), "§" + t.getColor().getChar());
    }
}
