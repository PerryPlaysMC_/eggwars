package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpectate implements SubCommand {


    public String getName() {
        return "setspectate";
    }

    public String getInfo()
    {
        return "Set the spectator respawn";
    }

    public boolean isPlayer()
    {
        return true;
    }

    public boolean selectMap()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "setspectate")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setspectate");
            return;
        }
        if ((!selected.containsKey(s)) || (selected.get(s) == null)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        EggWarsMap m = selected.get(s);
        if (!(s instanceof Player)) {
            s.sendMessage("You must be a player for this.");
            return;
        }
        if (!Utils.hasPermission(s, "setspectate")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setspectate");
            return;
        }
        m.setRespawn(((Player)s).getLocation());
        Utils.sendMessage(s, "Map.setSpectate", m.getName(), Utils.serializeLoc(((Player)s).getLocation()));
    }
}
