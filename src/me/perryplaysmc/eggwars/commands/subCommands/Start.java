package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Start implements SubCommand {
    @Override
    public String getName() {
        return "start";
    }

    @Override
    public String getInfo() {
        return "Starts the game!";
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "start")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.start");
            return;
        }
        EggWarsMap m  = MapManager.byPlayer((Player) s) == null ? selected.get(s) : MapManager.byPlayer((Player) s);
        if (m == null) {
            Utils.sendMessage(s, "Map.notInGame");
            return;
        }
        m.getTimer().setPaused(false);
    }
}
