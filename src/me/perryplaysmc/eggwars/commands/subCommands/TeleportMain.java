package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TeleportMain implements SubCommand {
    @Override
    public String getName() {
        return "teleportmain";
    }

    @Override
    public String[] getAliases() {
        return new String[] {"tpmain", "tpm"};
    }
    @Override
    public String getInfo() {
        return "Teleport to the main lobby";
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        Player p = (Player) s;
        if(Utils.deserializeLoc(Utils.getConfig().getString("Locations.mainLobby")) == null) {
            Utils.sendMessage(s, "Map.teleport-fail", selected.get(p).getName());
            return;
        }
        Utils.sendMessage(s, "Map.teleportMain");
        p.teleport(Utils.deserializeLoc(Utils.getConfig().getString("Locations.mainLobby")));
    }
}
