package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;

public class SetState implements me.perryplaysmc.eggwars.utils.SubCommand {

    public String getName() {
        return "setstate";
    }

    public String getInfo()
    {
        return "Set the GameState for the selected map";
    }

    public Integer argLength()
    {
        return 1;
    }


    public boolean selectMap()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "setstate")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setstate");
            return;
        }
        if ((!selected.containsKey(s)) || (selected.get(s) == null)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        selected.get(s).setState(GameState.value(args[0].toUpperCase().replace("PRESTART", "PRE_START")));
        if (GameState.value(args[0].toUpperCase().replace("PRESTART", "PRE_START")) != GameState.LOBBY) {
            selected.get(s).getTimer().setPaused(false);
        }
        Utils.sendMessage(s, "Map.setState", selected.get(s).getName(), GameState.value(args[0].toUpperCase().replace("PRESTART", "PRE_START")).getLocalizedName());
    }
}
