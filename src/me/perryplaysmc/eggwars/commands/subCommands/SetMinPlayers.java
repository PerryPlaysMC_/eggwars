package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.SubCommand;
import org.bukkit.command.CommandSender;

import static me.perryplaysmc.eggwars.utils.Utils.*;

public class SetMinPlayers implements SubCommand {

    public String getName() {
        return "setminplayers";
    }

    public String getInfo()
    {
        return "Set the minimum player count for the selected map";
    }

    public Integer argLength() {
        return 1;
    }

    public String[] getArguments()
    {
        return new String[] { "<Integer>" };
    }


    public boolean selectMap()
    {
        return true;
    }



    public void execute(CommandSender s, String[] args) {

        if (!hasPermission(s, "setminplayers")) {
            sendMessage(s, "Error.invalidPermission", "eggwars.command.setminplayers");
            return;
        }
        if ((!selected.containsKey(s)) || (selected.get(s) == null)) {
            sendMessage(s, "Team.selectMap");
            return;
        }
        EggWarsMap m = selected.get(s);
        if (!isInt(args[0])) {
            sendMessage(s, "Error.validNumber", selected.get(s).getName(), args[0]);
            return;
        }
        m.setMinPlayers(Integer.parseInt(args[0]));
        sendMessage(s, "Map.setMinPlayer", selected.get(s).getName(), Integer.parseInt(args[0]), args[0]);
    }
}
