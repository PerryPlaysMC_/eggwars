package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;

public class SetMaxPlayers implements SubCommand {

    public String getName() {
        return "setmaxplayers";
    }

    public String getInfo()
    {
        return "Set the max player count for the selected map";
    }

    public Integer argLength()
    {
        return 1;
    }

    public String[] getArguments()
    {
        return new String[] { "<Integer>" };
    }

    public boolean selectMap()
    {
        return true;
    }



    public void execute(CommandSender s, String[] args) {

        if (!Utils.hasPermission(s, "setmaxplayers")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setmaxplayers");
            return;
        }
        if ((!selected.containsKey(s)) || (selected.get(s) == null)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        EggWarsMap m = selected.get(s);
        if (!Utils.isInt(args[0])) {
            Utils.sendMessage(s, "Error.validNumber", selected.get(s).getName(), args[0]);
            return;
        }
        m.setMaxPlayers(Integer.parseInt(args[0]));
        Utils.sendMessage(s, "Map.setMaxPlayer", selected.get(s).getName(), Integer.parseInt(args[0]), args[0]);
    }
}
