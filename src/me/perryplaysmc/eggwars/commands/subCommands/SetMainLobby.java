package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.entity.Player;

public class SetMainLobby implements SubCommand {
    public String getName()
    {
        return "setmainlobby";
    }

    public String getInfo()
    {
        return "Sets the main lobby location";
    }

    public boolean isPlayer() {
        return true;
    }

    public void execute(org.bukkit.command.CommandSender s, String[] args) {
        if (!(s instanceof org.bukkit.entity.Player)) {
            s.sendMessage("You must be a player for this.");
            return;
        }
        if (!Utils.hasPermission(s, "setmainlobby")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setmainlobby");
            return;
        }
        Utils.getConfig().set("Locations.mainLobby", Utils.serializeLoc(((Player)s).getLocation()));
        Utils.sendMessage(s, "Main.setMainLobby", Utils.serializeLoc(((Player)s).getLocation()));
    }
}
