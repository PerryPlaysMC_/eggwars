package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetLobby implements SubCommand {

    public String getName() {
        return "setlobby";
    }

    public String getInfo()
    {
        return "Sets the selected maps lobby";
    }


    public boolean selectMap()
    {
        return true;
    }


    public boolean isPlayer()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "setlobby")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setlobby");
            return;
        }
        if ((!selected.containsKey(s)) || (selected.get(s) == null)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        EggWarsMap m = selected.get(s);
        if (!(s instanceof Player)) {
            s.sendMessage("You must be a player for this.");
            return;
        }
        selected.get(s).setLobby(((Player)s).getLocation());
        Utils.sendMessage(s, "Map.setLobby", m.getName(), Utils.serializeLoc(((Player)s).getLocation()));
    }
}
