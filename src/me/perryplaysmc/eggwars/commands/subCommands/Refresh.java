package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.timer.Generator;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.config.Config;
import me.perryplaysmc.eggwars.utils.config.ConfigManager;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

public class Refresh implements SubCommand {

    public String getName()
    {
        return "refresh";
    }

    public String getInfo()
    {
        return "Refresh all maps";
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "refresh")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.refresh");
            return;
        }
        ConfigManager.reloadAll();
        for (EggWarsMap mm : MapManager.getMaps()) {
            mm.endGame();
        }
        Utils.sendMessage(s, "Map.refresh");
    }
}
