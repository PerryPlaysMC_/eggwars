package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Test implements SubCommand {
    @Override
    public String getName() {
        return "test";
    }

    @Override
    public String getInfo() {
        return "Puts the game into test mode!";
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        EggWarsMap m  = MapManager.byPlayer((Player) s);
        if (m == null) {
            Utils.sendMessage(s, "Map.notInGame");
            return;
        }
        Utils.sendRaw(s, "[c]is Test: [pc]" + !m.isTesting());
        m.setTesting(!m.isTesting());
    }
}
