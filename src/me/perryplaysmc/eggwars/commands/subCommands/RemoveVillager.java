package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.util.BlockIterator;

public class RemoveVillager implements SubCommand {

    public String getName() {
        return "removevillager";
    }

    public String getInfo()
    {
        return "Removes the nearby villager of a player";
    }

    public boolean selectMap()
    {
        return true;
    }

    public boolean isPlayer()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if (!(s instanceof Player)) {
            s.sendMessage("§cYou must be a player");
            return;
        }
        if (!Utils.hasPermission(s, "removevillager")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.removevillager");
            return;
        }
        if (!selected.containsKey(s)) {
            Utils.sendMessage(s, "Team.selectTeam");
            return;
        }
        Player p = (Player)s;
        LivingEntity v1 = getTarget(p);
        if ((!(v1 instanceof Villager)) || (!selected.get(s).getVillagers().contains(v1)) || (v1 == null)) {
            Utils.sendMessage(s, "Map.noValidVillager");
            return;
        }
        Villager v = (Villager)v1;
        selected.get(s).removeVillager(v);
        Utils.sendMessage(s, "Map.removedVilager");
    }

    LivingEntity getTarget(Player p) {
        List<Entity> nearbyE = p.getNearbyEntities(10.0D, 10.0D, 10.0D);
        ArrayList<LivingEntity> livingE = new ArrayList<>();

        for (Entity e : nearbyE) {
            if ((e instanceof LivingEntity)) {
                livingE.add((LivingEntity)e);
            }
        }

        LivingEntity target = null;
        BlockIterator bItr = new BlockIterator(p, 10);

        int bx;

        int by;
        int bz;
        while (bItr.hasNext()) {
            Block block = bItr.next();
            bx = block.getX();
            by = block.getY();
            bz = block.getZ();

            for (LivingEntity e : livingE) {
                Location loc = e.getLocation();
                double ex = loc.getX();
                double ey = loc.getY();
                double ez = loc.getZ();
                if ((bx - 0.75D <= ex) && (ex <= bx + 1.75D) && (bz - 0.75D <= ez) && (ez <= bz + 1.75D) && (by - 1 <= ey) && (ey <= by + 2.5D)) {
                    target = e;
                    break;
                }
            }
        }
        return target;
    }
}
