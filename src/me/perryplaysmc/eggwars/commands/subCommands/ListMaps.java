package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;

public class ListMaps implements SubCommand {

    public String getName()
    {
        return "list";
    }

    public String getInfo()
    {
        return "List all maps";
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "list")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.list");
            return;
        }
        List<String> maps = new ArrayList<>();
        int i = 0;
        for (EggWarsMap mm : MapManager.getMaps()) {
            i++;
            StringBuilder sb = new StringBuilder();
            sb.append("[pc]" + mm.getName() + "[c]:")
                    .append("\n   [c]UniqueId: [pc]" + mm.getId().toString())
                    .append("\n   [c]State: [pc]" + mm.getState().getLocalizedName())
                    .append("\n   [c]Max: [pc]" + mm.getMaxPlayers())
                    .append("\n   [c]Min: [pc]" + mm.getMinPlayers())
                    .append("\n   [c]Current: [pc]" + mm.getPlayers().size());
            maps.add(sb.toString().trim()); }
        EggWarsMap mm;
        StringBuilder sb = new StringBuilder();
        for (String ss : maps) {
            i++;
            String space = i == 1 ? "" : " ";
            sb.append(ss).append("\n" + space);
        }
        Utils.sendMessage(s, "Map.List", sb.toString().trim());
    }
}
