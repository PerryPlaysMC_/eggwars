package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.map.team.EggWarsTeamManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;

public class AddTeam implements SubCommand {

    public String getName() {
        return "addteam";
    }

    public String getInfo()
    {
        return "Add a team to the selected map";
    }

    public Integer argLength()
    {
        return 3;
    }

    public String[] getArguments()
    {
        return new String[] { "<Name>", "<ChatColor>", "<DyeColor>" };
    }



    public boolean selectMap()
    {
        return true;
    }


    public void execute(CommandSender s, String[] args) {

        if (!Utils.hasPermission(s, "addteam")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.addteam");
            return;
        }
        if (!selected.containsKey(s)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        if (EggWarsTeamManager.mapHasTeam(selected.get(s), args[0])) {
            Utils.sendMessage(s, "Team.teamAlreadyExists", selected.get(s).getName(), args[0], EggWarsTeamManager.getTeam(selected.get(s), args[0]).getName());
            return;
        }

        EggWarsTeam team = new EggWarsTeam(selected.get(s), args[0], Utils.byNameChat(args[1]), Utils.byNameDye(args[2]));
        EggWarsCommand.getTeams().put(s, team);
        Utils.sendMessage(s, "Team.create", args[0], team.getColor().name(), team
                .getDyeColor().name());
    }
}
