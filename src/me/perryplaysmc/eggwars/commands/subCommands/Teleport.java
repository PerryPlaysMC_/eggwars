package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Teleport implements SubCommand {
    @Override
    public String getName() {
        return "teleport";
    }

    @Override
    public String[] getAliases() {
        return new String[] {"tp"};
    }

    @Override
    public String getInfo() {
        return "Teleport to a map's Lobby";
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public boolean selectMap() {
        return true;
    }

    @Override
    public void execute(CommandSender s, String[] args) {
        Player p = (Player) s;
        if(selected.get(s).getLobbyLoc() == null) {
            Utils.sendMessage(s, "Map.teleport-fail", selected.get(p).getName());
            return;
        }
        Utils.sendMessage(s, "Map.teleport", selected.get(p).getName());
        p.teleport(selected.get(s).getLobbyLoc());
    }
}
