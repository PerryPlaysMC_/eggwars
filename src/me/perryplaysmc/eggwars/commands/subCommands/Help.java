package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.chat.Info;
import me.perryplaysmc.eggwars.utils.chat.Message;
import org.bukkit.command.CommandSender;
import org.bukkit.util.ChatPaginator;

@SuppressWarnings("all")
public class Help implements SubCommand {

    private List<SubCommand> cmds = EggWarsCommand.getCommands();

    public String getName() {
        return "help";
    }

    public String getInfo()
    {
        return "Displays help message";
    }

    @Override
    public String[] getArguments() {
        return new String[] {"<integer>"};
    }

    @Override
    public Integer[] argsLength() {
        return new Integer[]{0, 1};
    }

    public String[] getAliases()
    {
        return new String[] { "?", "info" };
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "help")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.help");
            return;
        }
        if(args.length == 0) {
            getPage(s, 1).send(s);
            return;
        }
        if(Utils.isInt(args[0])) {
            getPage(s, Integer.parseInt(args[0])).send(s);
        }else {
            Utils.sendMessage(s, "Error.validNumber");
        }
    }

    public Message getPage(CommandSender sender, int index) {
        String cmdz = "";
        int i = 0;
        List<String> nS = new ArrayList<>();
        for(SubCommand cmd : cmds) {
            if(cmd.getArguments()==null)
                nS.add(cmd.getName());
        }
        Collections.sort(nS);
        for(String s : nS) {
            cmdz+=i == 0 ? s : "\n " + s;
            i++;
        }
        nS = new ArrayList<>();
        for(SubCommand cmd : cmds) {
            if(cmd.getArguments()!=null)
                nS.add(cmd.getName());
        }
        Collections.sort(nS);
        for(String s : nS) {
            cmdz+=i == 0 ? s : "\n " + s;
            i++;
        }
        ChatPaginator.ChatPage page = ChatPaginator.paginate(cmdz, index, Integer.MAX_VALUE, 10);
        for(i = 0; i < 20; i++) sender.sendMessage("");
        Message msg = new Message("[pc]&m&l            &r[c]EggWars[pc]([c]"+index+"[pc]/[c]"+page.getTotalPages()+"[pc])&r[pc]&m&l            &r");
        String spacez = "";
        for(char c : (Utils.translate("[pc]&m&l              ")).toCharArray()) {
            spacez+=" ";
        }
        msg.then("\n").then(spacez);
        if((index+-1) > 0) {
            msg.then("[pc]&l«&r").command("eggwars ? " + (index+-1))
                    .tooltip("[c]Click to turn the page!",
                            "[c]Previous Page: [pc]" + (index+-1));
        }else {
            msg.then("&8&l«&r");
        }
        msg.then("  ");
        if((index+1) <= page.getTotalPages()) {
            msg.then("[pc]&l»&r").command("eggwars ? " + (index+1))
                    .tooltip("[c]Click to turn the page!",
                    "[c]Next Page: [pc]" + (index+1));
        }else {
            msg.then("&8&l»&r");
        }
        for(String s : page.getLines()) {
            SubCommand cmd = EggWarsCommand.byName(Utils.stripColor(s));
            if(cmd == null) {
                msg.then("\n [c]-[pc] invalid command. [pc]" + Utils.stripColor(s));
                continue;
            }
            if(Utils.hasPermission(sender, cmd.permission()));
            String arguments = "", arguments2 = "";
            if (cmd.getArguments() != null) {
                for (String arg : cmd.getArguments()) {
                    arguments = arguments + " " + arg;
                }
            }
            if (cmd.getArguments() != null) {
                if(cmd.getArguments().length == 1) {
                    arguments2+=" 1";
                }else {
                    for(i = 0; i < cmd.getArguments().length; i++) {
                        arguments2 += " " + (i + 1);
                    }
                }
            }
            if(arguments.length() == 0) {
                msg.then("\n [c]-/[pc]eggwars " + cmd.getName()).tooltip(
                        "[c]Info:",
                        "[pc]"+cmd.getInfo(),
                        "[c]Click to run:",
                        "[c]/[pc]eggwars "+cmd.getName()).command("eggwars " + cmd.getName());
                continue;
            }
            msg.then("\n [c]-/[pc]eggwars " + cmd.getName() + arguments).tooltip(
                    "[c]Info:",
                    "[pc]"+cmd.getInfo(),
                    "[c]Click to insert:",
                    "[c]/[pc]eggwars "+cmd.getName() + arguments
            ).suggest("/eggwars " + cmd.getName() + arguments2);
        }
        return msg;
    }

}
