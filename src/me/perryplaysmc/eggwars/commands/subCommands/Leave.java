package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.entity.Player;

public class Leave implements SubCommand {

    public String getName()
    {
        return "leave";
    }

    public String getInfo()
    {
        return "Leave the game you are currently in.";
    }

    public String[] getAliases()
    {
        return new String[] { "lobby" };
    }

    public boolean isPlayer() {
        return true;
    }

    public void execute(org.bukkit.command.CommandSender s, String[] args) {
        if (!(s instanceof Player)) {
            s.sendMessage("§cYou must be a player.");
            return;
        }
        if (!Utils.hasPermission(s, "leave")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.leave");
            return;
        }
        Player p = (Player)s;
        if (MapManager.byPlayer(p) == null) {
            Utils.sendMessage(s, "Map.notInGame");
            return;
        }
        MapManager.byPlayer(p).removePlayer(p);
    }
}
