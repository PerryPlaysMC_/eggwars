package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;

public class Reload implements SubCommand {

    public String getName()
    {
        return "reload";
    }

    public String getInfo()
    {
        return "Reload the config files";
    }

    public void execute(org.bukkit.command.CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "reload")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.reload");
            return;
        }
        me.perryplaysmc.eggwars.utils.config.ConfigManager.reloadAll();
        Utils.sendMessage(s, "Config.reload");
    }
}
