package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import java.util.List;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class AddVillager implements SubCommand {

    public String getName() {
        return "addvillager";
    }

    public String getInfo()
    {
        return "Adds a villager to the game";
    }

    public boolean selectMap()
    {
        return true;
    }


    public boolean isPlayer()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if (!(s instanceof Player)) {
            s.sendMessage("§cYou must be a player to do this");
            return;
        }
        if (!Utils.hasPermission(s, "addvillager")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.addvillager");
            return;
        }
        if (!selected.containsKey(s)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        EggWarsMap m = selected.get(s);
        if (m.getVillagers().size() == m.getTeams().size()) {
            Utils.sendMessage(s, "Map.enoughVillagers");
            return;
        }
        Utils.sendMessage(s, "Map.addedVillager");
        m.addVillager(((Player)s).getLocation());
    }
}
