package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Join implements SubCommand {

    public String getName() {
        return "join";
    }

    public String getInfo()
    {
        return "Join the selected map";
    }

    public boolean selectMap()
    {
        return true;
    }

    @Override
    public String[] getArguments() {
        return new String[] {"[Map]"};
    }

    @Override
    public Integer[] argsLength() {
        return new Integer[] {1, 2};
    }

    public boolean isPlayer()
    {
        return true;
    }

    public void execute(CommandSender s, String[] args) {
        if(args.length == 1) {
            if(!Utils.hasPermission(s, "join")) {
                Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.join");
                return;
            }
            if(!(s instanceof Player)) {
                s.sendMessage("You must be a player for this.");
                return;
            }
            if(((!selected.containsKey(s)) || (selected.get(s) == null))) {
                Utils.sendMessage(s, "Team.selectMap");
                return;
            }
            EggWarsMap m = selected.get(s);
            m.addPlayer((Player) s);
            m.updateSign();
        }else if(args.length == 2) {
            if(!Utils.hasPermission(s, "join")) {
                Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.join");
                return;
            }
            if(!(s instanceof Player)) {
                s.sendMessage("You must be a player for this.");
                return;
            }
            if(MapManager.byName(args[0]) == null) {
                Utils.sendMessage(s, "Map.doesntExist");
                return;
            }
            EggWarsMap m = selected.get(s);
            m.addPlayer((Player) s);
            m.updateSign();
        }
    }
}
