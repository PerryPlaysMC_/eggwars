package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.map.team.EggWarsTeamManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;

public class Select implements SubCommand {

    public String getName() { return "select"; }


    public String getInfo()
    {
        return "Select a map or team";
    }

    public Integer argLength()
    {
        return 2;
    }


    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "select")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.select");
            return;
        }
        if (args[0].equalsIgnoreCase("map")) {
            EggWarsMap m = MapManager.byName(args[1]);
            if (m == null) {
                Utils.sendMessage(s, "Map.doesntExist", args[1]);
                return;
            }
            EggWarsCommand.getSelected().put(s, m);
            Utils.sendMessage(s, "Map.selected", m.getName(), args[1]);
        } else if (args[0].equalsIgnoreCase("team")) {
            if (!selected.containsKey(s)) {
                Utils.sendMessage(s, "Team.selectMap");
                return;
            }
            EggWarsTeam t = EggWarsTeamManager.getTeam(selected.get(s), args[1]);
            if (t == null) {
                Utils.sendMessage(s, "Team.doesntExist", args[1]);
                return;
            }
            EggWarsCommand.getTeams().put(s, t);
            Utils.sendMessage(s, "Team.selected", t.getName(), selected.get(s).getName(), args[1]);
        } else {
            Utils.sendMessage(s, "Error.unknownCommand");
        }
    }
}
