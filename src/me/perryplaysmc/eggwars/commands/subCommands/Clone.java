package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;

public class Clone implements SubCommand {

    public String getName() {
        return "clone";
    }

    public String getInfo()
    {
        return "Clone the selected map";
    }


    public boolean selectMap()
    {
        return true;
    }


    public void execute(CommandSender s, String[] args) {

        if (!Utils.hasPermission(s, "clone")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.clone");
            return;
        }
        if ((!selected.containsKey(s)) || (selected.get(s) == null)) {
            Utils.sendMessage(s, "Team.selectMap");
            return;
        }
        EggWarsMap m = selected.get(s);
        me.perryplaysmc.eggwars.map.MapManager.addMap(m.clone());
        Utils.sendMessage(s, "Map.clone", m.getName());
    }
}
