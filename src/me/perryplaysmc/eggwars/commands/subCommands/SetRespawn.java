package me.perryplaysmc.eggwars.commands.subCommands;

import java.util.HashMap;
import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetRespawn implements me.perryplaysmc.eggwars.utils.SubCommand {

    public String getName() {
        return "setrespawn";
    }

    public String getInfo()
    {
        return "Set the respawn location for the selected team";
    }

    public boolean isPlayer() {
        return true;
    }


    public boolean selectMap()
    {
        return true;
    }

    public boolean selectTeam()
    {
        return true;
    }


    public void execute(CommandSender s, String[] args) {

        if (!Utils.hasPermission(s, "setrespawn")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.setrespawn");
            return;
        }
        if (!(s instanceof Player)) {
            s.sendMessage("You must be a player for this.");
            return;
        }
        if (!teams.containsKey(s)) {
            Utils.sendMessage(s, "Team.selectTeam");
            return;
        }
        EggWarsTeam t = teams.get(s);

        t.setRespawn(((Player)s).getLocation());
        Utils.sendMessage(s, "Team.setrespawn", t.getName(), "§" + t.getColor().getChar());
    }
}
