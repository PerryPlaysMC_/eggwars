package me.perryplaysmc.eggwars.commands.subCommands;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Create implements SubCommand {

    public String getName()
    {
        return "create";
    }

    public String getInfo()
    {
        return "Create a new map";
    }

    public String[] getAliases()
    {
        return new String[] { "c" };
    }

    @Override
    public Integer argLength() {
        return 1;
    }

    public String[] getArguments()
    {
        return new String[] { "<Name>" };
    }

    public void execute(CommandSender s, String[] args) {
        if (!Utils.hasPermission(s, "create")) {
            Utils.sendMessage(s, "Error.invalidPermission", "eggwars.command.create");
            return;
        }
        String name = args[0];
        if ((MapManager.byName(name) != null) && (MapManager.byId(MapManager.byName(name).getId()) != null)) {
            Utils.sendMessage(s, "Map.alreadyExists", MapManager.byName(name).getName(), name);
            return;
        }
        String newName = name;
        if (MapManager.byName(name) != null) {
            newName = name + "-" + (MapManager.byNameCount(name) + 1);
        }
        EggWarsMap m = MapManager.addMap(new EggWarsMap(newName));
        m.setName(name);
        Utils.sendMessage(s, "Map.create", name);
        if (Utils.getConfig().getBoolean("Map-Settings.perWorldMap")) {
            m.getWorld().setSpawnLocation(0, 64, 0);
            for (int x = -4; x < 3; x++) {
                for (int z = -4; z < 3; z++) {
                    Block b = m.getWorld().getBlockAt(x, 63, z);
                    b.setType(Material.END_STONE);
                    b.getState().update(true);
                }
            }
        }
        if ((s instanceof Player)) {
            ((Player)s).teleport(new org.bukkit.Location(m.getWorld(), -1.0D, 66.0D, -1.0D));
        }
        Bukkit.dispatchCommand(s, "eggwars select map " + name);
    }
}
