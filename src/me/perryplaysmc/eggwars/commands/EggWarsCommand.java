package me.perryplaysmc.eggwars.commands;

import com.google.common.collect.Lists;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.MapManager;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.map.team.EggWarsTeamManager;
import me.perryplaysmc.eggwars.utils.Integers;
import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.items.Kits;
import org.bukkit.ChatColor;
import org.bukkit.DyeColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

public class EggWarsCommand implements CommandExecutor, TabCompleter
{
    private CommandSender s;
    private static HashMap<CommandSender, EggWarsMap> selected = new HashMap<>();
    private static HashMap<CommandSender, EggWarsTeam> teams = new HashMap<>();
    private static List<Integers> integers = new ArrayList<>();
    private static List<SubCommand> cmds = new ArrayList<>();

    public static HashMap<CommandSender, EggWarsTeam> getTeams() {
        return teams;
    }


    public static List<SubCommand> getCommands() {
        return cmds;
    }

    public void addSubCommand(SubCommand... commands) {
        for(SubCommand cmd : commands) {
            if(!cmds.contains(cmd))
                cmds.add(cmd);
        }
    }

    public EggWarsCommand() {
        for (Class c : Utils.getClasses(new File("plugins/EggWars.jar"), "me.perryplaysmc.eggwars.commands.subCommands")) {
            try {
                if (c.newInstance() instanceof SubCommand) {
                    SubCommand cmd = (SubCommand)c.newInstance();
                    addSubCommand(cmd);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static HashMap<CommandSender, EggWarsMap> getSelected() {
        return selected;
    }

    public static List<Integers> getGens() {
        return integers;
    }

    public static SubCommand byName(String sub) {
        for(SubCommand cmd : cmds) {
            if (sub.equalsIgnoreCase(cmd.getName())) return cmd;
            if (cmd.getAliases() != null) {
                for (String a : cmd.getAliases()) {
                    if (sub.equalsIgnoreCase(a)) return cmd;
                }
            }
        }
        return null;
    }

    public boolean onCommand(CommandSender s, Command c, String cl, String[] args) {
        if ((!s.hasPermission("eggwars.command")) && (!s.isOp()) && (!s.hasPermission("*"))) {
            s.sendMessage("§cInvalid permission \n§cRequired permission: '§4eggwars.command§c'");
            return true;
        }
        if (args.length > 0) {

            this.s = s;
            if (args.length >= 2) {
                if((args[0].equalsIgnoreCase("createkit")) || (args[0].equalsIgnoreCase("addkit"))) {
                    if(!s.hasPermission("eggwars.command.createkit")) {
                        s.sendMessage("§cInvalid permission \n§cRequired permission: '§4eggwars.command.createkit§c'");
                        return true;
                    }
                    StringBuilder sb = new StringBuilder();
                    for(int i = 1; i < args.length; i++) {
                        sb.append(args[i] + " ");
                    }
                    String kitname = sb.toString().trim();
                    if(Kits.getKit(kitname) != null) {
                        Utils.sendMessage(s, "Kit.exists");
                        return true;
                    }
                    Kits.createKit(kitname, (Player) s);
                    return true;
                }
                if(args[0].equalsIgnoreCase("setlore")) {
                    if(!s.hasPermission("eggwars.command.setlore")) {
                        s.sendMessage("§cInvalid permission \n§cRequired permission: '§4eggwars.command.setlore§c'");
                        return true;
                    }
                    StringBuilder sb = new StringBuilder();
                    for(int i = 1; i < args.length; i++) {
                        sb.append(args[i] + " ");
                    }
                    String kitname = Utils.translate(sb.toString().trim().replace("___", "\n").replace("\n", "\\n"));
                    ItemStack inHand = ((Player) s).getInventory().getItemInMainHand();
                    ItemMeta im = inHand.getItemMeta();
                    im.setLore(Arrays.asList(kitname));
                    inHand.setItemMeta(im);
                    s.sendMessage(Utils.translate("[c]Lore set to: " + kitname));
                    return true;
                }
                if(args[0].equalsIgnoreCase("setname")) {
                    if(!s.hasPermission("eggwars.command.setname")) {
                        s.sendMessage("§cInvalid permission \n§cRequired permission: '§4eggwars.command.setname§c'");
                        return true;
                    }
                    StringBuilder sb = new StringBuilder();
                    for(int i = 1; i < args.length; i++) {
                        sb.append(args[i] + " ");
                    }
                    String kitname = Utils.translate(sb.toString().trim());
                    ItemStack inHand = ((Player) s).getInventory().getItemInMainHand();
                    ItemMeta im = inHand.getItemMeta();
                    im.setDisplayName(kitname);
                    inHand.setItemMeta(im);
                    s.sendMessage(Utils.translate("[c]Name set to: " + kitname));
                    return true;
                }
            }
            SubCommand cmd = byName(args[0]);
            if (cmd != null) {
                if ((!s.hasPermission("eggwars.command." + cmd.permission())) && (!s.isOp()) && (!s.hasPermission("*"))) {
                    s.sendMessage("§cInvalid permission \n§cRequired permission: '§4eggwars.command." + cmd.permission() + "§c'");
                    return true;
                }
                if(cmd.isPlayer()) {
                    if(!(s instanceof Player)) {
                        s.sendMessage("§cYou must be a player for this.");
                        return true;
                    }
                }
                if(cmd.selectMap()) {
                    if(!selected.containsKey(s) || selected.get(s) == null) {
                        Utils.sendMessage(s, "Team.selectMap");
                        return true;
                    }
                }
                if(cmd.selectTeam()) {
                    if (!teams.containsKey(s) || teams.get(s) == null) {
                        Utils.sendMessage(s, "Team.selectTeam");
                        return true;
                    }
                }
                String[] newArgs = Arrays.copyOfRange(args, 1, args.length);
                if (cmd.argsLength() != null) {
                    for (int i = 0; i < cmd.argsLength().length; i++) {
                        int l = cmd.argsLength()[i];
                        if (newArgs.length == l) {
                            cmd.execute(s, newArgs);
                            return true;
                        }
                    }
                }
                if (newArgs.length == cmd.argLength()) {
                    cmd.execute(s, newArgs);
                    return true;
                }
            } else {
                Utils.sendMessage(s, "Error.unknownSubCommand");
            }
        } else {
            Utils.sendMessage(s, "Error.moreArguments");
        }
        return true;
    }

    public List<String> onTabComplete(CommandSender s, Command cd, String cl, String[] args)
    {
        List<String> result = Lists.newArrayList();
        List<String> results1 = Lists.newArrayList();
        List<String> results2 = Lists.newArrayList();
        List<String> results3 = Lists.newArrayList();
        List<String> results4 = Lists.newArrayList();
        List<String> results5 = Arrays.asList("map", "team");
        List<String> results6 = Arrays.asList("diamond", "emerald", "gold", "iron");
        List<String> clear = Arrays.asList("");
        for (ChatColor c : ChatColor.values()) {
            results3.add(c.name().toLowerCase());
        }
        for (DyeColor c : DyeColor.values()) {
            results4.add(c.name().toLowerCase());
        }
        if ((args.length == 3) &&
                (args[0].equalsIgnoreCase("select"))) {
            if (args[1].equalsIgnoreCase("map"))
                for (EggWarsMap m : MapManager.getMaps()) {
                    results2.add(m.getName());
                }
            if ((args[1].equalsIgnoreCase("team")) && (selected.containsKey(s)) &&
                    (EggWarsTeamManager.getTeams(selected.get(s)).size() > 0)) {
                for (EggWarsTeam t : EggWarsTeamManager.getTeams(selected.get(s))) {
                    results2.add(t.getName());
                }
            }
        }

        for (SubCommand cmd : cmds){
            if (((s instanceof Player)) || (!cmd.isPlayer())) {
                if ((Utils.hasPermission(s, cmd.permission())) && (!results1.contains(cmd.getName())))
                    if ((cmd.selectMap()) && (selected.containsKey(s)) && (!cmd.selectTeam())) {
                        results1.add(cmd.getName());
                    } else if ((cmd.selectTeam()) && (teams.containsKey(s)) && (!cmd.selectMap())) {
                        results1.add(cmd.getName());
                    } else if ((cmd.selectTeam()) && (cmd.selectMap()) && (teams.containsKey(s)) && (selected.containsKey(s))) {
                        results1.add(cmd.getName());
                    } else if ((!cmd.selectMap()) && (!cmd.selectTeam()))
                        results1.add(cmd.getName());
            }
        }
        switch (args.length) {
            case 1:
                for (String a : results1){
                    if (a.toLowerCase().startsWith(args[0].toLowerCase())) result.add(a);
                }
                break;
            case 2:
                if (args[0].equalsIgnoreCase("select"))
                    for (String a : results5){
                        if (a.toLowerCase().startsWith(args[1].toLowerCase())) result.add(a); }
                if ((args[0].equalsIgnoreCase("setstate")) && (selected.containsKey(s))) {
                    for (GameState a : GameState.values())
                        if (a.getLocalizedName().toLowerCase().startsWith(args[1].toLowerCase())) result.add(a.getLocalizedName().toLowerCase());
                }
                if ((args[0].equalsIgnoreCase("generator")) || (args[0].equalsIgnoreCase("gen")))
                    for (String a : results6) {
                        if (a.toLowerCase().startsWith(args[1].toLowerCase())) result.add(a); }
                break;


            case 3:
                if (args[0].equalsIgnoreCase("select")) {
                    if (args[1].equalsIgnoreCase("team")) {
                        for (String a :results2) {
                            if (a.toLowerCase().startsWith(args[2].toLowerCase())) result.add(a);
                        }
                        break;
                    }
                    if (args[1].equalsIgnoreCase("map")) {
                        for (String a :results2) {
                            if (a.toLowerCase().startsWith(args[2].toLowerCase())) result.add(a);
                        }
                        break;
                    }
                }
                if (args[0].equalsIgnoreCase("addteam"))
                    for (String a :results3) {
                        if ((args[0].equalsIgnoreCase("createkit")) || (args[0].equalsIgnoreCase("addkit"))) {
                            break;
                        }
                        if (a.toLowerCase().startsWith(args[2].toLowerCase())) result.add(a);
                    }
                break;


            case 4:
                if (args[0].equalsIgnoreCase("addteam")) {
                    for (String a :results4) {
                        if (a.toLowerCase().startsWith(args[2].toLowerCase())) result.add(a);
                    }
                }
                break;
        }
        if (args.length > 4) {
            for (String a :clear) {
                if (a.toLowerCase().startsWith(args[(args.length + -1)].toLowerCase())) result.add(a);
            }
            return result;
        }
        return result;
    }
}
