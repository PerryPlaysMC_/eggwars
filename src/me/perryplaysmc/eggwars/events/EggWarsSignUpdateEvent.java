package me.perryplaysmc.eggwars.events;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.block.Sign;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class EggWarsSignUpdateEvent
  extends Event implements Cancellable
{
  private static HandlerList handlerList = new HandlerList();
  private Sign sign;
  private EggWarsMap game;
  private String[] lines;
  private boolean cancel = false;
  
  public EggWarsSignUpdateEvent(Sign sign, EggWarsMap game, String[] lines)
  {
    this.sign = sign;
    this.game = game;
    this.lines = lines;
  }
  
  public EggWarsMap getGame() {
    return game;
  }
  
  public Sign getSign() {
    return sign;
  }
  
  public String[] getLines()
  {
    return lines;
  }
  
  public String getLine(int i) {
    return lines[i];
  }
  
  public void setLine(int i, String newText) {
    lines[i] = Utils.translate(newText);
  }
  
  public void setLines(String[] lines) {
    this.lines = lines;
  }
  

  public boolean isCancelled()
  {
    return cancel;
  }
  
  public void setCancelled(boolean b)
  {
    cancel = b;
  }
  
  public HandlerList getHandlers()
  {
    return handlerList;
  }
  
  public static HandlerList getHandlerList() {
    return handlerList;
  }
}
