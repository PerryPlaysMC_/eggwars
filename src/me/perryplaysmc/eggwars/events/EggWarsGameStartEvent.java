package me.perryplaysmc.eggwars.events;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class EggWarsGameStartEvent
  extends Event implements Cancellable
{
  private static HandlerList handlerList = new HandlerList();
  private EggWarsMap game;
  private boolean cancel = false;
  
  public EggWarsGameStartEvent(EggWarsMap game)
  {
    this.game = game;
  }
  
  public EggWarsMap getGame() {
    return game;
  }
  

  public boolean isCancelled()
  {
    return cancel;
  }
  
  public void setCancelled(boolean b)
  {
    cancel = b;
  }
  
  public HandlerList getHandlers()
  {
    return handlerList;
  }
  
  public static HandlerList getHandlerList() {
    return handlerList;
  }
}
