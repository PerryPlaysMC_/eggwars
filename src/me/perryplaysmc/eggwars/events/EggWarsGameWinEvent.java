package me.perryplaysmc.eggwars.events;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class EggWarsGameWinEvent extends Event
{
  private static HandlerList handlerList = new HandlerList();
  private EggWarsMap game;
  private EggWarsTeam team;
  
  public EggWarsGameWinEvent(EggWarsTeam team, EggWarsMap game)
  {
    this.game = game;
    this.team = team;
  }
  
  public EggWarsTeam getTeam()
  {
    return team;
  }
  
  public EggWarsMap getGame() {
    return game;
  }
  

  public HandlerList getHandlers()
  {
    return handlerList;
  }
  
  public static HandlerList getHandlerList() {
    return handlerList;
  }
}
