package me.perryplaysmc.eggwars.events;

import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.Utils;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

public class EggWarsPlayerJoinTeamEvent extends PlayerEvent
{
  private static HandlerList handlerList = new HandlerList();
  private String message;
  private EggWarsTeam team;
  private EggWarsMap map;
  
  public EggWarsPlayerJoinTeamEvent(Player who, String message, EggWarsTeam team, EggWarsMap map)
  {
    super(who);
    this.message = message;
    this.team = team;
    this.map = map;
  }
  
  public EggWarsTeam getTeam() {
    return team;
  }
  
  public EggWarsMap getMap() {
    return map;
  }
  
  public void setMessage(String message) {
    this.message = Utils.translate(message);
  }
  
  public String getMessage() {
    return message;
  }
  

  public HandlerList getHandlers()
  {
    return handlerList;
  }
  
  public static HandlerList getHandlerList() {
    return handlerList;
  }
}
