package me.perryplaysmc.eggwars.map.team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import org.bukkit.entity.Player;


public class EggWarsTeamManager {
    private static HashMap<EggWarsMap, List<EggWarsTeam>> teams = new HashMap<>();

    public static void registerTeam(EggWarsMap map, EggWarsTeam team) { List<EggWarsTeam> teamlist = teams.get(map);
        if (teamlist != null) {
            teamlist.add(team);
        } else {
            teamlist = new ArrayList<>();
            teamlist.add(team);
        }
        map.addTeam(team);
        teams.put(map, teamlist);
    }

    public static List<EggWarsTeam> getTeams(EggWarsMap m) {
        if (!teams.containsKey(m)) return new ArrayList<>();
        return teams.get(m);
    }

    public static EggWarsTeam getTeam(EggWarsMap map, String name) {
        if (!teams.containsKey(map)) return null;
        for (EggWarsTeam team : teams.get(map)) {
            if (team.getName().equalsIgnoreCase(name)) return team;
        }
        return null;
    }

    public static EggWarsTeam getTeam(EggWarsMap map, Player p) {
        if (!teams.containsKey(map)) return null;
        for (EggWarsTeam team : teams.get(map)) {
            if (team.getPlayers().contains(p)) return team;
        }
        return null;
    }

    public static boolean mapHasTeam(EggWarsMap map, String name) {
        return getTeam(map, name) != null;
    }
}
