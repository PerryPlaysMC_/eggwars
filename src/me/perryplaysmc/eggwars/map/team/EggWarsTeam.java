package me.perryplaysmc.eggwars.map.team;

import java.util.ArrayList;
import java.util.List;
import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.events.EggWarsPlayerJoinTeamEvent;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.config.MapConfig;
import me.perryplaysmc.eggwars.utils.items.Kits;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;

public class EggWarsTeam {
    private String name;
    private List<Player> players;
    private List<Player> dead;
    private EggWars ew = EggWars.getInstance();

    private List<Location> spawns;
    private EggWarsMap map;
    private MapConfig cfg;
    private Team t;

    public EggWarsTeam(EggWarsMap map, String name, ChatColor color, DyeColor DyeColor)
    {
        this.name = name;
        players = new ArrayList<>();
        dead = new ArrayList<>();
        this.map = map;
        cfg = map.getConfig();
        spawns = new ArrayList<>();
        if (!cfg.isSet("Teams." + name + ".Locations.Egg"))
            cfg.set("Teams." + name + ".Locations.Egg", "");
        if ((!cfg.isSet("Teams." + name + ".Color")) && (color != null))
            cfg.set("Teams." + name + ".Color", color.name());
        if (!cfg.isSet("Teams." + name + ".DyeColor"))
            cfg.set("Teams." + name + ".DyeColor", DyeColor.name());
        if (!cfg.isSet("Teams." + name + ".Locations.Respawn"))
            cfg.set("Teams." + name + ".Locations.Respawn", "");
        EggWarsTeamManager.registerTeam(map, this);
    }

    public DyeColor getDyeColor()
    {
        return Utils.byNameDye(cfg.isSet("Teams." + name + ".DyeColor") ? cfg.getString("Teams." + name + ".DyeColor") : null);
    }

    public ChatColor getColor()
    {
        return Utils.byNameChat(cfg.isSet("Teams." + name + ".Color") ? cfg.getString("Teams." + name + ".Color") : null);
    }

    public boolean addSpawn(Location loc)
    {
        if (spawns.size() == map.getMaxPlayersPerTeam()) return false;
        spawns.add(loc);
        setSpawns(spawns);
        return true;
    }

    public void setSpawns(List<Location> locs) {
        spawns = locs;
        cfg.set("Teams." + name + ".Locations.Spawns", Utils.serializeLoc(locs));
    }

    public List<Location> getSpawns() {
        List<Location> spawns = new ArrayList<>();
        if (cfg.isSet("Teams." + name + ".Locations.Spawns")) {
            spawns = Utils.deserializeLoc(cfg.getStringList("Teams." + name + ".Locations.Spawns"));
        }
        return spawns;
    }

    public Location getRespawn() {
        return cfg.isSet("Teams." + name + ".Locations.Respawn") ? Utils.deserializeLoc(cfg.getString("Teams." + name + ".Locations.Respawn")) : null;
    }

    public void broadcast(Object... messages) {
        for (Player p : getPlayers())
            for (Object obj : messages)
                p.sendMessage(Utils.translate(obj.toString()));
    }

    public Scoreboard registerTeam(Scoreboard b) {
        Scoreboard sb = b;
        if (sb.getTeam(name + "-" + getColor().name()) == null) {
            t = sb.registerNewTeam(name + "-" + getColor().name());
        } else {
            t = sb.getTeam(name + "-" + getColor().name());
        }
        t.setColor(getColor());
        t.setPrefix(getColor() + getName().toUpperCase() + " " + getColor() + "" + ChatColor.BOLD);
        t.setAllowFriendlyFire(false);
        t.setOption(Team.Option.DEATH_MESSAGE_VISIBILITY, org.bukkit.scoreboard.Team.OptionStatus.NEVER);
        t.setOption(Team.Option.COLLISION_RULE, org.bukkit.scoreboard.Team.OptionStatus.FOR_OWN_TEAM);
        for (Player p : getPlayers()) {
            if (!t.getEntries().contains(p.getName())) {
                t.addEntry(p.getName());
            }
        }
        return sb;
    }

    public void addDead(Player p) {
        if (dead.contains(p)) {
            return;
        }
        dead.add(p);
        p.setGameMode(GameMode.SPECTATOR);
        for (Player p2 : map.getPlayers()) {
            if (p2 != p)
                p2.hidePlayer(p);
        }
        Player killer = p.getKiller();
        List<String> death = java.util.Arrays.asList("[color]" + p.getName() + "§c Has been eliminated", "[color]" + p
                .getName() + "§c Has been Killed to death");




        String d = death.get(new java.util.Random().nextInt(death.size())).replace("[color]", getColor() + "").replace("[colorT]", (killer == null) || (map.getTeam(p) != null) ? map.getTeam(p).getColor() + "" : "");
        map.broadCast(d);
    }

    public List<Player> getDead() {
        return dead;
    }

    public String getName() {
        return name;
    }

    public boolean hasEgg()
    {
        if (getEgg() != null) {
            return (getEgg().getBlock().getType() == org.bukkit.Material.DRAGON_EGG) && (getEgg().getBlock().getType() != org.bukkit.Material.AIR) && (getEgg().getBlock().getType() != null);
        }
        return false;
    }

    public String formatHasEgg() {
        return hasEgg() ? "Yes" : "No";
    }

    public String eggisLost() {
        return formatHasEgg() == "Yes" ? "No" : "Yes";
    }

    public Location getEgg() {
        return cfg.isSet("Teams." + name + ".Locations.Egg") ? Utils.deserializeLoc(cfg.getString("Teams." + name + ".Locations.Egg")) : null;
    }

    public void setEggLocation(Location eggLocation) {
        cfg.set("Teams." + name + ".Locations.Egg", Utils.serializeLoc(eggLocation));
    }

    public MapConfig getConfig()
    {
        return cfg;
    }

    public EggWarsMap getMap()
    {
        return map;
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public void joinTeam(Player p) {
        if (getPlayers().contains(p)) {
            Utils.sendMessage(p, "Team.already-in-team");
            return;
        }
        if (getPlayers().size() == getMap().getMaxPlayersPerTeam()) {
            Utils.sendMessage(p, "Team.team-full");
            return;
        }
        getPlayers().add(p);

        String res = Utils.translate("Messages.Team.team-join", getName(), getColor());
        EggWarsPlayerJoinTeamEvent e = new EggWarsPlayerJoinTeamEvent(p, res, this, map);
        org.bukkit.Bukkit.getPluginManager().callEvent(e);
        res = e.getMessage();
        p.sendMessage(res);
    }

    public void setRespawn(Location location) {
        cfg.set("Teams." + name + ".Locations.Respawn", Utils.serializeLoc(location));
    }

    public void respawnPlayer(Player p) {
        respawnPlayer(p, null, false);
    }
    public void respawnPlayer(Player p, Player killer, boolean isVoidDeath) {
        p.teleport(map.getRespawnLoc());
        if(killer != null && isVoidDeath) {
            for(int i = 0; i < p.getInventory().getContents().length; i++) {
                ItemStack c = p.getInventory().getItem(i);
                if(c != null && c.getType() != Material.AIR) {
                    if(c.getType() == Material.DIAMOND || c.getType() == Material.EMERALD || c.getType() == Material.IRON_INGOT || c.getType() == Material.GOLD_INGOT) {
                        killer.getLocation().getWorld().dropItem(killer.getLocation(), c);
                    }
                }
            }
        }else {
            for(int i = 0; i < p.getInventory().getContents().length; i++) {
                ItemStack c = p.getInventory().getItem(i);
                if(c != null && c.getType() != Material.AIR)
                    if(c.getType() == Material.DIAMOND || c.getType() == Material.EMERALD || c.getType() == Material.IRON_INGOT || c.getType() == Material.GOLD_INGOT)
                        p.getLocation().getWorld().dropItem(p.getLocation(), c);
            }
        }

        p.getInventory().clear();
        p.setGameMode(GameMode.SPECTATOR);
        if (!hasEgg()) {
            addDead(p);
            if (getDead().size() == getPlayers().size()) {
                getMap().broadCast(getColor() + getName() + " Team§c Has been eliminated");
                map.getDeadTeams().add(this);
            }
            return;
        }
        (new BukkitRunnable() {
            int i = 5;
            public void run() {
                if ((i <= 0) || (map.getState() != GameState.RUNNING)) {
                    p.teleport(getRespawn());
                    p.setGameMode(GameMode.SURVIVAL);
                    p.setHealth(p.getMaxHealth());
                    if (map.kits().containsKey(p))
                        Kits.giveKit(p, map.kits().get(p).getName());
                    cancel();
                    return;
                }
                p.sendTitle("§6Respawning in§e " + i + ".", "", 5, 15, 5);
                i--;
            }
        }).runTaskTimer(EggWars.getInstance(), 10L, 20L);
    }
}
