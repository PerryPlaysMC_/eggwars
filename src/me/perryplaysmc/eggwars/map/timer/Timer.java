package me.perryplaysmc.eggwars.map.timer;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.EggWarsAPI;
import me.perryplaysmc.eggwars.events.EggWarsGameStartEvent;
import me.perryplaysmc.eggwars.events.EggWarsGameWinEvent;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.map.GameState;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.config.Config;
import me.perryplaysmc.eggwars.utils.config.MapConfig;
import me.perryplaysmc.eggwars.utils.items.Kits;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import static me.perryplaysmc.eggwars.EggWarsAPI.getFormattedTime;

@SuppressWarnings("all")
public class Timer extends BukkitRunnable {
    private int lobby, mpt, r, i = 0, st, end, current;
    private EggWarsMap map;
    private MapConfig cfg;
    private EggWars ew = EggWars.getInstance();
    private boolean paused = true;

    public Timer(EggWarsMap map) {
        this.map = map;
        cfg = map.getConfig();
        resetTimings();
        paused = true;
        runTaskTimer(ew, 0L, 20L);
    }

    private Config s = Utils.getConfig();

    public EggWarsTeam getSmallestTeam() { if (map.getTeams() == null) {
        return null;
    }
        EggWarsTeam t = map.getTeams().get(0);
        for (EggWarsTeam team : map.getTeams()) {
            if (team.getPlayers().size() < t.getPlayers().size()) {
                t = team;
            }
        }
        return t;
    }

    public void balance(Player p) {
        if (map.getTeams() == null) {
            return;
        }
        EggWarsTeam ta = null;
        for (EggWarsTeam team : map.getTeams()) {
            ta = team;
        }
        EggWarsTeam a = map.getTeam(p);
        if ((a == null) && (ta != null) && (!ta.getPlayers().contains(p))) {
            EggWarsTeam t = getSmallestTeam();
            t.joinTeam(p);
        }
    }


    public void run() {
        if (!paused) {
            switch (map.getState()) {
                case LOBBY:
                    if (lobby > 0) {
                        lobby -= 1;
                    }
                    for (String a : s.getStringList("Timer.lobby")) {
                        if ((a.startsWith("<")) && (lobby == Integer.parseInt(a.split("<")[1].split(">")[0])))
                            map.broadCast(Utils.translate(a.replaceFirst("<" + Integer.parseInt(a.split("<")[1].split(">")[0]) + ">", "")));
                    }
                    map.updateScoreboard();
                    if (lobby <= 0) {
                        List<EggWarsTeam> teams = new ArrayList<>();
                        for(EggWarsTeam t : map.getTeams()) {
                            if (teams.contains(t) || t.getPlayers().size() <= 0)
                                teams.add(t);
                        }
                        if (teams.size() < 2 && map.getTeams().size() >= 2) {
                            for (EggWarsTeam t : teams) {
                                t.getPlayers().clear();
                            }
                        }
                        for (Player p : map.getPlayers()) {
                            balance(p);
                            map.update(p);
                            p.setHealth(p.getMaxHealth());
                            p.setFoodLevel(20);
                            p.setSaturation(20);
                        }
                        for (EggWarsTeam t : map.getTeams()) {
                            List<Location> spawns = new ArrayList<>();
                            spawns.addAll(t.getSpawns());
                            for (Player p : t.getPlayers()) {
                                if (t.getSpawns().size() > 0) {
                                    int i = new Random().nextInt(spawns.size());
                                    p.teleport(spawns.get(i));
                                    spawns.remove(i);
                                }
                            }
                            if ((t.getEgg() != null) && (t.getEgg().getBlock().getType() != Material.DRAGON_EGG)) {
                                t.getEgg().getBlock().setType(Material.DRAGON_EGG);
                                t.getEgg().getBlock().getState().update(true);
                            }
                        }
                        map.setState(GameState.PRE_START);
                        ew.addGenerators(map);
                    }
                    break;

                case PRE_START:
                    if (r != 0) {
                        r -= 1;
                    }
                    for (String a : s.getStringList("Timer.prestart")) {
                        if ((a.startsWith("<")) && (r == Integer.parseInt(a.split("<")[1].split(">")[0]))) {
                            map.broadCast(Utils.translate(a.replaceFirst("<" + Integer.parseInt(a.split("<")[1].split(">")[0]) + ">", "")));
                        }
                    }
                    map.updateScoreboard();
                    if (r == 0) {
                        map.setState(GameState.RUNNING);
                        for (Player p : map.getPlayers()) {
                            map.update(p);
                            p.getInventory().clear();
                        }
                        map.updateScoreboard();

                        for (Player p : map.getPlayers()) {
                            p.setGameMode(GameMode.SURVIVAL);
                            if(map.kits().get(p) != null)
                                Kits.giveKit(p, (map.kits().get(p)).getName());
                        }
                    }
                    break;
                case RUNNING:
                    current += 1;
                    this.i += 1;
                    map.updateScoreboard();
                    for (String a : s.getStringList("Timer.ingame")) {
                        if ((a.startsWith("<")) && (this.i == Integer.parseInt(a.split("<")[1].split(">")[0])))
                            map.broadCast(Utils.translate(a.replaceFirst("<" + Integer.parseInt(a.split("<")[1].split(">")[0]) + ">", "")));
                    }
                    for(Player player : map.getPlayers()) {
                        int i = EggWarsAPI.getTimePlayedSeconds(player)+1;
                        EggWarsAPI.getDataConfig().set("Data." + player.getUniqueId() + ".timePlayed", i);
                    }
                    if (!map.isTesting() &&
                            ((map.getPlayers().size() == 0)
                                    || (map.getPlayers().size() == 1)
                                    || (map.getDeadTeams().size() == map.getTeams().size() + -1)
                                    || (map.activeTeams().size() == 1))) {
                        map.setState(GameState.END);
                        map.updateScoreboard();
                        for(EggWarsTeam t : map.activeTeams()) {
                            for(Player p : t.getPlayers()) Utils.addWin(p);
                        }
                        EggWarsGameWinEvent e = new EggWarsGameWinEvent(map.activeTeams().get(0), map);
                        Bukkit.getPluginManager().callEvent(e);
                        for(Player p : map.getPlayers()) {
                            map.update(p);
                            EggWarsAPI.getDataConfig().set("Data." + p.getUniqueId() + ".gamesPlayed", (EggWarsAPI.getGamesPlayed(p)+1));
                        }
                    }
                    break;
                case END:
                    if (end == map.getEnd()) {
                        for (Generator g : map.getGenerators()) {
                            g.stop();
                        }
                    }
                    if (end != 0) {
                        end -= 1;
                    }
                    ew.removeGenerator(map);
                    map.updateScoreboard();

                    for (String a : s.getStringList("Timer.endGame")) {
                        if ((a.startsWith("<")) && (end == Integer.parseInt(a.split("<")[1].split(">")[0]))) {
                            map.broadCast(Utils.translate(a.replaceFirst("<" + Integer.parseInt(a.split("<")[1].split(">")[0]) + ">", "")));
                        }
                    }
                    if (end <= 0) {
                        EggWarsMap m = map;
                        map.updateScoreboard();
                        m.endGame();
                    }
                    break;
            }
        }
    }

    public boolean isPaused()
    {
        return paused;
    }

    public void setPaused(boolean paused) {
        EggWarsGameStartEvent e = new EggWarsGameStartEvent(map);
        org.bukkit.Bukkit.getPluginManager().callEvent(e);
        if (e.isCancelled()) return;
        this.paused = paused;
    }

    public void resetTimings() {
        lobby = map.getLobby();
        mpt = map.getMaxPlayerTimer();
        r = map.getStarting();
        end = map.getEnd();
    }

    public int getHours()
    {switch (map.getState()) {
        case LOBBY:
            return lobby / 60 * 60;
        case PRE_START:
            return r / 60 * 60;
        case RUNNING:
            return current / 60 * 60;
        case END:
            return end / 60 * 60;
    }
        return 0;
    }

    public int getMins() {
        switch (map.getState()) {
            case LOBBY:
                return lobby / 60;
            case PRE_START:
                return r / 60;
            case RUNNING:
                return current / 60;
            case END:
                return end / 60;
        }
        return 0;
    }

    public int getSecs() {
        switch (map.getState()) {
            case LOBBY:
                return lobby;
            case PRE_START:
                return r;
            case RUNNING:
                return current;
            case END:
                return end;
        }
        return 0;
    }

    public String format() {
        int i = getSecs();
        if (r > -1) {
            return getFormattedTime(i);
        }
        return getFormattedTime(i);
    }
}
