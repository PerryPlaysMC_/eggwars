package me.perryplaysmc.eggwars.map.timer;

public enum GeneratorType
{
  EMERALD,  DIAMOND,  GOLD,  IRON;
  
  private GeneratorType() {}
}
