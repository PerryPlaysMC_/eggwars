package me.perryplaysmc.eggwars.map.timer;

import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.map.EggWarsMap;
import me.perryplaysmc.eggwars.utils.InstantFireWork;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.inventory.Inventory;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("all")
public class Generator {
    private GeneratorType type;
    private int genId;
    private int max;
    private int id;
    private int level;
    private int start;
    private int dropSize = 1;
    private double stack;
    private double speed;
    private double resetSpeed;
    public  double run = 0.0D;
    private Item e;
    private Sign s;
    private EggWarsMap map;
    private BukkitTask task;
    private ItemStack upgrade;
    private ItemStack drop;
    private boolean pause;
    private boolean stop;
    private boolean running;
    private boolean isLocked = false;

    public Generator(EggWarsMap map, Sign s, int start, int max, GeneratorType type) {
        this.map = map;
        this.s = s;
        this.setLevel(start);
        this.max = max;
        this.type = type;
        this.start = start;
        updateLevel();
        resetSpeed = speed;
        genId = (map.getGenerators().size() + 1);
        stop = false;
        findDropType();
        updateSign(true);
    }
    

    
    void setUpgrade(ConfigurationSection s) {
        if(s.getString("UpgradeItem.Type").equalsIgnoreCase("gold")) {
            upgrade = new ItemStack(Material.GOLD_INGOT, s.getInt("UpgradeItem.Amount"));
            ItemMeta im = upgrade.getItemMeta();
            im.setDisplayName("§6Golden Token");
            upgrade.setItemMeta(im);
        } else if(s.getString("UpgradeItem.Type").equalsIgnoreCase("diamond")) {
            upgrade = new ItemStack(Material.DIAMOND, s.getInt("UpgradeItem.Amount"));
            ItemMeta im = upgrade.getItemMeta();
            im.setDisplayName("§bDiamond Token");
            upgrade.setItemMeta(im);
        } else if(s.getString("UpgradeItem.Type").equalsIgnoreCase("iron")) {
            upgrade = new ItemStack(Material.IRON_INGOT, s.getInt("UpgradeItem.Amount"));
            ItemMeta im = upgrade.getItemMeta();
            im.setDisplayName("§7Iron Token");
            upgrade.setItemMeta(im);
        } else if(s.getString("UpgradeItem.Type").equalsIgnoreCase("emerald")) {
            upgrade = new ItemStack(Material.EMERALD, s.getInt("UpgradeItem.Amount"));
            ItemMeta im = upgrade.getItemMeta();
            im.setDisplayName("§aEmerald Token");
            upgrade.setItemMeta(im);
        }
    }

    public void updateLevel() {
        int x = EggWars.getInstance().getConfiguration().getSection("Generators." + type.name().toLowerCase()).getKeys(false).size();
        if(max > x) max = x;
        if(start > max) {
            start = max;
        }
        speed = -1;
        stack = 0;
        dropSize = 0;
        if(level == 0) {
            ConfigurationSection s = EggWars.getInstance().getConfiguration().getSection("Generators." + type.name().toLowerCase() + ".fix");
            setUpgrade(s);
        }
        for (String a : EggWars.getInstance().getConfiguration().getSection("Generators." + type.name().toLowerCase()).getKeys(false)) {
            try {
                Integer.parseInt(a);
            }catch (Exception e) {
                if(!a.equalsIgnoreCase("fix"))
                    System.out.println("Error invalid upgrade level '" + a + "'");
                continue;
            }
            ConfigurationSection s = EggWars.getInstance().getConfiguration().getSection("Generators." + type.name().toLowerCase() + "." + a);
            if(level == Integer.parseInt(a)) {
                if(level < max) {
                    setUpgrade(s);
                }
                speed = s.getDouble("onUpgrade.Speed");
                stack = s.getDouble("onUpgrade.MaxAmountOnGenerator");
                dropSize = s.getInt("onUpgrade.DropsPerSpeedInterval");
                break;
            }
        }

        resetSpeed = speed;
        run = speed;
    }

    public ItemStack getUpgrade() {
        return upgrade;
    }

    public void upgrade() {
        if(getLevel() == max) return;
        setLevel(getLevel() + 1);
        updateLevel();
        updateSign(true);
        Location a = new Location(getSign().getBlock().getWorld(), getSign().getBlock().getX(), getSign().getBlock().getY(), getSign().getBlock().getZ());
        Color c = type == GeneratorType.GOLD ? Color.ORANGE : type == GeneratorType.EMERALD ? Color.LIME : type == GeneratorType.DIAMOND ? Color.AQUA : Color.GRAY;
        FireworkEffect effect = FireworkEffect.builder().withColor(c).flicker(true).trail(false).with(FireworkEffect.Type.BALL_LARGE).build();
        updateSign(true);
        new BukkitRunnable() {
            public void run() {
                new InstantFireWork(effect, a);
            }
        }.runTaskLater(EggWars.getInstance(), 0L);
    }

    public String getColor() {
        switch (getType()) {
            case DIAMOND:
                return "§b";
            case EMERALD:
                return "§a";
            case GOLD:
                return "§6";
            case IRON:
                return "§7";
        }
        return null;
    }

    public void stop() {
        stop = true;
        pause = true;
    }

    public int getDropSize() {
        return dropSize;
    }

    void findDropType() {
        switch (type) {
            case DIAMOND:
                drop = new ItemStack(Material.getMaterial(type.name()), 1);
                ItemMeta im = drop.getItemMeta();
                drop.setType(Material.getMaterial(type.name()));
                im.setDisplayName(getColor() + "Diamond Token");
                drop.setItemMeta(im);
                break;
            case EMERALD:
                drop = new ItemStack(Material.EMERALD, 1);
                im = drop.getItemMeta();
                drop.setType(Material.EMERALD);
                im.setDisplayName(getColor() + "Emerald Token");
                drop.setItemMeta(im);
                break;
            case GOLD:
                drop = new ItemStack(Material.getMaterial(type.name() + "_INGOT"), 1);
                im = drop.getItemMeta();
                im.setDisplayName(getColor() + "Golden Token");
                drop.setItemMeta(im);
                break;
            case IRON:
                drop = new ItemStack(Material.getMaterial(type.name() + "_INGOT"), 1);
                im = drop.getItemMeta();
                im.setDisplayName(getColor() + "Iron Token");
                drop.setItemMeta(im);
        }

        drop.setAmount(dropSize > 0 ? dropSize : 1);
    }

    public boolean isRunning() {
        return task != null;
    }

    public void run() {
        pause = (map.getState() == me.perryplaysmc.eggwars.map.GameState.END) || (map.getState() == me.perryplaysmc.eggwars.map.GameState.LOBBY);
        if((s != null) &&
                (!pause)) {
            if(speed >= 1000000.0D || speed <=-1) return;
            updateSign(false);
            if(run > 0.0D) {
                run += -0.1D;
                return;
            }
            run = resetSpeed;
            drop(drop);
        }
    }

    public void updateSign(boolean force) {
        Generator gen = this;
        DecimalFormat a = new DecimalFormat("##0.0#");
        String run = a.format(this.run).replace("-", "");
        String speed = getSpeed() + "";
        if(getSpeed() >= 1000.0D) {
            run = "§cBroken";
            speed = "§cBroken";
        }
        String name = (""+gen.getType().name().charAt(0)).toUpperCase() + gen.getType().name().substring(1).toLowerCase();
        String line1 = Utils.translate("Map-Settings.Generator.update.Line-1", name, gen.getLevel(), gen.getMax(), speed, gen.getColor(), run);
        String line2 = Utils.translate("Map-Settings.Generator.update.Line-2", name, gen.getLevel(), gen.getMax(), speed, gen.getColor(), run);
        String line3 = Utils.translate("Map-Settings.Generator.update.Line-3", name, gen.getLevel(), gen.getMax(), speed, gen.getColor(), run);
        String line4 = Utils.translate("Map-Settings.Generator.update.Line-4", name, gen.getLevel(), gen.getMax(), speed, gen.getColor(), run);
        if(force) {
            s.setLine(0, line1);
            s.setLine(1, line2);
            s.setLine(2, line3);
            s.setLine(3, line4);
            s.update(true);
            return;
        }
        if((run != "§cBroken") && (
                (line1.contains("{5}")) ||
                        (line2.contains("{5}")) ||
                        (line3.contains("{5}")) ||
                        (line4.contains("{5}")))) {
            s.update(true);
        }
    }

    void drop(ItemStack drop) {
        if(getNearbyItems(s.getLocation().clone().add(0.5D, 0, 0.5D), 0.5D).size() == 0) {
            e = s.getWorld().dropItem(s.getLocation().clone().add(0.5D, 0.2D, 0.5D), drop);
            e.setVelocity(new Vector());
            e.setPickupDelay(15);
            return;
        }
        for (Entity et : getNearbyItems(s.getLocation().clone().add(0.5D, 0, 0.5D), 0.5D)) {
            if((et instanceof Item)) {
                Item i = (Item)et;
                if((i.getItemStack().getAmount() == 64) || (!i.getItemStack().isSimilar(drop)))  {
                    continue;
                }
                if((i.getItemStack().getAmount() < stack) && (i.getItemStack().isSimilar(drop))) {
                    e = s.getWorld().dropItem(s.getLocation().clone().add(0.5D, 0.2D, 0.5D), drop);
                    e.setVelocity(new Vector());
                    e.setPickupDelay(10);
                    break;
                }
            }
        }
    }

    public List<Entity> getNearbyItems(Location l, double radius) {
        List<Entity> entities = new ArrayList<>();
        for (Entity e : l.getWorld().getNearbyEntities(l, radius, radius, radius)) {
            if((e instanceof Item)) {
                entities.add(e);
            }
        }
        return entities;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMax() {
        return max;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }

    public int getStart() {
        return start;
    }

    public int getId() {
        return genId;
    }

    public double getStack() {
        return stack;
    }

    public double getSpeed() {
        return speed;
    }

    public EggWarsMap getMap()
    {
        return map;
    }

    public Sign getSign() {
        return s;
    }

    public GeneratorType getType()
    {
        return type;
    }

    public Inventory getInventory() {
        Inventory inv = new Inventory(27, getColor() + type.name().charAt(0) + type.name().substring(1).toLowerCase() + " Generator");
        int m = level == 0 ? 1 : level;
        ItemStack i2 = new ItemStack(type == GeneratorType.GOLD ? Material.GOLD_INGOT :
                type == GeneratorType.EMERALD ? Material.EMERALD : type == GeneratorType.DIAMOND ? Material.DIAMOND : Material.IRON_INGOT, m);

        ItemMeta im2 = i2.getItemMeta();
        DecimalFormat a = new DecimalFormat("##0.0#");
        String run = a.format(getSpeed()).replace("-", "");
        if(getSpeed() == 1.0E7D || getSpeed() < 0) {
            run = "§cBroken";
        }
        Generator gen = this;
        im2.setDisplayName(Utils.translate("Map-Settings.Generator.Items.currentInfoItem.Name", gen.getLevel(), gen.getDropSize(), gen.getColor(), run));
        List<String> lore = new ArrayList<>();
        for (String a2 : Utils.getConfig().getStringList("Map-Settings.Generator.Items.currentInfoItem.Lore")) {
            lore.add(Utils.translate2(a2, gen.getLevel(), gen.getDropSize(), gen.getColor(), run));
        }
        im2.setLore(lore);
        i2.setItemMeta(im2);
        inv.setItem(11, i2);
        updateLevel();
        ItemStack i = new ItemStack(Material.EXPERIENCE_BOTTLE, getLevel() + 1);

        ItemMeta im = i.getItemMeta();
        if(getLevel() == max) {
            i.setAmount(max);
            im.setDisplayName(Utils.getString("Map-Settings.Generator.Items.upgradeItem.MaxedName"));
        } else {
            ItemStack upgrade = this.upgrade;
            setLevel(getLevel() + 1);
            updateLevel();
            a = new DecimalFormat("##0.0#");
            run = a.format(getSpeed()).replace("-", "");
            if(getSpeed() == 1.0E7D || getSpeed() < 0) {
                run = "0";
            }
            im.setDisplayName(Utils.translate("Map-Settings.Generator.Items.upgradeItem.Name", gen.getLevel() + -1, gen.getLevel(), gen.getDropSize(), gen.getColor(),
                    run, upgrade.getItemMeta().getDisplayName(), upgrade.getAmount()));
            List<String> lore2 = new java.util.ArrayList<>();
            for (String a2 : Utils.getConfig().getStringList("Map-Settings.Generator.Items.upgradeItem.Lore")) {
                lore2.add(Utils.translate2(a2, gen.getLevel() + -1, gen.getLevel(), gen.getDropSize(), gen.getColor(), run, upgrade.getItemMeta().getDisplayName(), upgrade.getAmount()));
            }
            im.setLore(lore2);
            setLevel(getLevel() + -1);
            updateLevel();
        }
        updateLevel();
        i.setItemMeta(im);
        inv.setItem(15, i);
        return inv;
    }
}
