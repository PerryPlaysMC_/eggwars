package me.perryplaysmc.eggwars.map;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import me.perryplaysmc.eggwars.EggWars;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.libs.org.apache.commons.io.FileUtils;

public class WorldManager {

    public static boolean duplicateWorld(String worldName, String dest) {
        if (new File("." + File.separator + dest).exists()) {
            EggWars.getInstance().getServer().getConsoleSender().sendMessage(Level.INFO.getLocalizedName() + " " + dest + " already exists!");
            return false;
        }
        try {
            FileUtils.copyDirectory(new File("." + File.separator + worldName), new File("." + File.separator + dest));
            FileUtils.forceDelete(new File("." + File.separator + dest + File.separator + "uid.dat"));
            EggWars.getInstance().getServer().getConsoleSender().sendMessage(Level.INFO.getLocalizedName() + " World " + worldName + " has been duplicated!");
            return true;
        } catch (IOException e) {
            EggWars.getInstance().getServer().getConsoleSender().sendMessage(Level.SEVERE.getLocalizedName() + " Error duplicating world!"); }
        return false;
    }

    public static org.bukkit.World loadWorld(String worldName)
    {
        org.bukkit.World world = new org.bukkit.WorldCreator(worldName).createWorld();
        return world;
    }

    public static boolean removeWorld(org.bukkit.World world) {
        if (Bukkit.getServer().getWorlds().contains(world))
            Bukkit.getServer().unloadWorld(world, false);
        try {
            File f = new File(Bukkit.getWorldContainer(), world.getName());
            if ((f.isDirectory()) && (f.listFiles() != null)) {
                for (File file : f.listFiles())
                    if (!delete(file))
                        FileUtils.forceDelete(file);
            }
            FileUtils.forceDelete(f);
            FileUtils.deleteDirectory(f);
            f.delete();
            FileUtils.deleteQuietly(f);
            return true;
        } catch (IOException e) {
            EggWars.getInstance().getServer().getConsoleSender().sendMessage(Level.SEVERE.getLocalizedName() + " Unable to delete world!"); }
        return false;
    }

    static boolean delete(File f)
    {
        if ((f.isDirectory()) && (f.listFiles() != null)) {
            for (File file : f.listFiles()) {
                if (!delete(file)) {
                    try {
                        FileUtils.forceDelete(file);
                    } catch (IOException e) {
                        EggWars.getInstance().getServer().getConsoleSender().sendMessage(Level.SEVERE.getLocalizedName() + " Unable to delete file " + file.getName() + "!");
                    }
                }
            }
            return true;
        }
        return false;
    }

    public static boolean removeWorld(String world) {
        if (Bukkit.getWorld(world) != null)
            Bukkit.getServer().unloadWorld(Bukkit.getWorld(world), false);
        try {
            FileUtils.deleteDirectory(new File(Bukkit.getWorldContainer(), world));
            return true;
        } catch (IOException e) {
            EggWars.getInstance().getServer().getConsoleSender().sendMessage(Level.SEVERE.getLocalizedName() + " Unable to delete world!"); }
        return false;
    }
}
