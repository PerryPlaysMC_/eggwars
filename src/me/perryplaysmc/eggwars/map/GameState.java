package me.perryplaysmc.eggwars.map;

public enum GameState {
  LOBBY("Lobby"), 
  PRE_START("PreStart"), 
  RUNNING("Running"), 
  END("End");


  private String localizedName;
  GameState(String localizedName) { this.localizedName = localizedName; }

  public static GameState value(String arg) {
    for (GameState state : GameState.values()) {
      if ((arg.equalsIgnoreCase(state.name())) || (arg.equalsIgnoreCase(state.getLocalizedName()))) return state;
    }
    return null;
  }
  
  public String getLocalizedName() {
    return localizedName;
  }
}
