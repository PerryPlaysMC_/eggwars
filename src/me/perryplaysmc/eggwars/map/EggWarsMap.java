package me.perryplaysmc.eggwars.map;

import me.perryplaysmc.eggwars.EggWars;
import me.perryplaysmc.eggwars.listeners.PlayerBlockInteraction;
import me.perryplaysmc.eggwars.map.team.EggWarsTeam;
import me.perryplaysmc.eggwars.map.team.EggWarsTeamManager;
import me.perryplaysmc.eggwars.map.timer.Generator;
import me.perryplaysmc.eggwars.map.timer.GeneratorType;
import me.perryplaysmc.eggwars.map.timer.Timer;
import me.perryplaysmc.eggwars.utils.EggWarsInventory;
import me.perryplaysmc.eggwars.utils.ScoreboardUtil;
import me.perryplaysmc.eggwars.utils.Utils;
import me.perryplaysmc.eggwars.utils.WorldGenerator;
import me.perryplaysmc.eggwars.utils.config.MapConfig;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Villager;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
@SuppressWarnings("all")
public class EggWarsMap implements Cloneable {
    private int Lobby, MaxPlayerTimer, Starting, Started, End, maxPlayers, minPlayers, maxPlayersPerTeam;
    private String name;
    private List<Generator> generators;
    private List<Player> players;
    private Location lobby;
    private Timer timer;
    private List<EggWarsTeam> teams;
    private MapConfig cfg;
    private UUID id;
    private World world;
    private GameState state;
    private boolean isDuped = false, isTesting = false;

    private List<EggWarsTeam> deadTeams;
    private HashMap<Player, me.perryplaysmc.eggwars.utils.items.Kits.KitHolder> kits;
    private List<Villager> villagers;

    public EggWarsMap(String name) {
        id = UUID.randomUUID();
        cfg = new MapConfig(this, name);
        if (!cfg.isSet("Name"))
            cfg.set("Name", name);
        villagers = new ArrayList();
        generators = new ArrayList();
        deadTeams = new ArrayList();
        maxPlayers = cfg.getMaxPlayers();
        minPlayers = cfg.getMinPlayers();
        maxPlayersPerTeam = cfg.getMaxPlayersPerTeam();
        kits = new HashMap<>();
        players = new ArrayList();
        lobby = cfg.getLocation("Locations.Lobby");
        teams = new ArrayList();

        Lobby = cfg.getLobby();
        MaxPlayerTimer = cfg.getMaxPlayerTimer();
        Starting = cfg.getStarting();
        End = cfg.getEnd();

        state = GameState.LOBBY;
        timer = new Timer(this);
        this.name = cfg.getString("Name");
        if (!cfg.isSet("UUID")) {
            cfg.set("UUID", id.toString());
            while (MapManager.byId(id) != null) {
                id = UUID.randomUUID();
            }
            cfg.set("UUID", id.toString());
        }
        if (cfg.getSection("Teams") != null)
            for (String a : cfg.getSection("Teams").getKeys(false)) {

                EggWarsTeam t = new EggWarsTeam(this, a, byNameChat(cfg.getString("Teams." + a + ".Color")), byName(cfg.getString("Teams." + a + ".DyeColor")));

                if (!teams.contains(t))
                    teams.add(t);
            }
        String a;
        if (cfg.isSet("Duped")) isDuped = cfg.getBoolean("Duped").booleanValue();
        if (!cfg.isSet("Duped"))
            cfg.set("Duped", Boolean.valueOf(isDuped));
        if (!isDuped) {
            loadWorld(isDuped);
        }
        MapManager.addMap(this);
        if (MapManager.byNameCount(name) > 0) {
            Object maps = new ArrayList();
            for (EggWarsMap map : MapManager.getMaps()) {
                if ((map.getName().equalsIgnoreCase(name)) && (!((Boolean)map.getConfig().get("Duped")).booleanValue())) {
                    map.loadWorld(false);
                    world = world;
                    break;
                }
            }
        }
        if ((!cfg.isSet("World")) && (world != null))
            cfg.set("World", world.getName());
        loadGenerators();
        loadVillagers();
        MapManager.getMaps().remove(this);
    }

    public Location getRespawnLoc() {
        String a = cfg.isSet("Locations.Respawn") ? cfg.get("Locations.Respawn") + "" : "";
        return Utils.deserializeLoc(a);
    }

    public void setRespawn(Location loc) {
        cfg.set("Locations.Respawn", Utils.serializeLoc(loc));
    }

    public void removeGenerator(Generator gen) {
        String aa = Utils.serializeLoc(gen.getSign().getLocation()) + "==" + gen.getStart() + "==" + gen.getMax();
        String g = gen.getType().toString().toLowerCase();

        List<String> locs = new ArrayList<>();
        for(String a : cfg.getSection("Generators." + g).getKeys(false)) {
            locs.add(cfg.getString("Generators." + g + "." + a));
        }
        for(String a : cfg.getSection("Generators." + g).getKeys(false)) {
            cfg.set("Generators." + g + "." + a, null);
        }
        int i = cfg.getSection("Generators." + g).getKeys(false).size()+1;
        for(String a : locs) {
            cfg.set("Generators." + g + "." + i, a);
            i++;
        }
    }

    public List<Location> getRespawnLocations() {
        List<Location> locations = new ArrayList();
        for (EggWarsTeam t : teams) {
            if (t.getRespawn() != null)
                locations.add(t.getRespawn());
        }
        return locations;
    }

    public Generator getGenerator(Sign s) {
        for (Generator gen : generators) {
            Location l1 = gen.getSign().getLocation();Location l2 = s.getLocation();
            if ((l1.getWorld().getName() == l2.getWorld().getName()) &&
                    (l1.getBlockX() == l2.getBlockX()) &&
                    (l1.getBlockY() == l2.getBlockY()) &&
                    (l1.getBlockZ() == l2.getBlockZ())) {
                return gen;
            }
        }
        return null;
    }

    public void loadWorld(boolean duped) {
        if (duped) return;
        if (Utils.getConfig().getBoolean("Map-Settings.perWorldMap"))
        {
            if ((!cfg.isSet("Duped")) || (!cfg.getBoolean("Duped"))) {
                String file = (String)(cfg.isSet("World") ? cfg.get("World") : name);
                if (new File(file).exists()) {
                    WorldManager.loadWorld(name);
                } else {
                    world = Bukkit.createWorld(new WorldGenerator(file));
                }
            } else {
                String name2 = world.getName() + "-" + (MapManager.byNameCount(name) + 1);
                WorldManager.duplicateWorld(cfg.getString("World"), name2);
                WorldManager.loadWorld(name2);
                world = Bukkit.getWorld(name2);
            }

            if (((MapManager.byName(name) != null) && (MapManager.byName(name).getId() != getId())) || ((cfg.isSet("Duped")) && (cfg.getBoolean("Duped")))) {
                String name2 = MapManager.byName(name).world.getName() + "-" + (MapManager.byNameCount(name) + 1);
                if (!new java.io.File(name).exists()) {
                    if (cfg.isSet("World"))
                        name2 = cfg.get("World") + "-" + (MapManager.byNameCount(name) + 1);
                    WorldManager.duplicateWorld(MapManager.byName(name).world.getName(), name2);
                    world = Bukkit.getWorld(name2);
                } else {
                    if (cfg.isSet("World"))
                        name2 = (String)cfg.get("World");
                    world = WorldManager.loadWorld(name2);
                }
                isDuped = true;
            } else {
                world = Bukkit.createWorld(new WorldGenerator(cfg.isSet("World") ? cfg.getString("World") : name));
            }
            world.setGameRule(GameRule.DO_WEATHER_CYCLE, false);
            world.setGameRule(GameRule.ANNOUNCE_ADVANCEMENTS, false);
            world.setGameRule(GameRule.DO_DAYLIGHT_CYCLE, false);
            world.setGameRule(GameRule.SPAWN_RADIUS, 0);
            world.setTime(6000);
        }
    }

    public void removePlayer(Player p) { if (getPlayers().contains(p)) {
        EggWarsTeam t = EggWarsTeamManager.getTeam(this, p);
        if (t == null) t = getTeams().get(0);
        broadCast(Utils.translate("Messages.Map.playerLeave", p.getName(), t.getColor()));
        getPlayers().remove(p);
        t.getPlayers().remove(p);
        p.setGameMode(org.bukkit.GameMode.SURVIVAL);
        if(Utils.deserializeLoc(Utils.getConfig().getString("Locations.mainLobby")) != null)
            p.teleport(Utils.deserializeLoc(Utils.getConfig().getString("Locations.mainLobby")));
        updateSign();
        (new BukkitRunnable() {
            @Override
            public void run() {
                if(Utils.inventories.containsKey(p)) {
                    EggWarsInventory i = Utils.inventories.get(p);
                    p.getInventory().setContents(i.getContents());
                    p.getInventory().setStorageContents(i.getStorageContents());
                    p.getInventory().setArmorContents(i.getArmorContents());
                    p.getInventory().setExtraContents(i.getExtraContents());
                }else p.getInventory().clear();
                p.updateInventory();
                p.setScoreboard(EggWars.getInstance().getDefaultScoreboard());
                for (Player p2 : Bukkit.getOnlinePlayers()) {
                    if (MapManager.byPlayer(p2) == null) {
                        p.showPlayer(p2);
                        p2.showPlayer(p);
                    } else {
                        p.hidePlayer(p2);
                        p2.hidePlayer(p);
                    }
                }
            }
        }).runTaskLater(EggWars.getInstance(), 15);
    }
    }

    public boolean isDuped() {
        return cfg.getBoolean("Duped").booleanValue();
    }

    public World getWorld() {
        return world;
    }

    private ChatColor byNameChat(String name) {
        for (ChatColor c : ChatColor.values()) {
            if ((c.name().equalsIgnoreCase(name)) || (c.getChar() == name.charAt(0))) return c;
        }
        return null;
    }

    private DyeColor byName(String name) {
        for (DyeColor c : DyeColor.values()) {
            if (c.name().equalsIgnoreCase(name)) return c;
        }
        return null;
    }

    public Timer getTimer() {
        return timer;
    }

    public void addPlayer(Player p) {
        if (getPlayers().contains(p)) {
            Utils.sendMessage(p, "Join.alreadyInGame-same");
            return;
        }
        if ((MapManager.byPlayer(p) != null) && (MapManager.byPlayer(p).getId().toString() != getId().toString())) {
            Utils.sendMessage(p, "Join.alreadyInGame-other");
            return;
        }
        if (getPlayers().size() == maxPlayers) {
            Utils.sendMessage(p, "Join.gameFull");
            EggWarsMap m = MapManager.byName(name);
            int i = 0;
            Utils.sendMessage(p, "Join.searching");
            boolean found = false;
            int ii = 0;
            if (MapManager.getMaps().size() > 1) {
                for (int a = 0; a < MapManager.getMaps().size(); a++) {
                    EggWarsMap ma = MapManager.getMaps().get(a);

                    ii++;
                    if ((m != null) &&
                            (ma.getId().toString() != getId().toString()))
                    {

                        if ((ii == MapManager.getMaps().size()) || (!p.isOnline())) {
                            found = false;
                            break;
                        }
                        if ((ma.getName().equalsIgnoreCase(getName())) && (!ma.getId().toString().equalsIgnoreCase(getId().toString())) &&
                                (ma.getPlayers().size() != ma.getMaxPlayers())) {
                            if (ma.getMaxPlayers() == 0)
                                ma.setMaxPlayers(Utils.getConfig().getInt("Map-Settings.Defaults.MaxPlayers"));
                            if (ma.getMinPlayers() == 0)
                                ma.setMinPlayers(Utils.getConfig().getInt("Map-Settings.Defaults.MinPlayers"));
                            if ((MapManager.byPlayer(p) != null) && (MapManager.byPlayer(p).getId().toString() == m.getId().toString())) {
                                Utils.sendMessage(p, "Join.alreadyInGame-same");
                                break;
                            }
                            if ((MapManager.byPlayer(p) != null) && (MapManager.byPlayer(p).getId().toString() != m.getId().toString())) {
                                Utils.sendMessage(p, "Join.alreadyInGame-other");
                                break;
                            }
                            if ((ma.getPlayers().size() < ma.getMaxPlayers()) && (ma.getState() == GameState.LOBBY) && (!ma.getPlayers().contains(p))) {
                                ma.getPlayers().add(p);
                                found = true;
                                Utils.scheduleJoin(ma, p);
                                break;
                            }
                        }
                    }
                }
            }
            if (!found) {
                m = clone();
                if (m.getMaxPlayers() == 0)
                    m.setMaxPlayers(Utils.getConfig().getInt("Map-Settings.Defaults.MaxPlayers"));
                if (m.getMinPlayers() == 0)
                    m.setMinPlayers(Utils.getConfig().getInt("Map-Settings.Defaults.MinPlayers"));
                m.getConfig().set("Duped", Boolean.valueOf(true));
                MapManager.addMap(m);
                if (m.getPlayers().size() < m.getMaxPlayers()) {
                    if ((MapManager.byPlayer(p) != null) && (MapManager.byPlayer(p).getId().toString() == m.getId().toString())) {
                        Utils.sendMessage(p, "Join.alreadyInGame-same");
                        return;
                    }
                    if ((MapManager.byPlayer(p) != null) && (MapManager.byPlayer(p).getId().toString() != m.getId().toString())) {
                        Utils.sendMessage(p, "Join.alreadyInGame-other");
                        return;
                    }
                    if (!m.getPlayers().contains(p)) {
                        m.addPlayer(p);
                        if ((m.getPlayers().size() == m.getMinPlayers()) && (m.getPlayers().size() > 0)) {
                            m.getTimer().setPaused(false);
                        }
                    }
                } else {
                    Utils.sendMessage(p, "Join.gamesFull");
                }
                return;
            }
            return;
        }
        if (getState() != GameState.LOBBY) {
            Utils.sendMessage(p, "Join.gameAlreadyStarted");
            return;
        }
        getPlayers().add(p);
        Utils.scheduleJoin(this, p);
    }

    public void updateSign() {
        boolean found = false;
        if (!cfg.isSet("Locations.Sign")) return;
        if(getSignBlock().getType() == Material.AIR) {
            for(int x = -1; x < 2; x++) {
                for(int z = -1; z < 2; z++) {
                    Block b = getSignBlock().getLocation().clone().add(x, 0, z).getBlock();
                    if(b != null && b.getType().isSolid() && b.getType() != Material.AIR) {
                        Block f = b.getRelative(getSignBlock().getFace(b).getOppositeFace());
                        f.setType(Material.OAK_WALL_SIGN);
                        Sign s = (Sign) f.getState();
                        org.bukkit.material.Sign zz = new org.bukkit.material.Sign(f.getType());
                        zz.setFacingDirection(getSignBlock().getFace(b).getOppositeFace());
                        s.setData(zz);
                        s.update();
                        s.update(true);
                        return;
                    }
                }
            }
            if(getSignBlock().getRelative(BlockFace.DOWN).getType() == Material.AIR)
                getSignBlock().getRelative(BlockFace.DOWN).setType(Material.STONE);
            getSignBlock().setType(Material.OAK_SIGN);
        }
        Sign s = getSign();
        if (s == null) return;
        String[] lines = { trans("Sign.lines.line-1"), trans("Sign.lines.line-2"), trans("Sign.lines.line-3"), trans("Sign.lines.line-4") };
        me.perryplaysmc.eggwars.events.EggWarsSignUpdateEvent e = new me.perryplaysmc.eggwars.events.EggWarsSignUpdateEvent(s, this, lines);
        Bukkit.getPluginManager().callEvent(e);
        if (e.isCancelled()) return;
        for (int i = 0; i < e.getLines().length; i++) {
            s.setLine(i, e.getLine(i));
        }
        s.update(true);
    }

    public Location getLobbyLoc() {
        if ((cfg.isSet("Locations.Lobby")) && (cfg.get("Locations.Lobby") != "") && (!cfg.getString("Locations.Lobby").isEmpty()) &&
                (cfg.getString("Locations.Lobby") != "null") &&
                (cfg.get("Locations.Lobby") != null))
            return Utils.deserializeLoc(cfg.getString("Locations.Lobby"));
        return null;
    }

    public void setLobby(Location lobby) {
        this.lobby = lobby;
        cfg.set("Locations.Lobby", Utils.serializeLoc(lobby));
    }

    String trans(String str) {
        return Utils.translate(str, getName(), Integer.valueOf(getMinPlayers()), Integer.valueOf(getMaxPlayers()),
                Integer.valueOf(getPlayers().size()),
                Integer.valueOf(getMaxPlayersPerTeam()), getState().getLocalizedName());
    }

    public EggWarsMap clone()
    {
        String na = name;
        String worldName = name + "-" + (MapManager.byNameCount(name) + 1);
        EggWarsMap m = new EggWarsMap(name + "-" + (MapManager.byNameCount(name) + 1));
        m.setName(na);
        WorldManager.duplicateWorld(getWorld().getName(), worldName);
        WorldManager.loadWorld(worldName);
        World world = Bukkit.getWorld(worldName);
        world = world;
        if (getLobbyLoc() != null)
            m.setLobby(getLobbyLoc());
        teams = teams;
        timer = new Timer(m);
        return m;
    }

    public void broadCast(String msg) {
        for (Player p : getPlayers()) {
            p.sendMessage(Utils.translate(msg));
        }
    }

    public void broadcast(EggWarsTeam exclude, String message) {
        for (EggWarsTeam t : teams) {
            if (t.getName() != exclude.getName()) {
                t.broadcast(message);
            }
        }
    }

    public EggWarsTeam getTeam(Player p) {
        return me.perryplaysmc.eggwars.map.team.EggWarsTeamManager.getTeam(this, p);
    }

    public UUID getId() {
        return id;
    }

    public MapConfig getConfig() {
        return cfg;
    }

    public Sign getSign() {
        return getSignBlock().getType().name().contains("SIGN") ? (Sign)(getSignBlock().getState()) : null;
    }
    public Block getSignBlock() {
        return (cfg.isSet("Locations.Sign") ? Utils.deserializeLoc(cfg.getString("Locations.Sign")).getBlock() : null);
    }

    public void setSign(Location sign) {
        if (sign == null) {
            cfg.set("Locations.Sign", null);
            return;
        }
        cfg.set("Locations.Sign", Utils.serializeLoc(sign));
    }

    public List<EggWarsTeam> getTeams() {
        return teams;
    }

    public void addTeam(EggWarsTeam team) {
        if (!teams.contains(team)) {
            teams.add(team);
        }
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        cfg.set("Name", name);
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
        cfg.set("Players.Max", Integer.valueOf(maxPlayers));
    }

    public int getMinPlayers() {
        return minPlayers;
    }

    public void setMinPlayers(int minPlayers) {
        this.minPlayers = minPlayers;
        cfg.set("Players.Min", Integer.valueOf(minPlayers));
        updateSign();
    }

    public int getMaxPlayersPerTeam() {
        return maxPlayersPerTeam;
    }

    public void setMaxPlayersPerTeam(int maxPlayersPerTeam) {
        this.maxPlayersPerTeam = maxPlayersPerTeam;
        cfg.set("Players.MaxTeamSize", Integer.valueOf(maxPlayersPerTeam));
        updateSign();
    }

    public GameState getState() {
        return state;
    }

    public void setState(GameState state) {
        this.state = state;
        updateSign();
    }

    void loadGenerators() {
        if (!cfg.isSet("Generators")) return;
        for (String e : cfg.getSection("Generators").getKeys(false)) {
            for (String a1 : cfg.getSection("Generators." + e).getKeys(false)) {
                String a = cfg.get("Generators." + e + "." + a1) + "";
                if ((cfg.get("Generators." + e + "." + a1) != null) &&
                        (a.contains("=="))) {
                    String[] args = a.split("==");
                    if (args.length == 3)
                        if ((Utils.deserializeLoc(args[0]) != null) && (!Utils.deserializeLoc(args[0]).getBlock().getType().name().contains("AIR"))) {
                            Generator gen = new Generator(this,
                                    (Sign)Utils.deserializeLoc(args[0]).getBlock().getState(),
                                    Integer.parseInt(args[1]), Integer.parseInt(args[2]),  GeneratorType.valueOf(e.toUpperCase()));
                            gen.run = gen.getSpeed();
                            gen.updateSign(true);
                            gen.run = 0.0D;
                            generators.add(gen);
                        }
                }
            }
        }
        String e; }

    public List<Villager> getVillagers() { return villagers; }

    public void loadVillagers() {
        if (!cfg.isSet("Locations.Villagers")) return;
        for (String a : cfg.getSection("Locations.Villagers").getKeys(false)) {
            Location lo = cfg.getLocation("Locations.Villagers." + a + ".Location");
            Location loc = new Location(lo.getWorld(), lo.getBlockX(), lo.getBlockY(), lo.getBlockZ()).add(0.5D, 0.0D, 0.5D);
            loc.setPitch(lo.getPitch());
            loc.setYaw(lo.getYaw());

            Villager villager = (Villager)loc.getWorld().spawnEntity(loc, org.bukkit.entity.EntityType.VILLAGER);
            villager.setProfession(org.bukkit.entity.Villager.Profession.FARMER);
            villager.setProfession(Villager.Profession.FLETCHER);
            villager.setInvulnerable(true);
            villager.setAI(false);
            villager.setRemoveWhenFarAway(false);
            villager.setCanPickupItems(false);
            villager.setCollidable(false);
            villager.setRecipes(new ArrayList());

            ArmorStand as1 = (ArmorStand)loc.getWorld().spawnEntity(loc.clone().add(0.0D, 0.2D, 0.0D), org.bukkit.entity.EntityType.ARMOR_STAND);
            as1.setGravity(false);
            as1.setInvulnerable(true);
            as1.setVisible(false);
            as1.setBasePlate(false);
            as1.setVisible(false);
            as1.setVisible(false);
            as1.setCustomName("§6§lEggwars Shop");
            as1.setCanPickupItems(false);
            as1.setCustomNameVisible(true);
            as1.setCollidable(false);

            ArmorStand as2 = (ArmorStand)loc.getWorld().spawnEntity(loc.clone().add(0.0D, 0.02D, 0.0D), org.bukkit.entity.EntityType.ARMOR_STAND);
            as2.setGravity(false);
            as2.setInvulnerable(true);
            as2.setVisible(false);
            as2.setBasePlate(false);
            as2.setVisible(false);
            as2.setVisible(false);
            as2.setCustomName("§7Click to open");
            as2.setCanPickupItems(false);
            as2.setCustomNameVisible(true);
            as2.setCollidable(false);
            villagers.add(villager);
        }
    }

    public void addVillager(Location lo) {
        if (villagers.size() == getTeams().size()) return;
        int i = cfg.isSet("Locations.Villagers") ? cfg.getSection("Locations.Villagers").getKeys(false).size() + 1 : 0;
        Location loc = new Location(lo.getWorld(), lo.getBlockX(), lo.getBlockY(), lo.getBlockZ()).add(0.5D, 0.0D, 0.5D);
        loc.setPitch(lo.getPitch());
        loc.setYaw(lo.getYaw());
        cfg.set("Locations.Villagers." + i + ".Location", loc);

        Villager villager = (Villager)loc.getWorld().spawnEntity(loc, org.bukkit.entity.EntityType.VILLAGER);
        villager.setProfession(Villager.Profession.FLETCHER);
        villager.setInvulnerable(true);
        villager.setAI(false);
        villager.setRemoveWhenFarAway(false);
        villager.setCanPickupItems(false);
        villager.setCollidable(false);
        villager.setRecipes(new ArrayList());

        ArmorStand as1 = (ArmorStand)loc.getWorld().spawnEntity(loc.clone().add(0.0D, 0.2D, 0.0D), org.bukkit.entity.EntityType.ARMOR_STAND);
        as1.setGravity(false);
        as1.setInvulnerable(true);
        as1.setVisible(false);
        as1.setBasePlate(false);
        as1.setCustomName("§6§lEggwars Shop");
        as1.setCanPickupItems(false);
        as1.setCustomNameVisible(true);
        as1.setCollidable(false);

        ArmorStand as2 = (ArmorStand)loc.getWorld().spawnEntity(loc.clone().add(0.0D, 0.02D, 0.0D), org.bukkit.entity.EntityType.ARMOR_STAND);
        as2.setGravity(false);
        as2.setInvulnerable(true);
        as2.setVisible(false);
        as2.setBasePlate(false);
        as2.setCustomName("§7Click to open");
        as2.setCanPickupItems(false);
        as2.setCustomNameVisible(true);
        as2.setCollidable(false);
        villagers.add(villager);
    }

    public void removeVillager(Villager v) {
        if (!villagers.contains(v)) return;
        for (String a : cfg.getSection("Locations.Villagers").getKeys(false)) {
            if (Utils.checkLocations(v.getLocation(), cfg.getLocation("Locations.Villagers." + a + ".Location"))) {
                cfg.set("Locations.Villagers." + a, null);
                HashMap<String, String> vals = new HashMap<>();
                for (String a2 : cfg.getSection("Locations.Villagers").getKeys(false)) {
                    if ((a2 != a) && (cfg.getString("Locations.Villagers." + a2) != null) && (cfg.getString("Locations.Villagers." + a2) != "") && (!cfg.getString("Locations.Villagers." + a2).isEmpty())) {
                        vals.put(a2, cfg.getString("Locations.Villagers." + a2));
                    }
                }
                v.remove();
                villagers.remove(v);
                for (Entity e : v.getNearbyEntities(0.0D, 0.2D, 0.0D))
                    if(e instanceof ArmorStand) e.remove();
                break;
            }
        }
    }

    public List<Generator> getGenerators() {
        return generators;
    }

    public Generator addGenerator(Generator gen) {
        String g = gen.getType().toString().toLowerCase();
        int i = 1;
        String a = Utils.serializeLoc(gen.getSign().getLocation()) + "==" + gen.getStart() + "==" + gen.getMax();
        while (cfg.isSet("Generators." + g + ".generator-" + i)) i++;
        cfg.set("Generators." + g + ".generator-" + i, a);
        gen.run = gen.getSpeed();
        gen.updateSign(true);
        if (gen.getSpeed() < 10000.0D) {
            gen.run = 0.0D;
        }
        generators.add(gen);
        return gen; }

    HashMap<Player, Scoreboard> scoreboards = new HashMap<>();

    public void updateScoreboard() { for (Player p : scoreboards.keySet()) {
        Scoreboard b = scoreboards.get(p);
        b.getObjective(org.bukkit.scoreboard.DisplaySlot.SIDEBAR).setDisplayName(Utils.translate("[p]" + timer.format()));
        EggWarsTeam t = getTeam(p);
        if (t != null) {
            String egg = t.hasEgg() ? "§a√" : "§cX";
            String l = t.getPlayers().contains(p) ? "§l" : "";
            Team et = b.getTeam(t.getName() + "");
            if (et != null) {
                et.setPrefix(Utils.translate(egg + " " + t.getColor()));
                et.setSuffix(l + t.getName());
            }
        }
        for (EggWarsTeam e : getTeams()) {
            String egg = e.hasEgg() ? "§a√" : "§cX";
            if ((t == null) || (e.getName() != t.getName())) {
                Team a = b.getTeam(e.getName() + "");
                if (a != null) {
                    a.setPrefix(Utils.translate(egg + " " + e.getColor()));
                    a.setSuffix(e.getName());
                }
            } }
        p.setScoreboard(b);
        scoreboards.put(p, b);
    }
    }

    public List<EggWarsTeam> getDeadTeams() {
        return deadTeams;
    }

    public void update(Player p) {
        ScoreboardUtil sb = new ScoreboardUtil(p.getName(), "dummy");
        ScoreboardUtil sb2 = sb;
        if (state == GameState.LOBBY)
        {



            sb.setDisplay("[p]" + timer.format(), org.bukkit.scoreboard.DisplaySlot.SIDEBAR).setTeam(2, "[pc]", "[c]EggWarsMap: ", name).setTeam(1, "§§[pc]", "[c]Teams of: ", getMaxPlayersPerTeam() + "").setTeam(0, "§![pc]", "[c]Website: ", "Website not set");
            p.setScoreboard(sb.finish());
            scoreboards.put(p, sb.finish()); }
        if ((state == GameState.PRE_START) || (state == GameState.RUNNING)) {
            if (sb.getTeam("[pc]") != null)
                sb.getTeam("[pc]").unregister();
            if (sb.getTeam("§§[pc]") != null)
                sb.getTeam("§§[pc]").unregister();
            if (sb.getTeam("§![pc]") != null)
                sb.getTeam("§![pc]").unregister();
            sb2.setDisplay("[p]" + timer.format(), org.bukkit.scoreboard.DisplaySlot.SIDEBAR);
            sb2.setTeam(getMaxPlayersPerTeam() + 1, "§c", "§c", "§a", "§b");
            sb2.setTeam(-1, "§d", "§d", "§b", "§a");
            sb2.setTeam(-2, "§f", "§f", "[c]Website: ", "[pc]titanreborn.net");
            for (EggWarsTeam t : teams) {
                if (t.getPlayers().size() + -t.getDead().size() == 0) {
                    if (sb2.getTeam(t.getName()) != null) {
                        sb2.getTeam(t.getName()).unregister();
                    }
                }
                else {
                    sb2.setScoreboard(t.registerTeam(sb2.finish()));
                    String egg = t.hasEgg() ? "§a√" : "§cX";
                    String l = t.getPlayers().contains(p) ? "§l" : "";
                    sb2.setTeam(t.getPlayers().size() + -t.getDead().size(), t.getName(), t.getColor() + "", egg + " " + t.getColor(), l + t.getName());
                } }
            p.setScoreboard(sb2.finish());
            scoreboards.put(p, sb2.finish());
        }
        if (state == GameState.END) {
            EggWarsTeam a = activeTeams().get(0);
            for (EggWarsTeam t : teams) {
                if (t.getPlayers().size() + -t.getDead().size() == 0) {
                    if (sb2.getTeam(t.getName()) != null) {
                        sb2.getTeam(t.getName()).unregister();
                    }
                }
                else {
                    sb2.setScoreboard(t.registerTeam(sb2.finish()));
                    String egg = t.hasEgg() ? "§a√" : "§cX";
                    String l = t.getPlayers().contains(p) ? "§l" : "";
                    if (sb2.getTeam(t.getName()) != null)
                        sb2.getTeam(t.getName()).unregister();
                }
            }
            sb2.setTeam(a.getPlayers().size() == 0 ? 5 : a.getPlayers().size(), "winner", a.getColor() + "", "[c]Winning team: ", a.getName());
            sb2.setTeam(a.getPlayers().size() == 0 ? 4 : a.getPlayers().size() + -1, "hasegg", "§a" + a.getColor(), "[c]EggStatus: ", a
                    .eggisLost().replace("Yes", "Alive").replace("No", "Dead"));
            p.setScoreboard(sb2.finish());
            scoreboards.put(p, sb2.finish());
        }
    }

    public HashMap<Player, me.perryplaysmc.eggwars.utils.items.Kits.KitHolder> kits() { return kits; }

    public List<EggWarsTeam> activeTeams()
    {
        List<EggWarsTeam> a = new ArrayList();
        for (EggWarsTeam t : getTeams()) {
            if (!deadTeams.contains(t)) {
                a.add(t);
            }
        }
        return a;
    }

    public void endGame() {
        endGame(false);
    }

    public void endGame(boolean isOffline) {
        timer.setPaused(true);
        timer.resetTimings();
        Scoreboard sb = me.perryplaysmc.eggwars.EggWars.getInstance().getResetScoreboard();
        if ((getPlayers() != null) && (getPlayers().size() > 0))
            for (Player p : getPlayers())
                if (p != null) {
                    for (Player p2 : Bukkit.getOnlinePlayers()) {
                        if ((MapManager.byPlayer(p2) == null) || ((MapManager.byPlayer(p2) != null) && (MapManager.byPlayer(p2) == this))) {
                            p.showPlayer(p2);
                            p2.showPlayer(p);
                        }
                    }
                    p.setScoreboard(sb);
                    p.setGameMode(GameMode.SURVIVAL);
                    if(Utils.deserializeLoc(Utils.getConfig().getString("Locations.mainLobby")) != null)
                        p.teleport(Utils.deserializeLoc(Utils.getConfig().getString("Locations.mainLobby")));
                    p.setScoreboard(EggWars.getInstance().getDefaultScoreboard());
                    if(Utils.inventories.containsKey(p)) {
                        EggWarsInventory i = Utils.inventories.get(p);
                        p.getInventory().setContents(i.getContents());
                        p.getInventory().setStorageContents(i.getStorageContents());
                        p.getInventory().setArmorContents(i.getArmorContents());
                        p.getInventory().setExtraContents(i.getExtraContents());
                    }
                    p.updateInventory();
                }
        getPlayers().clear();
        if (isOffline) {
            for (Villager v : getVillagers()) {
                v.setInvulnerable(false);
                v.remove();
                v.setHealth(0);
            }
            for (Entity i : world.getEntities()) {
                if (villagers.contains(i)) i.remove();
                if (((i instanceof ArmorStand)) && (
                        (i.getCustomName().equalsIgnoreCase("§6§lEggwars Shop")) ||
                                (i.getCustomName().equalsIgnoreCase("§7Click to open")))) {
                    i.remove();
                    ((ArmorStand)i).setHealth(0);
                    continue;
                }
                if(!(i instanceof Player)) i.remove();
            }
        }
        updateSign();
        if ((getTeams() != null) && (getTeams().size() > 0))
            for (EggWarsTeam t : getTeams()) {
                t.getPlayers().clear();
                t.getDead().clear();
            }
        deadTeams.clear();
        if ((kits != null) && (kits.size() > 0))
            kits.clear();
        Utils.removeDupedMap(this);
        if (PlayerBlockInteraction.placed.containsKey(this)) {
            for (Block b : PlayerBlockInteraction.placed.get(this).keySet()) {
                BlockState x = PlayerBlockInteraction.placed.get(this).get(b);
                b.setType(x.getType());
                b.setBlockData(x.getBlockData());
                b.getState().update(true);
            }
        }
        if (!isOffline) {
            new org.bukkit.scheduler.BukkitRunnable()
            {
                public void run()
                {
                    setState(GameState.LOBBY);
                    updateSign();
                }
            }.runTaskLater(me.perryplaysmc.eggwars.EggWars.getInstance(), 60L); }
    }

    public boolean isTesting() {
        return isTesting;
    }

    public void setTesting(boolean testing) {
        isTesting = testing;
    }

    public int getEnd() {
        return End;
    }

    public int getStarted() { return Started; }

    public int getStarting() {
        return Starting;
    }

    public int getLobby() { return Lobby; }

    public int getMaxPlayerTimer() {
        return MaxPlayerTimer;
    }
}
