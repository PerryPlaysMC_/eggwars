package me.perryplaysmc.eggwars.map;

import java.util.List;
import java.util.UUID;

public class MapManager {
    private static List<EggWarsMap> maps;

    static {
        if (maps == null)
            maps = new java.util.ArrayList<>();
    }

    public static EggWarsMap addMap(EggWarsMap map) {
        maps.add(map);return map;
    }

    public static EggWarsMap byName(String name) {
        for (EggWarsMap map : maps) {
            if (map.getName().equalsIgnoreCase(name)) return map;
        }
        return null;
    }

    public static int byNameCount(String name) { int i = 0;
        for (EggWarsMap map : maps) {
            if (map.getName().equalsIgnoreCase(name)) i++;
        }
        return i;
    }

    public static EggWarsMap byId(String name) {
        for (EggWarsMap map : maps) {
            if (map.getId().toString() == name) return map;
        }
        return null;
    }

    public static EggWarsMap byPlayer(org.bukkit.entity.Player p) {
        for (EggWarsMap map : maps) {
            if (map.getPlayers().contains(p)) return map;
        }
        return null;
    }

    public static EggWarsMap byId(UUID id) {
        for (EggWarsMap map : maps) {
            if (map.getId().toString() == id.toString()) return map;
        }
        return null;
    }

    public static List<EggWarsMap> getMaps() {
        return maps;
    }

    public MapManager() {}
}
