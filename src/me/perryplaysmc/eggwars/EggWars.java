package me.perryplaysmc.eggwars;

import me.perryplaysmc.eggwars.commands.EggWarsCommand;
import me.perryplaysmc.eggwars.map.*;
import me.perryplaysmc.eggwars.map.timer.*;
import me.perryplaysmc.eggwars.utils.*;
import me.perryplaysmc.eggwars.utils.abstraction.AbstractionHandler;
import me.perryplaysmc.eggwars.utils.chat.json.command.JsonHandler;
import me.perryplaysmc.eggwars.utils.config.Config;
import me.perryplaysmc.eggwars.utils.inventory.*;
import me.perryplaysmc.eggwars.utils.items.Kits;
import me.perryplaysmc.eggwars.utils.placeholders.EggWarsPlaceHolder;
import org.bukkit.Bukkit;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EggWars extends JavaPlugin {
    private static EggWars instance;
    private Scoreboard defaultScoreBoard;
    private List<EggWarsMap> maps = new ArrayList<>();
    private Config cfg, winData;
    private EggWarsCommand cmd;
    private Config inventoryMain;
    private List<Listener> listeners;
    public GlowEnchant ench = new GlowEnchant("glow");


    public void onEnable() {
        instance = this;
        AbstractionHandler.load();
        cmd = new EggWarsCommand();
        new EggWarsAPI(this);
        if(Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null){
            new EggWarsPlaceHolder().register();
        }
        listeners = new ArrayList<>();
        cfg = new Config(getInstance(), new File("plugins/EggWars"), "config.yml");
        if(cfg.getBoolean("winData")) {
            winData = new Config("winData.yml");
        }
        inventoryMain = new Config(this, new File("plugins/EggWars"), "ShopMain.yml");
        defaultScoreBoard = Bukkit.getScoreboardManager().getNewScoreboard();
        boolean isSet = cfg.isSet("loadDefault-Shops");
        if (cfg.getBoolean("loadDefault-Shops") || !isSet) {
            new Config(new File("plugins/EggWars/Guis"), "Guis/Blocks.yml", "Blocks.yml");
            new Config(new File("plugins/EggWars/Guis"), "Guis/Weapons.yml", "Weapons.yml");
            new Config(new File("plugins/EggWars/Guis"), "Guis/Tools.yml", "Tools.yml");
            new Config(new File("plugins/EggWars/Guis"), "Guis/Food.yml", "Food.yml");
            new Config(new File("plugins/EggWars/Guis"), "Guis/Armor.yml", "Armor.yml");
        }
        if(Enchantment.isAcceptingRegistrations())
            Enchantment.registerEnchantment(ench);
        getCommand("eggwars").setExecutor(cmd);
        Kits.loadKits();
        registerListeners();
        getServer().getScheduler().scheduleSyncRepeatingTask(this, new TPS(), 100L, 1L);
        registerMaps();
        InventoryManager.reloadGuis();
    }

    public EggWarsCommand getEggWarsCommand() {
        return cmd;
    }

    @SuppressWarnings("all")
    public void resetEnchantments() {
        try {
            Field byKeyField = Enchantment.class.getDeclaredField("byKey");
            Field byNameField = Enchantment.class.getDeclaredField("byName");

            byKeyField.setAccessible(true);
            byNameField.setAccessible(true);
            HashMap<NamespacedKey, Enchantment> byKey = (HashMap<NamespacedKey, Enchantment>)byKeyField.get(null);
            HashMap<String, Enchantment> byName = (HashMap<String, Enchantment>)byNameField.get(null);

            byKey.remove(ench.getKey());

            byName.remove(ench.getName());
        } catch (Exception localException) {}
    }

    void registerListeners() {
        for (Class c : Utils.getClasses(new File("plugins/EggWars.jar"), "me.perryplaysmc.eggwars.listeners")) {
            try {
                if (!c.getName().contains("$")) {
                    if (((c.newInstance() instanceof Listener)) &&
                            (!listeners.contains(c.newInstance()))) {
                        getServer().getPluginManager().registerEvents((Listener)c.newInstance(), this);
                        listeners.add((Listener)c.newInstance());
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        getServer().getPluginManager().registerEvents(new JsonHandler(), this);
        listeners.add(new JsonHandler());
    }

    public void removeGenerator(EggWarsMap m) {
        maps.remove(m);
    }

    public void addGenerators(EggWarsMap m) {
        if (!maps.contains(m))
            maps.add(m);
        if (maps.size() == 1) {
            new org.bukkit.scheduler.BukkitRunnable()
            {
                public void run() {
                    for (EggWarsMap map : maps) {
                        if ((map.getState() != GameState.LOBBY) && (map.getState() != GameState.END)) {
                            for (Generator gen : map.getGenerators())
                                if (gen.getSpeed() < 1000000.0D)
                                    gen.run();
                        }
                    }
                }
            }.runTaskTimer(getInstance(), 0L, 2L);
        }
    }


    public void onDisable() {
        resetEnchantments();
        for (EggWarsMap m : MapManager.getMaps())
            m.endGame(true);
    }

    void registerMaps() {
        if (!Utils.getConfig().getBoolean("Map-Settings.perMapConfig")) {
            cfg = new Config(this, getDataFolder(), "maps.yml");
            if (cfg.getSection("Maps") != null)
                for (String name : cfg.getSection("Maps").getKeys(false)) {
                    EggWarsMap m = Utils.loadMap(name);
                    m.setMaxPlayers(cfg.getInt("Maps." + name + ".Players.Max"));
                    m.setMinPlayers(cfg.getInt("Maps." + name + ".Players.Min"));
                    m.setMaxPlayersPerTeam(cfg.getInt("Maps." + name + ".Players.MaxTeamSize"));
                    m.setSign(Utils.deserializeLoc(cfg.getString("Maps." + name + ".Locations.Sign")));
                    MapManager.addMap(m);
                }
            return;
        }
        File f = new File("plugins/EggWars/maps");
        File[] fi = f.listFiles();
        if ((f.exists()) && (fi != null) && (fi.length > 0)) {
            for (File map : fi) {
                if ((map.getName().endsWith(".yml")) && (map.exists()) && (map != null)) {
                    Config mc = new Config("plugins/EggWars/maps", map.getName());
                    EggWarsMap m = new EggWarsMap(mc.getString("Name"));
                    if ((m.isDuped()) || (mc.getBoolean("Duped"))) {
                        if (m.getWorld() != null)
                            WorldManager.removeWorld(m.getWorld());
                        else
                            WorldManager.removeWorld(m.getConfig().getString("World"));
                        boolean x = map.delete();
                        if(x){x = false;}
                    }
                    else {
                        if ((mc.isSetMap(m.getName(), "Locations.Sign")) && (mc.getMap(m.getName(), "Locations.Sign") != "") &&
                                (!((String)mc.getMap(m.getName(), "Locations.Sign")).isEmpty()) &&
                                (mc.getMap(m.getName(), "Locations.Sign") != "null") &&
                                (mc.getMap(m.getName(), "Locations.Sign") != null) &&
                                (Bukkit.getWorlds().contains(Utils.deserializeLoc(m.getConfig().getString("Locations.Sign")).getWorld()))) {
                            m.setSign(Utils.deserializeLoc(m.getConfig().getString("Locations.Sign")));
                        }
                        if ((mc.isSetMap(m.getName(), "Locations.Lobby")) && (mc.getMap(m.getName(), "Locations.Lobby") != "") &&
                                (!((String)mc.getMap(m.getName(), "Locations.Lobby")).isEmpty()) &&
                                (mc.getMap(m.getName(), "Locations.Lobby") != "null") &&
                                (mc.getMap(m.getName(), "Locations.Lobby") != null)) {
                            m.setLobby(Utils.deserializeLoc(m.getConfig().getString("Locations.Lobby")));
                        }
                        MapManager.addMap(m);
                    }
                }
            }
        }
    }

    public static EggWars getInstance() { return instance; }

    public Scoreboard getDefaultScoreboard()
    {
        return defaultScoreBoard;
    }

    public Scoreboard registerNewScoreBoard() {
        return Bukkit.getScoreboardManager().getNewScoreboard();
    }

    public Scoreboard getResetScoreboard() {
        Scoreboard sb = registerNewScoreBoard();
        sb.registerNewObjective("TestingEndGame", "dummy");
        return sb;
    }

    public Config getConfiguration() {
        return cfg;
    }

    public Config getInventoryMain() {
        return inventoryMain;
    }

    public Config getWinData() {
        return winData;
    }
}
