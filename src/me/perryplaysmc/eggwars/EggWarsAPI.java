package me.perryplaysmc.eggwars;

import me.perryplaysmc.eggwars.utils.SubCommand;
import me.perryplaysmc.eggwars.utils.config.Config;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class EggWarsAPI {

    private static EggWars main;
    private static EggWarsAPI inst;

    protected EggWarsAPI(EggWars main) {
        this.main = main;
        this.inst = this;
    }


    public static EggWarsAPI addSubCommand(SubCommand... commands) {
        main.getEggWarsCommand().addSubCommand(commands);
        return inst;
    }

    public static Config getDataConfig() {
        return main.getWinData();
    }

    public static int getGamesPlayed(OfflinePlayer player) {
        if(getDataConfig() == null || !getDataConfig().isSet("Data." + player.getUniqueId() + ".gamesPlayed")) return 0;
        return getDataConfig().getInt("Data." + player.getUniqueId() + ".gamesPlayed");
    }

    public static int getTimePlayedSeconds(OfflinePlayer player) {
        if(getDataConfig() == null || !getDataConfig().isSet("Data." + player.getUniqueId() + ".timePlayed")) return 0;
        return getDataConfig().getInt("Data." + player.getUniqueId() + ".timePlayed");
    }

    public static String getFormattedPlayerTime(OfflinePlayer player) {
        return getFormattedTime(getTimePlayedSeconds(player));
    }

    public static HashMap<OfflinePlayer, HashMap<WinType, Integer>> getData() {
        if(getDataConfig() == null) return new HashMap<>();
        HashMap<OfflinePlayer, HashMap<WinType, Integer>> data = new HashMap<>();
        for(OfflinePlayer p : Bukkit.getOfflinePlayers()) {
            HashMap<WinType, Integer> d = new HashMap<>();
            if(getDataConfig().isSet("Data." + p.getUniqueId() + ".Solo")) {
                d.put(WinType.SOLO, getDataConfig().getInt("Data." + p.getUniqueId() + ".Solo"));
            }
            if(getDataConfig().isSet("Data." + p.getUniqueId() + ".Team")) {
                d.put(WinType.TEAM, getDataConfig().getInt("Data." + p.getUniqueId() + ".Team"));
            }
            data.put(p, d);
        }
        for(Player p : Bukkit.getOnlinePlayers()) {
            HashMap<WinType, Integer> d = new HashMap<>();
            if(getDataConfig().isSet("Data." + p.getUniqueId() + ".Solo")) {
                d.put(WinType.SOLO, getDataConfig().getInt("Data." + p.getUniqueId() + ".Solo"));
            }
            if(getDataConfig().isSet("Data." + p.getUniqueId() + ".Team")) {
                d.put(WinType.TEAM, getDataConfig().getInt("Data." + p.getUniqueId() + ".Team"));
            }
            data.put(p, d);
        }
        return data;
    }

    public static HashMap<WinType, Integer> getData(OfflinePlayer player) {
        return getData().containsKey(player) ? getData().get(player) : new HashMap<>();
    }

    public static int getSoloWins(OfflinePlayer player) {
        return getData().containsKey(player) ? getData().get(player).get(WinType.SOLO) : 0;
    }

    public static int getTeamWins(OfflinePlayer player) {
        return getData().containsKey(player) ? getData().get(player).get(WinType.TEAM) : 0;
    }

    public enum WinType {
        SOLO, TEAM
    }


    public static String getFormattedTime(int secondz) {
        int og = secondz;
        int minutes = secondz / 60;
        int hours = minutes / 60;
        int days = hours / 24;

        secondz -= minutes * 60;
        minutes -= hours * 60;
        hours -= days * 24;

        String seconds = secondz + "";

        StringBuilder sb = new StringBuilder();
        if (days > 0) {
            sb.append(days + (hours > 0 ? ":" : ""));
            if(hours > 0 || minutes > 0 || secondz > 0) sb.append(":");
        }
        if (hours > 0) {
            sb.append(hours + (minutes > 0 ? ":" : ""));
            if(minutes > 0 || secondz > 0) sb.append(":");

        }
        if (minutes > 0) {
            sb.append(minutes);
            if(secondz > 0) sb.append(":");
        }
        if (secondz > 0) {
            sb.append(seconds);
        }
        if(og == 0 || sb.toString().length()==0) return "0";
        return sb.toString();
    }

}
